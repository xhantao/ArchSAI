#include <ArchSAI.h>
#include <ArchSAI_General.h>
#include <ArchSAI_ParseArgs.h>
#include <ArchSAI_Read.h>
#include <ArchSAI_PCBuilder.h>
#include <ArchSAI_Solvers.h>
#include <ArchSAI_Matrix.h>
#include <ArchSAI_Print.h>

ARCHSAI_INT main(ARCHSAI_INT argc, ARCHSAI_CHAR *argv[]){

    ArchSAI_Init(&argc, &argv);                   // TODO Implement utilities such as memory tracking

    ARCHSAI_MATCSR    A;
    ARCHSAI_VEC       b;
    ARCHSAI_PRECOND   PC;
    ARCHSAI_VEC       sol;
    ARCHSAI_HANDLER   handler;

    ArchSAI_ParseArgs(argc, argv, &handler);
    ArchSAI_ReadSystem(&A, &b, MPI_COMM_WORLD, &handler);
    ArchSAI_BuildPC(A, &PC, &handler);
    sol = ArchSAI_SolveLinearSystem(A, b, PC, &handler);
    ArchSAI_ExportData(handler, A.comm);

    ArchSAI_End();

    return 0;
}

