#include <ArchSAI_ParseArgs.h>

#ifdef ARCHSAI_USING_CUDA
	#include <cuda.h>
	#include <cuda_runtime.h>
#endif

#ifdef ARCHSAI_USING_AMD
	#include <hip/hip_runtime.h>
#endif

ARCHSAI_INT ArchSAI_ParseArgs(int argc, char *argv[], ARCHSAI_HANDLER *handler){

    #ifdef ARCHSAI_TIMING
    ARCHSAI_INT timeindex = ArchSAI_InitializeTiming("PARSE");
    ArchSAI_BeginTiming(timeindex);
    #endif

    #ifdef ARCHSAI_VERBOSE
    ArchSAI_ParPrintf(MPI_COMM_WORLD, "\n************** ArchSAI Parse *************\n\n");
    #endif


    // Set values for a base case when no option is selected.

    snprintf(handler->params.matname, ARCHSAI_MAX_FILE_NAME_LEN * sizeof(ARCHSAI_CHAR), "test/pores_1.mtx");
    snprintf(handler->params.rhs, ARCHSAI_MAX_FILE_NAME_LEN * sizeof(ARCHSAI_CHAR), "test/b_pores_1.mtx");

    handler->params.pat_sel           = 0;
    handler->params.pat_aux           = 0;
    handler->params.ext_sel           = 0;
    handler->params.ext_spa           = 1;
    handler->params.ext_tmp           = 1;
    handler->params.sol_sel           = 1;
    handler->params.sol_aux           = 100;

    handler->params.kap_epsilon       = 0e-00;
    handler->params.filter_sel        = 0;
    handler->params.filter            = 0E-00;
    handler->params.precond           = 0;

    handler->params.imax              = 10000;
    handler->params.reps              = 1;
    handler->params.rel_tol           = 1E-08;
    handler->params.abs_tol           = 0.0;

    handler->params.rand              = 0;
    handler->params.epos              = 0;

	handler->params.format 			  = 0;
	handler->params.format_aux		  = 0;

    // These booleans allow to avoid repeated arguments.

    bool 	mat_input           = false,
            rhs_input           = false,
            sol_input           = false,
            sax_input           = false,
            pc_input            = false,
            pat_input           = false,
            pax_input           = false,
            ext_input           = false,
            esp_input           = false,
            etm_input           = false,
            flt_input           = false,
            fil_input           = false,
            ite_input           = false,
            rep_input           = false,
            tol_input           = false,
            ato_input           = false,
            ran_input           = false,
            epo_input           = false,
			for_input           = false,
			fax_input           = false;


    // This structure contains the codes for the arguments.

    static const struct option longopts[] = {

        // SYSTEM ARGUMENTS
        // --mat=<PATH/TO/MATRIX/FILE>
        {.name = "mat",         .has_arg = required_argument,   .val = 'a'},
        // --rhs=<PATH/TO/RHS/FILE>
        {.name = "rhs",         .has_arg = required_argument,   .val = 'b'},

        // SOLVER
        {.name = "solver",      .has_arg = required_argument,   .val = 'c'},
        {.name = "solver_aux",  .has_arg = required_argument,   .val = 'd'},

        // PRECONDITIONER
        {.name = "pc",          .has_arg = required_argument,   .val = 'e'},

        // INITIAL PATTERN ARGUMENTS
        {.name = "pattern",     .has_arg = required_argument,   .val = 'f'},
        {.name = "pattern_aux", .has_arg = required_argument,   .val = 'g'},

        // EXTENSION METHOD
        {.name = "extension",     .has_arg = required_argument,   .val = 'h'},
        {.name = "spat",          .has_arg = required_argument,   .val = 'i'},
        {.name = "temp",          .has_arg = required_argument,   .val = 'j'},

        // OTHERS
        {.name = "filter_sel",  .has_arg = required_argument,   .val = 'k'},
        {.name = "filter",      .has_arg = required_argument,   .val = 'l'},
        {.name = "imax",        .has_arg = required_argument,   .val = 'm'},
        {.name = "reps",        .has_arg = required_argument,   .val = 'n'},
        {.name = "rel_tol",     .has_arg = required_argument,   .val = 'o'},
        {.name = "abs_tol",     .has_arg = required_argument,   .val = 'p'},
        {.name = "rand",        .has_arg = required_argument,   .val = 'q'},
        {.name = "epos",        .has_arg = required_argument,   .val = 'r'},

		{.name = "format",        .has_arg = required_argument,   .val = 's'},
		{.name = "format_aux",    .has_arg = required_argument,   .val = 't'},


        {},
    };

    for (;;){

        opterr = 0;
        int opt = getopt_long(argc, argv, "a:b:", longopts, NULL);

        if (opt == -1)
            break;

        switch (opt){

            /* GENERAL INPUTS */
            case 'a':
                ArchSAI_GetOpt(argv, opt, &mat_input, handler->params.matname);
                break;
            case 'b':
                ArchSAI_GetOpt(argv, opt, &rhs_input, handler->params.rhs);
                break;

            /* SOLVER */
            case 'c':
                ArchSAI_GetOpt(argv, opt, &sol_input, &handler->params.sol_sel);
                break;
            case 'd':
                ArchSAI_GetOpt(argv, opt, &sax_input, &handler->params.sol_aux);
                break;

            /* PRECONDITIONER */
            case 'e':
                ArchSAI_GetOpt(argv, opt, &pc_input, &handler->params.precond);
                break;

            /* INIT PATTERN */
            case 'f':
                ArchSAI_GetOpt(argv, opt, &pat_input, &handler->params.pat_sel);
                break;
            case 'g':
                ArchSAI_GetOpt(argv, opt, &pax_input, &handler->params.pat_aux);
                break;

            /* EXTENSION */
            case 'h':
                ArchSAI_GetOpt(argv, opt, &ext_input, &handler->params.ext_sel);
                break;
            case 'i':
                ArchSAI_GetOpt(argv, opt, &esp_input, &handler->params.ext_spa);
                break;
            case 'j':
                ArchSAI_GetOpt(argv, opt, &etm_input, &handler->params.ext_tmp);
                break;


            /* OTHERS */
            case 'k':
                ArchSAI_GetOpt(argv, opt, &flt_input, &handler->params.filter_sel);
                break;
            case 'l':
                ArchSAI_GetOpt(argv, opt, &fil_input, &handler->params.filter);
                break;
            case 'm':
                ArchSAI_GetOpt(argv, opt, &ite_input, &handler->params.imax);
                break;
            case 'n':
                ArchSAI_GetOpt(argv, opt, &rep_input, &handler->params.reps);
                break;
            case 'o':
                ArchSAI_GetOpt(argv, opt, &tol_input, &handler->params.rel_tol);
                break;
            case 'p':
                ArchSAI_GetOpt(argv, opt, &ato_input, &handler->params.abs_tol);
                break;
            case 'q':
                ArchSAI_GetOpt(argv, opt, &ran_input, &handler->params.rand);
                break;
            case 'r':
                ArchSAI_GetOpt(argv, opt, &epo_input, &handler->params.epos);
                break;


            case 's':
                ArchSAI_GetOpt(argv, opt, &for_input, &handler->params.format);
                break;
            case 't':
                ArchSAI_GetOpt(argv, opt, &fax_input, &handler->params.format_aux);
                break;


            default:
                ArchSAI_fprintf(stderr, "WARNING: There are unknown input parameters or some are undefined. Skipping/Using default parameter.\n");
                break;
        }
    }

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

	ARCHSAI_INT deviceCount = 0;
	#if defined ARCHSAI_USING_CUDA
	cudaError_t error = cudaGetDeviceCount(&deviceCount);
	#elif defined ARCHSAI_USING_AMD
// 	cudaError_t error = cudaGetDeviceCount(&deviceCount);
	#endif


    #ifdef ARCHSAI_VERBOSE
        ArchSAI_ParPrintf(MPI_COMM_WORLD, "-- System\n");
        ArchSAI_ParPrintf(MPI_COMM_WORLD, "   -- Matrix Path: %s\n", handler->params.matname);
        ArchSAI_ParPrintf(MPI_COMM_WORLD, "   -- RHS Path:    %s\n", handler->params.rhs);

        ArchSAI_ParPrintf(MPI_COMM_WORLD, "   -- Procs:       %i\n", nprocs);
		#if defined ARCHSAI_USING_CUDA
		ArchSAI_ParPrintf(MPI_COMM_WORLD, "   -- GPUs:        %i\n", deviceCount);
		#endif

        #pragma omp parallel
        #pragma omp single
        ArchSAI_ParPrintf(MPI_COMM_WORLD, "   -- Threads:     %i\n", omp_get_num_threads());

        ArchSAI_ParPrintf(MPI_COMM_WORLD, "-- Solver\n");
        ArchSAI_ParPrintf(MPI_COMM_WORLD, "   -- Solver:      %i\n", handler->params.sol_sel);
        ArchSAI_ParPrintf(MPI_COMM_WORLD, "   -- Solver aux:  %i\n", handler->params.sol_aux);
        ArchSAI_ParPrintf(MPI_COMM_WORLD, "   -- Iter max:    %i\n", handler->params.imax);
        ArchSAI_ParPrintf(MPI_COMM_WORLD, "   -- Repetitions: %i\n", handler->params.reps);
        ArchSAI_ParPrintf(MPI_COMM_WORLD, "   -- Rel tol:     %.2e\n", handler->params.rel_tol);
        ArchSAI_ParPrintf(MPI_COMM_WORLD, "   -- Abs tol:     %.2e\n", handler->params.abs_tol);
		ArchSAI_ParPrintf(MPI_COMM_WORLD, "   -- Format:      %i\n", handler->params.format);
		ArchSAI_ParPrintf(MPI_COMM_WORLD, "   -- Format aux:  %i\n", handler->params.format_aux);

        ArchSAI_ParPrintf(MPI_COMM_WORLD, "-- Preconditioner\n");
        ArchSAI_ParPrintf(MPI_COMM_WORLD, "   -- PC Type:     %i\n", handler->params.precond);

        if (handler->params.precond){
            ArchSAI_ParPrintf(MPI_COMM_WORLD, "-- Pattern\n");
            ArchSAI_ParPrintf(MPI_COMM_WORLD, "   -- Type:     %i\n", handler->params.pat_sel);
            ArchSAI_ParPrintf(MPI_COMM_WORLD, "   -- Level:    %i\n", handler->params.pat_aux);

            if (handler->params.ext_sel) {
                ArchSAI_ParPrintf(MPI_COMM_WORLD, "-- Extension\n");
                ArchSAI_ParPrintf(MPI_COMM_WORLD, "   -- Rand:        %i\n", handler->params.rand);
                ArchSAI_ParPrintf(MPI_COMM_WORLD, "   -- Ext pos:     %i\n", handler->params.epos);
                ArchSAI_ParPrintf(MPI_COMM_WORLD, "   -- Type:        %i\n", handler->params.ext_sel);
                ArchSAI_ParPrintf(MPI_COMM_WORLD, "   -- Spatial:     %i Bytes\n", handler->params.ext_spa * 8);
                ArchSAI_ParPrintf(MPI_COMM_WORLD, "   -- Temporal:    %i Bytes\n", handler->params.ext_tmp * 8);
                ArchSAI_ParPrintf(MPI_COMM_WORLD, "   -- Filter type: %i\n", handler->params.filter_sel);
                ArchSAI_ParPrintf(MPI_COMM_WORLD, "   -- Filter:      %lf\n", handler->params.filter);
            }
        }
    #endif // VERBOSE

    #ifdef ARCHSAI_TIMING
    ArchSAI_EndTiming(timeindex);
    #endif // TIMING

    return archsai_error_flag;
}


ARCHSAI_INT iArchSAI_GetOpt(char *argv[], int opt, bool *input, ARCHSAI_INT* param){

    if (*input == true) {
        ArchSAI_fprintf(stderr, "Input (-%c) defined multiple times. Using first definition.\n", opt);
        return archsai_error_flag;
    }

    *input = true;

    if (optarg == NULL && argv[optind] != NULL && argv[optind][0] != '-') {
        ++optind;
    }
    else {
        if (optarg != NULL) {
            *param = atoi(optarg);
        }
        else ArchSAI_Error(4);
    }
    return archsai_error_flag;
}

ARCHSAI_INT dArchSAI_GetOpt(char *argv[], int opt, bool *input, ARCHSAI_DOUBLE* param){

    if (*input == true) {
        ArchSAI_fprintf(stderr, "Input (-%c) defined multiple times. Using first definition.\n", opt);
        return archsai_error_flag;
    }

    *input = true;

    if (optarg == NULL && argv[optind] != NULL && argv[optind][0] != '-') {
        ++optind;
    }
    else {
        if (optarg != NULL) {
            *param = atof(optarg);
        }
        else ArchSAI_Error(4);
    }
    return archsai_error_flag;
}

ARCHSAI_INT cArchSAI_GetOpt(char *argv[], int opt, bool *input, char* param){

    if (*input == true) {
        ArchSAI_fprintf(stderr, "Input (-%c) defined multiple times. Using first definition.\n", opt);
        return archsai_error_flag;
    }

    *input = true;

    if (optarg == NULL && argv[optind] != NULL && argv[optind][0] != '-') {
        ++optind;
    }
    else {
        if (optarg != NULL) {
            snprintf(param, ARCHSAI_MAX_FILE_NAME_LEN * sizeof(ARCHSAI_CHAR), "%s", optarg);
        }
        else ArchSAI_Error(4);
    }
    return archsai_error_flag;
}










