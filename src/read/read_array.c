
#include <ArchSAI_Read.h>

#define MAXC  1024

ARCHSAI_VEC ArchSAI_ReadArray(ARCHSAI_PARAMS params, ARCHSAI_INT *rowdist, MPI_Comm context){

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(context, &rank);
    MPI_Comm_size(context, &nprocs);

    ARCHSAI_VEC       vec;

    ARCHSAI_INT       ret_code;
    FILE *f;

    ARCHSAI_CHAR     *path = params.rhs;

    ARCHSAI_INT       size = 0;
    ARCHSAI_INT       gsize;
    ARCHSAI_INT       nrhs;
    ARCHSAI_INT       i;
    ARCHSAI_INT       nval;
    ARCHSAI_INT       ret = 0;
    ARCHSAI_CHAR      line[MAXC] = "";

    // Open File to read
    if ((f = fopen(path, "r")) == NULL) {
        printf("\n\nError: Could not read RHS File. Check PATH in --rhs=%s.\n", path);
        ArchSAI_Error(4);
    }

    /* Find out size of sparse matrix .... */
    if ((ret_code = mm_read_mtx_vec_size(f, &gsize, &nrhs)) !=0)
        ArchSAI_Error(1);

    ARCHSAI_DOUBLE *readvec = ArchSAI_TAlloc(ARCHSAI_DOUBLE, gsize, ArchSAI_MEMORY_HOST);

    while (fgets (line, MAXC, f)) {     /* reach each line in file */
        ARCHSAI_INT     offset = 0,     /* offset in line for next sscanf() read */
                        nchr = 0;       /* number of char consumed by last read */
        long double     val;            /* integer value read with sscanf() */

        nval = 0;                       /* number of values read in line */

        /* convert each integer at line + offset, saving no. of chars consumed */
        while (sscanf (line + offset, "%llf%n", &val, &nchr) == 1) {
            offset += nchr;             /* update offset with no. chars consumend */
            nval++;                     /* increment value count */
        }
        break;
    }

    rewind(f);
    mm_read_mtx_vec_size(f, &gsize, &nrhs);

    long double readval = 0.0;

    if (nval == 1) {
        for (int i = 0; i < gsize; i++){
            fscanf(f, "%llf\n", &readval);
            if (rowdist[i] == rank){
                readvec[size] = (ARCHSAI_DOUBLE) readval;
                size++;
            }
        }
    }
    else {
        for (int i = 0; i < gsize; i++){
            fscanf(f, "%d %llf\n", &nval, &readval);
            if (rowdist[i] == rank){
                readvec[size] = (ARCHSAI_DOUBLE) readval;
                size++;
            }
        }
    }

    if (f !=stdin) fclose(f);

    /************************/
    /* now write out matrix */
    /************************/

    vec = ArchSAI_DAllocCreateVecfromArray(readvec, size, gsize);

    ArchSAI_TFree(readvec, ArchSAI_MEMORY_HOST);

    return vec;
}
