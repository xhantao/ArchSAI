#include <ArchSAI_Filter.h>


ARCHSAI_INT ArchSAI_FilterMatCSR(ARCHSAI_MATCSR *pattern, ARCHSAI_MATCSR reference_pattern, ARCHSAI_PARAMS params){


    switch(params.filter_sel){

        case 0:
            break;
        /* AbsVal With Ref */
        case 1:
            ArchSAI_AbsValFilterMatCSRWithRef(pattern, reference_pattern, params.filter);
            break;
        /* AbsVal Without Ref */
        case 2:
            ArchSAI_AbsValFilterMatCSR(pattern, params.filter);
            break;
        /* Scale-Independent With Ref */
        case 3:
            ArchSAI_ScaleIndependentFilterMatCSRWithRef(pattern, reference_pattern, params.filter);
            break;
        /* Scale-Independent Without Ref */
        case 4:
            ArchSAI_ScaleIndependentFilterMatCSR(pattern, params.filter);
            break;

        /* No Filter */
        default:
            break;
    }

    if (pattern->fval) ArchSAI_TFree(pattern->fval, ArchSAI_MEMORY_HOST);


    return archsai_error_flag;
}


 ARCHSAI_INT ArchSAI_AbsValFilterMatCSRWithRef(ARCHSAI_MATCSR *pattern, ARCHSAI_MATCSR reference_pattern, ARCHSAI_DOUBLE filter){

    ARCHSAI_INT   *limits,
                nthreads    = omp_get_max_threads(),
                *tmprows    = ArchSAI_CTAlloc(ARCHSAI_INT,   pattern->nrows + 1, ArchSAI_MEMORY_HOST),
                *copypos    = ArchSAI_CTAlloc(ARCHSAI_INT,   (nthreads + 1), ArchSAI_MEMORY_HOST);

    ARCHSAI_FLOAT ffilter = (ARCHSAI_FLOAT) filter;
    ARCHSAI_MATCSRBalancer(pattern, &limits, nthreads, 1);

    ARCHSAI_FLOAT *largestval = ArchSAI_CTAlloc(ARCHSAI_FLOAT, pattern->nrows, ArchSAI_MEMORY_HOST);

    #pragma omp parallel
    {
        #pragma omp for
        for (int i = 0; i < pattern->nrows; i++){
            ARCHSAI_FLOAT tmplargest = 0.0;
            for (int j = pattern->rowind[i]; j < pattern->rowind[i + 1]; j++){
                ARCHSAI_FLOAT tmp = ArchSAI_Abs(pattern->fval[j]);
                tmplargest = (tmplargest > tmp ? tmplargest : tmp);
            }
            largestval[i] = 1.0/tmplargest;
        }
    }

    #pragma omp parallel num_threads(nthreads)
    {
        ARCHSAI_INT   thid = omp_get_thread_num(),
                    *colsthread = ArchSAI_CTAlloc(ARCHSAI_INT, (pattern->rowind[limits[thid + 1]] - pattern->rowind[limits[thid]]), ArchSAI_MEMORY_HOST),
                    counter = 0,
                    rowcount = 0;

        ARCHSAI_FLOAT val;

        for (int i = limits[thid]; i < limits[thid + 1]; i++){
            for (int j = pattern->rowind[i]; j < pattern->rowind[i + 1]; j++){
                if (ArchSAI_IsColMatCSR(i, pattern->colind[j], &reference_pattern) == 1){
                    colsthread[counter] = pattern->colind[j];
                    counter++;
                    rowcount++;
                }
                else {
                    val = fabs(pattern->fval[j] * largestval[i]);
                    if (val > ffilter){
                        colsthread[counter] = pattern->colind[j];
                        counter++;
                        rowcount++;
                    }
                }
            }
            tmprows[i + 1] = rowcount;
            rowcount = 0;
        }
        copypos[thid + 1] = counter;

        #pragma omp barrier
        #pragma omp single
        {
            for (int i = 0; i < (nthreads); i++) copypos[i + 1] = copypos[i + 1] + copypos[i];
            pattern->nnz = copypos[nthreads];
            ArchSAI_TFree(pattern->colind, ArchSAI_MEMORY_HOST);
            pattern->colind = ArchSAI_CTAlloc(ARCHSAI_INT, pattern->nnz, ArchSAI_MEMORY_HOST);
        }

        ArchSAI_TMemcpy(pattern->colind + *(copypos + thid), colsthread, ARCHSAI_INT, counter, ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);
        ArchSAI_TFree(colsthread, ArchSAI_MEMORY_HOST);
    }

    MPI_Allreduce(&pattern->nnz, &pattern->gnnz, 1, ARCHSAI_MPI_INT, MPI_SUM, pattern->comm);

    ArchSAI_TMemcpy(pattern->rowind, tmprows, ARCHSAI_INT, (pattern->nrows + 1), ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);
    for (int i = 0; i < pattern->nrows; i++) pattern->rowind[i + 1] = pattern->rowind[i + 1] + pattern->rowind[i];

    ArchSAI_TFree(limits, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(tmprows, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(copypos, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(largestval, ArchSAI_MEMORY_HOST);

    return archsai_error_flag;
}


 ARCHSAI_INT ArchSAI_ScaleIndependentFilterMatCSRWithRef(ARCHSAI_MATCSR *pattern, ARCHSAI_MATCSR reference_pattern, ARCHSAI_DOUBLE filter){

    if (pattern->gnrows != pattern->gncols){
        ArchSAI_ParPrintf(pattern->comm, "\nError: Scale Independent Filter can only be used in square matrices.");
        ArchSAI_Error(1);
    }

    if (pattern->fulldiag == 0){
        ArchSAI_ParPrintf(pattern->comm, "\nError: Scale Independent Filter can only be used if system matrix has full numerical rank.");
        ArchSAI_Error(1);
    }

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(pattern->comm, &rank);
    MPI_Comm_size(pattern->comm, &nprocs);

    ARCHSAI_INT   *limits,
                nthreads    = omp_get_max_threads(),
                *tmprows    = ArchSAI_CTAlloc(ARCHSAI_INT,   pattern->nrows + 1, ArchSAI_MEMORY_HOST),
                *copypos    = ArchSAI_CTAlloc(ARCHSAI_INT,   (nthreads + 1), ArchSAI_MEMORY_HOST);

    ARCHSAI_FLOAT ffilter = (ARCHSAI_FLOAT) filter;
    ARCHSAI_MATCSRBalancer(pattern, &limits, nthreads, 1);

    ARCHSAI_FLOAT *diagvalue = ArchSAI_CTAlloc(ARCHSAI_FLOAT, pattern->ncols, ArchSAI_MEMORY_HOST);

    #pragma omp parallel
    {
        #pragma omp for
        for (int i = 0; i < pattern->nrows; i++){
            ARCHSAI_INT start = 0;
            diagvalue[i] = 1.0/sqrt(fabs(ArchSAI_GetFValueMatCSR(&start, i, i, *pattern)));
        }
    }

    /* I need to communicate diagonal values required by other processes */

    ARCHSAI_INT send_size = pattern->commscheme->sendsize;
    ARCHSAI_INT nrecv = pattern->commscheme->nrecv;
    ARCHSAI_INT nsend = pattern->commscheme->nsend;

    MPI_Request *req = ArchSAI_TAlloc(MPI_Request, 2 * nprocs, ArchSAI_MEMORY_HOST);
    ARCHSAI_FLOAT *send_buffer = ArchSAI_TAlloc(ARCHSAI_FLOAT, send_size, ArchSAI_MEMORY_HOST);

    /* Post receive call*/
    for (int i = 0; i < nrecv; i++){
        ARCHSAI_INT size = pattern->commscheme->recvptr[i + 2] - pattern->commscheme->recvptr[i + 1];
        MPI_Irecv(diagvalue + pattern->commscheme->recvptr[i + 1], size, MPI_FLOAT, pattern->commscheme->recvproc[i], 0, pattern->comm, &req[i]);
    }

    /* Fill sending buffer */
    for (int i = 0; i < send_size; i++){
        send_buffer[i] = diagvalue[pattern->commscheme->sendpos[i]];
    }

    /* Post sending call */
    for (int i = 0; i < nsend; i++){
        ARCHSAI_INT size = pattern->commscheme->sendptr[i + 1] - pattern->commscheme->sendptr[i];
        MPI_Isend(send_buffer + pattern->commscheme->sendptr[i], size, MPI_FLOAT, pattern->commscheme->sendproc[i], 0, pattern->comm, &req[nrecv + i]);
    }

    MPI_Waitall(nsend + nrecv, req, MPI_STATUSES_IGNORE);

    #pragma omp parallel num_threads(nthreads)
    {
        ARCHSAI_INT   thid = omp_get_thread_num(),
                    *colsthread = ArchSAI_CTAlloc(ARCHSAI_INT, (pattern->rowind[limits[thid + 1]] - pattern->rowind[limits[thid]]), ArchSAI_MEMORY_HOST),
                    counter = 0,
                    rowcount = 0;

        ARCHSAI_FLOAT val;

        for (int i = limits[thid]; i < limits[thid + 1]; i++){
            for (int j = pattern->rowind[i]; j < pattern->rowind[i + 1]; j++){
                if (ArchSAI_IsColMatCSR(i, pattern->colind[j], &reference_pattern) == 1){
                    colsthread[counter] = pattern->colind[j];
                    counter++;
                    rowcount++;
                }
                else {
                    val = fabs(pattern->fval[j] * diagvalue[i] * diagvalue[pattern->colind[j]]);
                    if (val > ffilter){
                        colsthread[counter] = pattern->colind[j];
                        counter++;
                        rowcount++;
                    }
                }
            }
            tmprows[i + 1] = rowcount;
            rowcount = 0;
        }
        copypos[thid + 1] = counter;

        #pragma omp barrier
        #pragma omp single
        {
            for (int i = 0; i < (nthreads); i++) copypos[i + 1] = copypos[i + 1] + copypos[i];
            pattern->nnz = copypos[nthreads];
            ArchSAI_TFree(pattern->colind, ArchSAI_MEMORY_HOST);
            pattern->colind = ArchSAI_CTAlloc(ARCHSAI_INT, pattern->nnz, ArchSAI_MEMORY_HOST);
        }

        ArchSAI_TMemcpy(pattern->colind + *(copypos + thid), colsthread, ARCHSAI_INT, counter, ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);
        ArchSAI_TFree(colsthread, ArchSAI_MEMORY_HOST);
    }

    MPI_Allreduce(&pattern->nnz, &pattern->gnnz, 1, ARCHSAI_MPI_INT, MPI_SUM, pattern->comm);

    ArchSAI_TMemcpy(pattern->rowind, tmprows, ARCHSAI_INT, (pattern->nrows + 1), ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);
    for (int i = 0; i < pattern->nrows; i++) pattern->rowind[i + 1] = pattern->rowind[i + 1] + pattern->rowind[i];

    ArchSAI_TFree(limits, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(tmprows, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(copypos, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(diagvalue, ArchSAI_MEMORY_HOST);

    return archsai_error_flag;
}








ARCHSAI_INT ArchSAI_AbsValFilterMatCSR(ARCHSAI_MATCSR *pattern, ARCHSAI_DOUBLE filter){

    ARCHSAI_INT   *limits,
                nthreads    = omp_get_max_threads(),
                *tmprows    = ArchSAI_CTAlloc(ARCHSAI_INT,   pattern->nrows + 1, ArchSAI_MEMORY_HOST),
                *copypos    = ArchSAI_CTAlloc(ARCHSAI_INT,   (nthreads + 1), ArchSAI_MEMORY_HOST);

    ARCHSAI_FLOAT ffilter = (ARCHSAI_FLOAT) filter;
    ARCHSAI_MATCSRBalancer(pattern, &limits, nthreads, 1);

    ARCHSAI_FLOAT *largestval = ArchSAI_CTAlloc(ARCHSAI_FLOAT, pattern->nrows, ArchSAI_MEMORY_HOST);

    #pragma omp parallel
    {
        #pragma omp for
        for (int i = 0; i < pattern->nrows; i++){
            ARCHSAI_FLOAT tmplargest = 0.0;
            for (int j = pattern->rowind[i]; j < pattern->rowind[i + 1]; j++){
                ARCHSAI_FLOAT tmp = ArchSAI_Abs(pattern->fval[j]);
                tmplargest = (tmplargest > tmp ? tmplargest : tmp);
            }
            largestval[i] = 1.0/tmplargest;
        }
    }

    #pragma omp parallel num_threads(nthreads)
    {
        ARCHSAI_INT   thid = omp_get_thread_num(),
                    *colsthread = ArchSAI_CTAlloc(ARCHSAI_INT, (pattern->rowind[limits[thid + 1]] - pattern->rowind[limits[thid]]), ArchSAI_MEMORY_HOST),
                    counter = 0,
                    rowcount = 0;

        ARCHSAI_FLOAT val;

        for (int i = limits[thid]; i < limits[thid + 1]; i++){
            for (int j = pattern->rowind[i]; j < pattern->rowind[i + 1]; j++){
                if (pattern->colind[j] == i){
                    colsthread[counter] = pattern->colind[j];
                    counter++;
                    rowcount++;
                }
                else {
                    val = fabs(pattern->fval[j] * largestval[i]);
                    if (val > ffilter){
                        colsthread[counter] = pattern->colind[j];
                        counter++;
                        rowcount++;
                    }
                }
            }
            tmprows[i + 1] = rowcount;
            rowcount = 0;
        }
        copypos[thid + 1] = counter;

        #pragma omp barrier
        #pragma omp single
        {
            for (int i = 0; i < (nthreads); i++) copypos[i + 1] = copypos[i + 1] + copypos[i];
            pattern->nnz = copypos[nthreads];
            ArchSAI_TFree(pattern->colind, ArchSAI_MEMORY_HOST);
            pattern->colind = ArchSAI_CTAlloc(ARCHSAI_INT, pattern->nnz, ArchSAI_MEMORY_HOST);
        }

        ArchSAI_TMemcpy(pattern->colind + *(copypos + thid), colsthread, ARCHSAI_INT, counter, ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);
        ArchSAI_TFree(colsthread, ArchSAI_MEMORY_HOST);
    }

    MPI_Allreduce(&pattern->nnz, &pattern->gnnz, 1, ARCHSAI_MPI_INT, MPI_SUM, pattern->comm);

    ArchSAI_TMemcpy(pattern->rowind, tmprows, ARCHSAI_INT, (pattern->nrows + 1), ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);
    for (int i = 0; i < pattern->nrows; i++) pattern->rowind[i + 1] = pattern->rowind[i + 1] + pattern->rowind[i];

    ArchSAI_TFree(limits, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(tmprows, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(copypos, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(largestval, ArchSAI_MEMORY_HOST);

    return archsai_error_flag;
}


 ARCHSAI_INT ArchSAI_ScaleIndependentFilterMatCSR(ARCHSAI_MATCSR *pattern, ARCHSAI_DOUBLE filter){

    if (pattern->gnrows != pattern->gncols){
        ArchSAI_ParPrintf(pattern->comm, "\nError: Scale Independent Filter can only be used in square matrices.");
        ArchSAI_Error(1);
    }

    if (pattern->fulldiag == 0){
        ArchSAI_ParPrintf(pattern->comm, "\nError: Scale Independent Filter can only be used if system matrix has full numerical rank.");
        ArchSAI_Error(1);
    }

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(pattern->comm, &rank);
    MPI_Comm_size(pattern->comm, &nprocs);

    ARCHSAI_INT   *limits,
                nthreads    = omp_get_max_threads(),
                *tmprows    = ArchSAI_CTAlloc(ARCHSAI_INT,   pattern->nrows + 1, ArchSAI_MEMORY_HOST),
                *copypos    = ArchSAI_CTAlloc(ARCHSAI_INT,   (nthreads + 1), ArchSAI_MEMORY_HOST);

    ARCHSAI_FLOAT ffilter = (ARCHSAI_FLOAT) filter;
    ARCHSAI_MATCSRBalancer(pattern, &limits, nthreads, 1);

    ARCHSAI_FLOAT *diagvalue = ArchSAI_CTAlloc(ARCHSAI_FLOAT, pattern->ncols, ArchSAI_MEMORY_HOST);

    #pragma omp parallel
    {
        #pragma omp for
        for (int i = 0; i < pattern->nrows; i++){
            ARCHSAI_INT start = 0;
            diagvalue[i] = 1.0/sqrt(fabs(ArchSAI_GetFValueMatCSR(&start, i, i, *pattern)));
        }
    }

    /* I need to communicate diagonal values required by other processes */

    ARCHSAI_INT send_size = pattern->commscheme->sendsize;
    ARCHSAI_INT nrecv = pattern->commscheme->nrecv;
    ARCHSAI_INT nsend = pattern->commscheme->nsend;

    MPI_Request *req = ArchSAI_TAlloc(MPI_Request, 2 * nprocs, ArchSAI_MEMORY_HOST);
    ARCHSAI_FLOAT *send_buffer = ArchSAI_TAlloc(ARCHSAI_FLOAT, send_size, ArchSAI_MEMORY_HOST);

    /* Post receive call*/
    for (int i = 0; i < nrecv; i++){
        ARCHSAI_INT size = pattern->commscheme->recvptr[i + 2] - pattern->commscheme->recvptr[i + 1];
        MPI_Irecv(diagvalue + pattern->commscheme->recvptr[i + 1], size, MPI_FLOAT, pattern->commscheme->recvproc[i], 0, pattern->comm, &req[i]);
    }

    /* Fill sending buffer */
    for (int i = 0; i < send_size; i++){
        send_buffer[i] = diagvalue[pattern->commscheme->sendpos[i]];
    }

    /* Post sending call */
    for (int i = 0; i < nsend; i++){
        ARCHSAI_INT size = pattern->commscheme->sendptr[i + 1] - pattern->commscheme->sendptr[i];
        MPI_Isend(send_buffer + pattern->commscheme->sendptr[i], size, MPI_FLOAT, pattern->commscheme->sendproc[i], 0, pattern->comm, &req[nrecv + i]);
    }

    MPI_Waitall(nsend + nrecv, req, MPI_STATUSES_IGNORE);

    #pragma omp parallel num_threads(nthreads)
    {
        ARCHSAI_INT   thid = omp_get_thread_num(),
                    *colsthread = ArchSAI_CTAlloc(ARCHSAI_INT, (pattern->rowind[limits[thid + 1]] - pattern->rowind[limits[thid]]), ArchSAI_MEMORY_HOST),
                    counter = 0,
                    rowcount = 0;

        ARCHSAI_FLOAT val;

        for (int i = limits[thid]; i < limits[thid + 1]; i++){
            for (int j = pattern->rowind[i]; j < pattern->rowind[i + 1]; j++){
                if (pattern->colind[j] == i){
                    colsthread[counter] = pattern->colind[j];
                    counter++;
                    rowcount++;
                }
                else {
                    val = fabs(pattern->fval[j] * diagvalue[i] * diagvalue[pattern->colind[j]]);
                    if (val > ffilter){
                        colsthread[counter] = pattern->colind[j];
                        counter++;
                        rowcount++;
                    }
                }
            }
            tmprows[i + 1] = rowcount;
            rowcount = 0;
        }
        copypos[thid + 1] = counter;

        #pragma omp barrier
        #pragma omp single
        {
            for (int i = 0; i < (nthreads); i++) copypos[i + 1] = copypos[i + 1] + copypos[i];
            pattern->nnz = copypos[nthreads];
            ArchSAI_TFree(pattern->colind, ArchSAI_MEMORY_HOST);
            pattern->colind = ArchSAI_CTAlloc(ARCHSAI_INT, pattern->nnz, ArchSAI_MEMORY_HOST);
        }

        ArchSAI_TMemcpy(pattern->colind + *(copypos + thid), colsthread, ARCHSAI_INT, counter, ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);
        ArchSAI_TFree(colsthread, ArchSAI_MEMORY_HOST);
    }

    MPI_Allreduce(&pattern->nnz, &pattern->gnnz, 1, ARCHSAI_MPI_INT, MPI_SUM, pattern->comm);

    ArchSAI_TMemcpy(pattern->rowind, tmprows, ARCHSAI_INT, (pattern->nrows + 1), ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);
    for (int i = 0; i < pattern->nrows; i++) pattern->rowind[i + 1] = pattern->rowind[i + 1] + pattern->rowind[i];

    ArchSAI_TFree(limits, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(tmprows, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(copypos, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(diagvalue, ArchSAI_MEMORY_HOST);

    return archsai_error_flag;
}
