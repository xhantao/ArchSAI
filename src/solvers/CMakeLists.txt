file(GLOB_RECURSE SRC_FILES ${CMAKE_CURRENT_LIST_DIR}/*.c)

target_sources(${PROJECT_NAME}
    PRIVATE ${SRC_FILES}
)

#if (ARCHSAI_NEC)
    #add_compile_options(-g -report-all -fdiag-vector=2 -ftrace -proginf -traceback -static)
#endif()

if (ARCHSAI_NEC)
    target_link_libraries(${PROJECT_NAME} PRIVATE -lm -ftrace)
endif()

#if (ARCHSAI_USING_CUDA)
    #set(GPU_DIRS    cuda)
    #foreach (DIR IN LISTS GPU_DIRS)
        #add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/${DIR})
        #target_include_directories(${PROJECT_NAME} PUBLIC $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/${DIR}>)
    #endforeach ()
#endif (ARCHSAI_USING_CUDA)

#if (ARCHSAI_USING_AMD)
    #add_subdirectory(AMD)
    #target_include_directories(${PROJECT_NAME} PUBLIC $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/amd>)
#endif (ARCHSAI_USING_AMD)
