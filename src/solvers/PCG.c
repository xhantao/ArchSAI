#include <ArchSAI_Solvers.h>
#include <ArchSAI_Print.h>

#if defined ARCHSAI_NEC
    #include <ftrace.h>
    #include <sblas.h>
#endif

#ifdef ARCHSAI_USING_GPU
	#include <cuda.h>
	#include <cuda_runtime.h>
	#include <cuda_runtime_api.h>
#endif

ARCHSAI_VEC ArchSAI_PCG(ARCHSAI_SOLVER *PCG, ARCHSAI_MATCSR A, ARCHSAI_VEC b, ARCHSAI_PRECOND PC, ARCHSAI_PARAMS params){

    /* Sanity Checks */

    #ifdef ARCHSAI_VERBOSE
    #endif

    /* ****************************************************************************************** */
    /* VARIABLES FOR PCG */

    *PCG = ArchSAI_InitSolver(params, A, PC);

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    // LOOP HANDLERS

    ARCHSAI_INT          rep = 0;
    ARCHSAI_INT         iter = 0;
    ARCHSAI_INT      i, j, k = 0;

    // ARRAYS
    // Implement ArchSAI_AlignedCTAlloc()
    // r must be aligned in memory
    // p[] each vector in p[] must be aligned in memory

    ARCHSAI_VEC            x;
                       x.val = ArchSAI_TAlignedAlloc(ARCHSAI_DOUBLE,      A.ncols,                64, ArchSAI_MEMORY_HOST);
                      x.size = A.ncols;

    ARCHSAI_DOUBLE        *r;
                        if (params.precond == 0) r = ArchSAI_TAlignedAlloc(ARCHSAI_DOUBLE, A.ncols, 64, ArchSAI_MEMORY_HOST);
                        else r = ArchSAI_TAlignedAlloc(ARCHSAI_DOUBLE, G(PC).ncols, 64, ArchSAI_MEMORY_HOST);


    ARCHSAI_DOUBLE        *d = ArchSAI_TAlignedAlloc(ARCHSAI_DOUBLE,      A.ncols,                64, ArchSAI_MEMORY_HOST);
    ARCHSAI_DOUBLE        *q = ArchSAI_TAlignedAlloc(ARCHSAI_DOUBLE,      A.nrows,                64, ArchSAI_MEMORY_HOST);
    ARCHSAI_DOUBLE        *s = ArchSAI_TAlignedAlloc(ARCHSAI_DOUBLE,      A.nrows,                64, ArchSAI_MEMORY_HOST);
    ARCHSAI_DOUBLE      *tmp = ArchSAI_TAlignedAlloc(ARCHSAI_DOUBLE,      A.nrows,                64, ArchSAI_MEMORY_HOST);

    ARCHSAI_DOUBLE     *spmv = NULL;
                        if (params.precond == 3) spmv = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, Gt(PC).ncols, ArchSAI_MEMORY_HOST);

    // To store norms of vectors
    ARCHSAI_DOUBLE    norm_r = 0.0;
    ARCHSAI_DOUBLE  norm_r_0 = 0.0;
    ARCHSAI_DOUBLE invnorm_r = 0.0;
    ARCHSAI_DOUBLE    norm_b = 0.0;

    // Other
    ARCHSAI_DOUBLE     d_new = 0.0;
    ARCHSAI_DOUBLE     d_old = 0.0;
    ARCHSAI_DOUBLE     d_div = 0.0;
    ARCHSAI_DOUBLE     alpha = 0.0;
    ARCHSAI_DOUBLE neg_alpha = 0.0;
    ARCHSAI_DOUBLE  den_norm = 0.0;
    ARCHSAI_DOUBLE   epsilon = 0.0;

    // Time measurement
    ARCHSAI_DOUBLE   reptime = 0.0;
    ARCHSAI_DOUBLE  itertime = 0.0;
    ARCHSAI_DOUBLE  besttime = 1E09;

    #if defined ARCHSAI_NEC

        #if defined ARCHSAI_SINGLE
            // A
            sblas_create_matrix_handle_from_csr_rs(A.nrows, A.ncols, A.rowind, A.colind, A.val, SBLAS_INDEXING_0, SBLAS_GENERAL, &(A.sbh));
            sblas_analyze_mv_rs(SBLAS_NON_TRANSPOSE, A.sbh);

            // PC
            if ((params.precond == 1) || (params.precond == 2)){
                sblas_create_matrix_handle_from_csr_rs(M(PC).nrows, M(PC).ncols, M(PC).rowind, M(PC).colind, M(PC).val, SBLAS_INDEXING_0, SBLAS_GENERAL, &(M(PC).sbh));
                sblas_analyze_mv_rs(SBLAS_NON_TRANSPOSE, M(PC).sbh);
            }
            else if (params.precond == 3){
                sblas_create_matrix_handle_from_csr_rs(G(PC).nrows, G(PC).ncols, G(PC).rowind, G(PC).colind, G(PC).val, SBLAS_INDEXING_0, SBLAS_GENERAL, &(G(PC).sbh));
                sblas_analyze_mv_rs(SBLAS_NON_TRANSPOSE, G(PC).sbh);
                sblas_create_matrix_handle_from_csr_rs(Gt(PC).nrows, Gt(PC).ncols, Gt(PC).rowind, Gt(PC).colind, Gt(PC).val, SBLAS_INDEXING_0, SBLAS_GENERAL, &(Gt(PC).sbh));
                sblas_analyze_mv_rs(SBLAS_NON_TRANSPOSE, Gt(PC).sbh);
            }

        #else
            // A
            sblas_create_matrix_handle_from_csr_rd(A.nrows, A.ncols, A.rowind, A.colind, A.val, SBLAS_INDEXING_0, SBLAS_GENERAL, &(A.sbh));
            sblas_analyze_mv_rd(SBLAS_NON_TRANSPOSE, A.sbh);

            // PC
            if ((params.precond == 1) || (params.precond == 2)){
                sblas_create_matrix_handle_from_csr_rd(M(PC).nrows, M(PC).ncols, M(PC).rowind, M(PC).colind, M(PC).val, SBLAS_INDEXING_0, SBLAS_GENERAL, &(M(PC).sbh));
                sblas_analyze_mv_rd(SBLAS_NON_TRANSPOSE, M(PC).sbh);
            }
            else if (params.precond == 3){
                sblas_create_matrix_handle_from_csr_rd(G(PC).nrows, G(PC).ncols, G(PC).rowind, G(PC).colind, G(PC).val, SBLAS_INDEXING_0, SBLAS_GENERAL, &(G(PC).sbh));
                sblas_analyze_mv_rd(SBLAS_NON_TRANSPOSE, G(PC).sbh);
                sblas_create_matrix_handle_from_csr_rd(Gt(PC).nrows, Gt(PC).ncols, Gt(PC).rowind, Gt(PC).colind, Gt(PC).val, SBLAS_INDEXING_0, SBLAS_GENERAL, &(Gt(PC).sbh));
                sblas_analyze_mv_rd(SBLAS_NON_TRANSPOSE, Gt(PC).sbh);
            }
        #endif
    #endif

    /* ****************************************************************************************** */

    /* Begin USER-DEFINED repetitions of solver */

    while (rep < params.reps){

        // Reinitialize used vectors
        if (params.precond == 0) ArchSAI_Memset(r, 0, A.ncols * sizeof(ARCHSAI_DOUBLE), ArchSAI_MEMORY_HOST);
        else ArchSAI_Memset(r, 0, G(PC).ncols * sizeof(ARCHSAI_DOUBLE), ArchSAI_MEMORY_HOST);

        ArchSAI_Memset(x.val,        0,       A.ncols * sizeof(ARCHSAI_DOUBLE),    ArchSAI_MEMORY_HOST);
        ArchSAI_Memset(d,            0,       A.ncols * sizeof(ARCHSAI_DOUBLE),    ArchSAI_MEMORY_HOST);
        ArchSAI_Memset(q,            0,       A.nrows * sizeof(ARCHSAI_DOUBLE),    ArchSAI_MEMORY_HOST);
        ArchSAI_Memset(s,            0,       A.nrows * sizeof(ARCHSAI_DOUBLE),    ArchSAI_MEMORY_HOST);
        ArchSAI_Memset(tmp,          0,       A.nrows * sizeof(ARCHSAI_DOUBLE),    ArchSAI_MEMORY_HOST);

       // SETUP

        // Check b and get norm_b
        norm_b = ArchSAI_Sqrt(ArchSAI_DOTP(b.size, vector(b), int_one, vector(b), int_one, A.comm, nprocs));

        // Copy RHS to r to Compute Initial Residual and preconditioned residual d
        ArchSAI_Copy(A.nrows, vector(b), int_one, r, int_one);
        ArchSAI_PrecondSpMV(PC, r, d, spmv, nprocs);

        d_new = ArchSAI_DOTP(A.nrows, d, int_one, r, int_one, A.comm, nprocs);

        // Check residual
        norm_r = ArchSAI_Sqrt(ArchSAI_DOTP(A.nrows, r, int_one, r, int_one, A.comm, nprocs));
        norm_r_0 = norm_r;

        PCG->residual[rep][0] = norm_r;
        PCG->rel_res[rep][0] = norm_r / norm_b;

        // Set convergence tolerances
        den_norm = norm_r;
        epsilon = ArchSAI_Max(params.abs_tol, params.rel_tol * den_norm);

        /* Begin PCG Iterations */

        #if defined ARCHSAI_NEC
            ftrace_region_begin("CG");
        #endif

        iter = 0;
        reptime = time_getWallclockSeconds();
        itertime = time_getWallclockSeconds();
        while (iter < params.imax){
            iter++;

            /* q = Ad */
        #if defined ARCHSAI_NEC
            ftrace_region_begin("SpMV 1");
        #endif
            ArchSAI_CSRMV(trans, matdescra, nprocs, fp_one, A, d, fp_zero, q);
        #if defined ARCHSAI_NEC
            ftrace_region_end("SpMV 1");
        #endif
            /* alpha = d_new / dq */
        #if defined ARCHSAI_NEC
            ftrace_region_begin("DOTP 1");
        #endif
            alpha = d_new / ArchSAI_DOTP(A.nrows, d, int_one, q, int_one, A.comm, nprocs);
        #if defined ARCHSAI_NEC
            ftrace_region_end("DOTP 1");
        #endif
            /* x = x + alpha*d */
        #if defined ARCHSAI_NEC
            ftrace_region_begin("AXPY 1");
        #endif
            ArchSAI_Axpy(A.nrows, alpha, d, int_one, vector(x), int_one);
        #if defined ARCHSAI_NEC
            ftrace_region_end("AXPY 1");
        #endif
            if (iter % 50 == 0){
                /* r = b - Ax */
        #if defined ARCHSAI_NEC
            ftrace_region_begin("SpMV C1");
        #endif
                ArchSAI_Copy(A.nrows, vector(b), int_one, r, int_one);
                ArchSAI_CSRMV(trans, matdescra, nprocs, fp_none, A, vector(x), fp_one, r);
        #if defined ARCHSAI_NEC
            ftrace_region_end("SpMV C1");
        #endif
            }
            else {
                /* r = r - alpha * q */
                neg_alpha = -alpha;
        #if defined ARCHSAI_NEC
            ftrace_region_begin("AXPY C1");
        #endif
                ArchSAI_Axpy(A.nrows, neg_alpha, q, int_one, r, int_one);
        #if defined ARCHSAI_NEC
            ftrace_region_end("AXPY C1");
        #endif
            }

            /* s = M-1 * r */
        #if defined ARCHSAI_NEC
            ftrace_region_begin("PC");
        #endif
            ArchSAI_PrecondSpMV(PC, r, s, spmv, nprocs);
        #if defined ARCHSAI_NEC
            ftrace_region_end("PC");
        #endif


            d_old = d_new;
        #if defined ARCHSAI_NEC
            ftrace_region_begin("DOTP 2");
        #endif
            d_new = ArchSAI_DOTP(A.nrows, r, int_one, s, int_one, A.comm, nprocs);
        #if defined ARCHSAI_NEC
            ftrace_region_end("DOTP 2");
        #endif

            /* d = s + d (d_new/d_old) */
            d_div = d_new / d_old;
        #if defined ARCHSAI_NEC
            ftrace_region_begin("AXPY 2");
        #endif
            ArchSAI_Scal(A.nrows, d_div, d, int_one);
            ArchSAI_Axpy(A.nrows, fp_one, s, int_one, d, int_one);
        #if defined ARCHSAI_NEC
            ftrace_region_end("AXPY 2");
        #endif

        #if defined ARCHSAI_NEC
            ftrace_region_begin("DOTP 3");
        #endif
            norm_r = ArchSAI_Sqrt(ArchSAI_DOTP(A.nrows, r, int_one, r, int_one, A.comm, nprocs));
        #if defined ARCHSAI_NEC
            ftrace_region_end("DOTP 3");
        #endif

            PCG->residual[rep][iter] = norm_r;
            PCG->rel_res[rep][iter] = norm_r / PCG->residual[rep][0];

            if (norm_r <= epsilon){
                PCG->iter_time[rep][iter] = time_getWallclockSeconds() - itertime;
                itertime = time_getWallclockSeconds();
                break;
            }

            PCG->iter_time[rep][iter] = time_getWallclockSeconds() - itertime;
            itertime = time_getWallclockSeconds();
        }

        PCG->time[rep] = time_getWallclockSeconds() - reptime;
        PCG->iter[rep] = iter;

        #if defined ARCHSAI_NEC
            ftrace_region_end("CG");
        #endif

        if (PCG->time[rep] < besttime){
            besttime = PCG->time[rep];
            PCG->bestrep = rep;
        }
        rep++;
    }

//     ArchSAI_ExportSolverMetrics(*PCG, params, A.comm);

    // Free Memory

    ArchSAI_TFree(d,          ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(q,          ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(s,          ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(r,          ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(tmp,        ArchSAI_MEMORY_HOST);

//     #if defined ARCHSAI_NEC
//         // A
//         sblas_destroy_matrix_handle(A.sbh);
//
//         // PC
//         if (params.precond == 1){
//             sblas_destroy_matrix_handle(M(PC).sbh);
//         }
//         else if (params.precond == 2){
//             sblas_destroy_matrix_handle(G(PC).sbh);
//             sblas_destroy_matrix_handle(Gt(PC).sbh);
//         }
//
//     #endif

    #ifdef ARCHSAI_VERBOSE
        ArchSAI_ParPrintf(A.comm, "\nRep %i: %i %lf\n", PCG->bestrep, PCG->iter[PCG->bestrep], PCG->time[PCG->bestrep]);
    #endif // VERBOSE

    return x;
}






























