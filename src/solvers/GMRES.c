#include <ArchSAI_Solvers.h>
#include <ArchSAI_Print.h>

#if defined ARCHSAI_NEC
    #include <ftrace.h>
    #include <sblas.h>
#endif

int CHECKNORM(ARCHSAI_DOUBLE norm){
    ARCHSAI_INT ieee_chk;
    if (norm != 0.) ieee_chk = norm / norm;
    if (ieee_chk != ieee_chk){
//         ArchSAI_ParPrintf(A.comm, "Error: Norm contains bad input.\n");
        ArchSAI_Error(1);
    }
}

void Update(ARCHSAI_VEC *x, ARCHSAI_INT k, ARCHSAI_DOUBLE **H, ARCHSAI_DOUBLE *residuals, ARCHSAI_DOUBLE **v){
    for (int i = k; i >= 0; i--){
        residuals[i] /= H[i][i];
        for (int j = i - 1; j >= 0; j--){
            residuals[j] -= H[j][i] * residuals[i];
        }
    }
    for (int i = 0; i < x->size; i++){
        for (int j = 0; j < k+1; j++){
            x->val[i] += v[j][i] * residuals[j];
        }
    }
}

void ApplyPlaneRotation(ARCHSAI_DOUBLE *dx, ARCHSAI_DOUBLE *dy, ARCHSAI_DOUBLE c, ARCHSAI_DOUBLE s){
    ARCHSAI_DOUBLE tmp = c * (*dx) + s * (*dy);
    (*dy) = -s * (*dx) + c * (*dy);
    (*dx) = tmp;
}

void GeneratePlaneRotation(ARCHSAI_DOUBLE dx, ARCHSAI_DOUBLE dy, ARCHSAI_DOUBLE *c, ARCHSAI_DOUBLE *s){

//     ARCHSAI_DOUBLE tmp = ArchSAI_Sqrt(dx*dx+dy*dy);
//     *c = dx / tmp;
//     *s = dy / tmp;

    if (dy == 0.0){(*c) = 1.0, (*s) = 0.0;}
    else if (ArchSAI_Abs(dy) > ArchSAI_Abs(dx)) {
        ARCHSAI_DOUBLE tmp = dx / dy;
        (*s) = 1.0 / ArchSAI_Sqrt(1.0+tmp*tmp);
        (*c) = tmp*(*s);
    }
    else {
        ARCHSAI_DOUBLE tmp = dy / dx;
        (*c) = 1.0 / ArchSAI_Sqrt(1.0+tmp*tmp);
        (*s) = tmp*(*c);
    }
}

ARCHSAI_VEC ArchSAI_GMRES(ARCHSAI_SOLVER *GMRES, ARCHSAI_MATCSR A, ARCHSAI_VEC b, ARCHSAI_PRECOND PC, ARCHSAI_PARAMS params){

    /* Sanity Checks */

    #ifdef ARCHSAI_VERBOSE
    #endif

    /* ****************************************************************************************** */
    /* VARIABLES FOR GMRES */

    *GMRES = ArchSAI_InitSolver(params, A, PC);

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    // LOOP HANDLERS
    ARCHSAI_INT          rep = 0;
    ARCHSAI_INT    converged = 0;
    ARCHSAI_INT         iter = 0;
    ARCHSAI_INT      i, j, k = 0;

    // ARRAYS
    // Implement ArchSAI_AlignedCTAlloc()
    // r must be aligned in memory
    // p[] each vector in p[] must be aligned in memory

    ARCHSAI_VEC            x;
                     x.val = ArchSAI_TAlignedAlloc(ARCHSAI_DOUBLE,        A.ncols,                64, ArchSAI_MEMORY_HOST);
                    x.size = A.nrows;

    ARCHSAI_DOUBLE     *tmpr = NULL;
                        if (params.precond == 0) tmpr = ArchSAI_TAlignedAlloc(ARCHSAI_DOUBLE, A.ncols, 64, ArchSAI_MEMORY_HOST);
                        else tmpr = ArchSAI_TAlignedAlloc(ARCHSAI_DOUBLE, G(PC).ncols, 64, ArchSAI_MEMORY_HOST);

    ARCHSAI_DOUBLE     *tmpb = NULL;
                        if (params.precond == 0) tmpb = ArchSAI_TAlignedAlloc(ARCHSAI_DOUBLE, A.ncols, 64, ArchSAI_MEMORY_HOST);
                        else tmpb = ArchSAI_TAlignedAlloc(ARCHSAI_DOUBLE, G(PC).ncols, 64, ArchSAI_MEMORY_HOST);

    ARCHSAI_DOUBLE     *spmv = NULL;
                        if (params.precond == 3) spmv = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, Gt(PC).ncols, ArchSAI_MEMORY_HOST);

    ARCHSAI_DOUBLE        *r = ArchSAI_TAlignedAlloc(ARCHSAI_DOUBLE,      A.nrows,                 64, ArchSAI_MEMORY_HOST); // Residual

    // For the base construction
    ARCHSAI_DOUBLE         *c = ArchSAI_TAlignedAlloc(ARCHSAI_DOUBLE,      params.sol_aux + 1,     64, ArchSAI_MEMORY_HOST); // Givens Rotator
    ARCHSAI_DOUBLE         *s = ArchSAI_TAlignedAlloc(ARCHSAI_DOUBLE,      params.sol_aux + 1,     64, ArchSAI_MEMORY_HOST); // Givens Rotator
    ARCHSAI_DOUBLE *residuals = ArchSAI_TAlignedAlloc(ARCHSAI_DOUBLE,      params.sol_aux + 1,     64, ArchSAI_MEMORY_HOST); // Residuals

    ARCHSAI_DOUBLE        **H = ArchSAI_TAlloc(       ARCHSAI_DOUBLE*,     params.sol_aux + 1,         ArchSAI_MEMORY_HOST); // Hessenberg matrix
    ARCHSAI_DOUBLE        **v = ArchSAI_TAlloc(       ARCHSAI_DOUBLE*,     params.sol_aux + 1,         ArchSAI_MEMORY_HOST); // Arnoldi vectors

    for (i = 0; i < params.sol_aux + 1; i++){
        H[i] = ArchSAI_TAlignedAlloc(ARCHSAI_DOUBLE, params.sol_aux + 1,    64, ArchSAI_MEMORY_HOST);
        v[i] = ArchSAI_TAlignedAlloc(ARCHSAI_DOUBLE, A.ncols,               64, ArchSAI_MEMORY_HOST);
    }

    // Time measurement
    ARCHSAI_DOUBLE   reptime = 0.0;
    ARCHSAI_DOUBLE  itertime = 0.0;
    ARCHSAI_DOUBLE  besttime = 1E09;

    #if defined ARCHSAI_NEC

        #if defined ARCHSAI_SINGLE
            // A
            sblas_create_matrix_handle_from_csr_rs(A.nrows, A.ncols, A.rowind, A.colind, A.val, SBLAS_INDEXING_0, SBLAS_GENERAL, &(A.sbh));
            sblas_analyze_mv_rs(SBLAS_NON_TRANSPOSE, A.sbh);

            // PC
            if ((params.precond == 1) || (params.precond == 2)){
                sblas_create_matrix_handle_from_csr_rs(M(PC).nrows, M(PC).ncols, M(PC).rowind, M(PC).colind, M(PC).val, SBLAS_INDEXING_0, SBLAS_GENERAL, &(M(PC).sbh));
                sblas_analyze_mv_rs(SBLAS_NON_TRANSPOSE, M(PC).sbh);
            }
            else if (params.precond == 3){
                sblas_create_matrix_handle_from_csr_rs(G(PC).nrows, G(PC).ncols, G(PC).rowind, G(PC).colind, G(PC).val, SBLAS_INDEXING_0, SBLAS_GENERAL, &(G(PC).sbh));
                sblas_analyze_mv_rs(SBLAS_NON_TRANSPOSE, G(PC).sbh);
                sblas_create_matrix_handle_from_csr_rs(Gt(PC).nrows, Gt(PC).ncols, Gt(PC).rowind, Gt(PC).colind, Gt(PC).val, SBLAS_INDEXING_0, SBLAS_GENERAL, &(Gt(PC).sbh));
                sblas_analyze_mv_rs(SBLAS_NON_TRANSPOSE, Gt(PC).sbh);
            }

        #else
            // A
            sblas_create_matrix_handle_from_csr_rd(A.nrows, A.ncols, A.rowind, A.colind, A.val, SBLAS_INDEXING_0, SBLAS_GENERAL, &(A.sbh));
            sblas_analyze_mv_rd(SBLAS_NON_TRANSPOSE, A.sbh);

            // PC
            if ((params.precond == 1) || (params.precond == 2)){
                sblas_create_matrix_handle_from_csr_rd(M(PC).nrows, M(PC).ncols, M(PC).rowind, M(PC).colind, M(PC).val, SBLAS_INDEXING_0, SBLAS_GENERAL, &(M(PC).sbh));
                sblas_analyze_mv_rd(SBLAS_NON_TRANSPOSE, M(PC).sbh);
            }
            else if (params.precond == 3){
                sblas_create_matrix_handle_from_csr_rd(G(PC).nrows, G(PC).ncols, G(PC).rowind, G(PC).colind, G(PC).val, SBLAS_INDEXING_0, SBLAS_GENERAL, &(G(PC).sbh));
                sblas_analyze_mv_rd(SBLAS_NON_TRANSPOSE, G(PC).sbh);
                sblas_create_matrix_handle_from_csr_rd(Gt(PC).nrows, Gt(PC).ncols, Gt(PC).rowind, Gt(PC).colind, Gt(PC).val, SBLAS_INDEXING_0, SBLAS_GENERAL, &(Gt(PC).sbh));
                sblas_analyze_mv_rd(SBLAS_NON_TRANSPOSE, Gt(PC).sbh);
            }
        #endif
    #endif

    /* ****************************************************************************************** */

    /* Begin USER-DEFINED repetitions of solver */


    while (rep < params.reps){

        // Reinitialize used vectors
        ArchSAI_Memset(vector(x),    0,       A.ncols * sizeof(ARCHSAI_DOUBLE),         ArchSAI_MEMORY_HOST);
        if (params.precond == 0) ArchSAI_Memset(tmpr, 0, A.ncols * sizeof(ARCHSAI_DOUBLE), ArchSAI_MEMORY_HOST);
        else ArchSAI_Memset(tmpr, 0, G(PC).ncols * sizeof(ARCHSAI_DOUBLE), ArchSAI_MEMORY_HOST);
        if (params.precond == 0) ArchSAI_Memset(tmpb, 0, A.ncols * sizeof(ARCHSAI_DOUBLE), ArchSAI_MEMORY_HOST);
        else ArchSAI_Memset(tmpb, 0, G(PC).ncols * sizeof(ARCHSAI_DOUBLE), ArchSAI_MEMORY_HOST);

        ArchSAI_Memset(r,     0,       A.nrows * sizeof(ARCHSAI_DOUBLE),         ArchSAI_MEMORY_HOST);

        ArchSAI_Memset(c,     0,       (params.sol_aux + 1) * sizeof(ARCHSAI_DOUBLE),   ArchSAI_MEMORY_HOST);
        ArchSAI_Memset(s,     0,       (params.sol_aux + 1) * sizeof(ARCHSAI_DOUBLE),   ArchSAI_MEMORY_HOST);
        ArchSAI_Memset(residuals,     0,       (params.sol_aux + 1) * sizeof(ARCHSAI_DOUBLE),   ArchSAI_MEMORY_HOST);

        for (i = 0; i < params.sol_aux + 1; i++) {
            ArchSAI_Memset(H[i], 0,   params.sol_aux + 1   * sizeof(ARCHSAI_DOUBLE),    ArchSAI_MEMORY_HOST);
            ArchSAI_Memset(v[i], 0,   A.ncols              * sizeof(ARCHSAI_DOUBLE),    ArchSAI_MEMORY_HOST);
        }

        // INIT RESTARTED GMRES

        // Outer iterations -> Restarting
        reptime = time_getWallclockSeconds();
        itertime = time_getWallclockSeconds();

        // SETUP
        // Check b and get norm_b is preconditioned
        ArchSAI_Copy(b.size, vector(b), int_one, tmpb, int_one);
        ArchSAI_PrecondSpMV(PC, tmpb, tmpr, spmv, nprocs);
        ARCHSAI_DOUBLE norm_b = ArchSAI_Sqrt(ArchSAI_DOTP(A.nrows, tmpr, int_one, tmpr, int_one, A.comm, nprocs)); CHECKNORM(norm_b);

        // Set convergence tolerances
        ARCHSAI_DOUBLE epsilon = ArchSAI_Max(params.abs_tol, params.rel_tol * norm_b);

        /* Begin GMRES Iterations */

        i = 0;
        iter = 0;
        converged = 0;

        while (iter < params.imax){

            // r = M⁻¹(b - Ax)
            ArchSAI_Copy(b.size, vector(b), int_one, tmpr, int_one);
            ArchSAI_CSRMV(trans, matdescra, nprocs, fp_none, A, vector(x), fp_one, tmpr);
            ArchSAI_PrecondSpMV(PC, tmpr, r, spmv, nprocs);

            ARCHSAI_DOUBLE norm_r = ArchSAI_Sqrt(ArchSAI_DOTP(A.nrows, r, int_one, r, int_one, A.comm, nprocs)); /*CHECKNORM(norm_r);*/
            GMRES->residual[rep][iter] = norm_r;
            GMRES->rel_res[rep][iter] = norm_r / norm_b;

            ArchSAI_Memset(residuals, 0, (params.sol_aux + 1) * sizeof(ARCHSAI_DOUBLE), ArchSAI_MEMORY_HOST);
            residuals[0] = norm_r;

            // v = r / norm_r
            ArchSAI_Copy(A.nrows, r, int_one, v[0], int_one);
            ARCHSAI_DOUBLE invnorm_r = 1.0 / norm_r;
            ArchSAI_Scal(A.nrows, invnorm_r, v[0], int_one);

            // Inner Iterations
            i = 0;
            while ((i < params.sol_aux) && (iter < params.imax)){
                i++;
                iter++;

                if (i != 1) itertime = time_getWallclockSeconds();

                /* v[i] = MAv[i-1]*/
                ArchSAI_CSRMV(trans, matdescra, nprocs, fp_one, A, v[i-1], fp_zero, tmpr);
                ArchSAI_PrecondSpMV(PC, tmpr, v[i], spmv, nprocs);

                /* ARNOLDI */
                /* Modified Gram-Schmidt */
                for (j = 0; j < i; j++){
                    // H=vw
                    H[j][i - 1] = ArchSAI_DOTP(A.nrows, v[j], int_one, v[i], int_one, A.comm, nprocs);
                    ARCHSAI_DOUBLE htmp = -H[j][i-1];
                    // v[i] -= Hv
                    ArchSAI_Axpy(A.nrows, htmp, v[j], int_one, v[i], int_one);
                }
                // H = norm(v[i])
                H[i][i - 1] = ArchSAI_Sqrt(ArchSAI_DOTP(A.nrows, v[i], int_one, v[i], int_one, A.comm, nprocs));

                if (H[i][i - 1] != 0.0){
                    ARCHSAI_DOUBLE invh = 1.0 / H[i][i - 1];
                    ArchSAI_Scal(A.nrows, invh, v[i], int_one);
                }

                /* GIVENS ROTATOR */
                for (j = 0; j < i - 1; j++)
                {
                    ApplyPlaneRotation(&H[j][i-1], &H[j+1][i-1], c[j], s[j]);
                }

                GeneratePlaneRotation(H[i-1][i-1], H[i][i-1], &c[i-1], &s[i-1]);
                ApplyPlaneRotation(&H[i-1][i-1], &H[i][i-1], c[i-1], s[i-1]);
                ApplyPlaneRotation(&residuals[i-1], &residuals[i], c[i-1], s[i-1]);

                GMRES->residual[rep][iter] = ArchSAI_Abs(residuals[i]);
                GMRES->rel_res[rep][iter] = GMRES->residual[rep][iter] / GMRES->residual[rep][0];

                /* CHECK CONVERGENCE TO EXIT RESTART CYCLE IF IT IS THE CASE */
                if (GMRES->residual[rep][iter] <= epsilon){
                    converged = 1;
                    break;
                }
                if (i != params.sol_aux) {
                    GMRES->iter_time[rep][iter] = time_getWallclockSeconds() - itertime;
                    itertime = time_getWallclockSeconds();
                }
            }

            // Outer iter
            /* Backsolve */
            Update(&x, i - 1, H, residuals, v);

            GMRES->iter_time[rep][iter] = time_getWallclockSeconds() - itertime;
            itertime = time_getWallclockSeconds();

            if (converged) break;
        }

        GMRES->time[rep] = time_getWallclockSeconds() - reptime;
        GMRES->iter[rep] = iter;

        if (GMRES->time[rep] < besttime){
            besttime = GMRES->time[rep];
            GMRES->bestrep = rep;
        }
        rep++;
    }

    // Free Memory
    ArchSAI_TFree(tmpr,       ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(tmpb,       ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(spmv,       ArchSAI_MEMORY_HOST);

    ArchSAI_TFree(r,          ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(c,          ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(s,          ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(residuals,  ArchSAI_MEMORY_HOST);

    for (int j = 0; j < params.sol_aux + 1; j++){
        ArchSAI_TFree(v[j],   ArchSAI_MEMORY_HOST);
        ArchSAI_TFree(H[j],   ArchSAI_MEMORY_HOST);
    }
    ArchSAI_TFree(v,          ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(H,          ArchSAI_MEMORY_HOST);


    #if defined ARCHSAI_NEC
        // A
        sblas_destroy_matrix_handle(A.sbh);

        // PC
        if (params.precond == 1){
            sblas_destroy_matrix_handle(M(PC).sbh);
        }
        else if (params.precond == 2){
            sblas_destroy_matrix_handle(G(PC).sbh);
            sblas_destroy_matrix_handle(Gt(PC).sbh);
        }

    #endif

    #ifdef ARCHSAI_VERBOSE
        ArchSAI_ParPrintf(A.comm, "\nRep %i: %i %lf\n", GMRES->bestrep, GMRES->iter[GMRES->bestrep], GMRES->time[GMRES->bestrep]);
    #endif // VERBOSE

    return x;
}
