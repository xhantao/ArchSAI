/******************************************************************************
 * Copyright (c) 1998 Lawrence Livermore National Security, LLC and other
 * ArchSAI Project Developers. See the top-level COPYRIGHT file for details.
 *
 * SPDX-License-Identifier: (Apache-2.0 OR MIT)
 ******************************************************************************/

#include "ArchSAI_MPI.h"

/******************************************************************************
 * This routine is the same in both the sequential and normal cases
 *
 * The 'comm' argument for MPI_Comm_f2c is MPI_Fint, which is always the size of
 * a Fortran integer and hence usually the size of ARCHSAI_INT.
 ****************************************************************************/

archsai_MPI_Comm
archsai_MPI_Comm_f2c( ARCHSAI_INT comm )
{
#ifdef ArchSAI_HAVE_MPI_COMM_F2C
   return (archsai_MPI_Comm) MPI_Comm_f2c(comm);
#else
   return (archsai_MPI_Comm) (size_t)comm;
#endif
}

/******************************************************************************
 * MPI stubs to generate serial codes without mpi
 *****************************************************************************/

#ifndef ARCHSAI_USING_MPI

ARCHSAI_INT
archsai_MPI_Init( ARCHSAI_INT   *argc,
                char      ***argv )
{
   return (0);
}

ARCHSAI_INT
archsai_MPI_Finalize( void )
{
   return (0);
}

ARCHSAI_INT
archsai_MPI_Abort( archsai_MPI_Comm comm,
                 ARCHSAI_INT      errorcode )
{
    exit(errorcode);
}

ARCHSAI_DOUBLE
archsai_MPI_Wtime( void )
{
   return (0.0);
}

ARCHSAI_DOUBLE
archsai_MPI_Wtick( void )
{
   return (0.0);
}

ARCHSAI_INT
archsai_MPI_Barrier( archsai_MPI_Comm comm )
{
   return (0);
}

ARCHSAI_INT
archsai_MPI_Comm_create( archsai_MPI_Comm   comm,
                       archsai_MPI_Group  group,
                       archsai_MPI_Comm  *newcomm )
{
   *newcomm = archsai_MPI_COMM_NULL;
   return (0);
}

ARCHSAI_INT
archsai_MPI_Comm_dup( archsai_MPI_Comm  comm,
                    archsai_MPI_Comm *newcomm )
{
   *newcomm = comm;
   return (0);
}

ARCHSAI_INT
archsai_MPI_Comm_size( archsai_MPI_Comm  comm,
                     ARCHSAI_INT      *size )
{
   *size = 1;
   return (0);
}

ARCHSAI_INT
archsai_MPI_Comm_rank( archsai_MPI_Comm  comm,
                     ARCHSAI_INT      *rank )
{
   *rank = 0;
   return (0);
}

ARCHSAI_INT
archsai_MPI_Comm_free( archsai_MPI_Comm *comm )
{
   return 0;
}

ARCHSAI_INT
archsai_MPI_Comm_group( archsai_MPI_Comm   comm,
                      archsai_MPI_Group *group )
{
   return (0);
}

ARCHSAI_INT
archsai_MPI_Comm_split( archsai_MPI_Comm  comm,
                      ARCHSAI_INT       n,
                      ARCHSAI_INT       m,
                      archsai_MPI_Comm *comms )
{
   return (0);
}

ARCHSAI_INT
archsai_MPI_Group_incl( archsai_MPI_Group  group,
                      ARCHSAI_INT        n,
                      ARCHSAI_INT       *ranks,
                      archsai_MPI_Group *newgroup )
{
   return (0);
}

ARCHSAI_INT
archsai_MPI_Group_free( archsai_MPI_Group *group )
{
   return 0;
}

ARCHSAI_INT
archsai_MPI_Address( void           *location,
                   archsai_MPI_Aint *address )
{
   return (0);
}

ARCHSAI_INT
archsai_MPI_Get_count( archsai_MPI_Status   *status,
                     archsai_MPI_Datatype  datatype,
                     ARCHSAI_INT          *count )
{
   return (0);
}

ARCHSAI_INT
archsai_MPI_Alltoall( void               *sendbuf,
                    ARCHSAI_INT           sendcount,
                    archsai_MPI_Datatype  sendtype,
                    void               *recvbuf,
                    ARCHSAI_INT           recvcount,
                    archsai_MPI_Datatype  recvtype,
                    archsai_MPI_Comm      comm )
{
   return (0);
}

ARCHSAI_INT
archsai_MPI_Allgather( void               *sendbuf,
                     ARCHSAI_INT           sendcount,
                     archsai_MPI_Datatype  sendtype,
                     void               *recvbuf,
                     ARCHSAI_INT           recvcount,
                     archsai_MPI_Datatype  recvtype,
                     archsai_MPI_Comm      comm )
{
   ARCHSAI_INT i;

   switch (sendtype)
   {
      case archsai_MPI_INT:
      {
         ARCHSAI_INT *crecvbuf = (ARCHSAI_INT *)recvbuf;
         ARCHSAI_INT *csendbuf = (ARCHSAI_INT *)sendbuf;
         for (i = 0; i < sendcount; i++)
         {
            crecvbuf[i] = csendbuf[i];
         }
      }
      break;

      case archsai_MPI_LONG_LONG_INT:
      {
         ARCHSAI_BIGINT *crecvbuf = (ARCHSAI_BIGINT *)recvbuf;
         ARCHSAI_BIGINT *csendbuf = (ARCHSAI_BIGINT *)sendbuf;
         for (i = 0; i < sendcount; i++)
         {
            crecvbuf[i] = csendbuf[i];
         }
      }
      break;

      case archsai_MPI_FLOAT:
      {
         float *crecvbuf = (float *)recvbuf;
         float *csendbuf = (float *)sendbuf;
         for (i = 0; i < sendcount; i++)
         {
            crecvbuf[i] = csendbuf[i];
         }
      }
      break;

      case archsai_MPI_DOUBLE:
      {
         double *crecvbuf = (double *)recvbuf;
         double *csendbuf = (double *)sendbuf;
         for (i = 0; i < sendcount; i++)
         {
            crecvbuf[i] = csendbuf[i];
         }
      }
      break;

      case archsai_MPI_LONG_DOUBLE:
      {
         long double *crecvbuf = (long double *)recvbuf;
         long double *csendbuf = (long double *)sendbuf;
         for (i = 0; i < sendcount; i++)
         {
            crecvbuf[i] = csendbuf[i];
         }
      }
      break;

      case archsai_MPI_CHAR:
      {
         char *crecvbuf = (char *)recvbuf;
         char *csendbuf = (char *)sendbuf;
         for (i = 0; i < sendcount; i++)
         {
            crecvbuf[i] = csendbuf[i];
         }
      }
      break;

      case archsai_MPI_LONG:
      {
         archsai_longint *crecvbuf = (archsai_longint *)recvbuf;
         archsai_longint *csendbuf = (archsai_longint *)sendbuf;
         for (i = 0; i < sendcount; i++)
         {
            crecvbuf[i] = csendbuf[i];
         }
      }
      break;

      case archsai_MPI_BYTE:
      {
         ArchSAI_TMemcpy(recvbuf, sendbuf, char, sendcount, ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);
      }
      break;

      case MPI_Double:
      {
         ARCHSAI_DOUBLE *crecvbuf = (ARCHSAI_DOUBLE *)recvbuf;
         ARCHSAI_DOUBLE *csendbuf = (ARCHSAI_DOUBLE *)sendbuf;
         for (i = 0; i < sendcount; i++)
         {
            crecvbuf[i] = csendbuf[i];
         }
      }
      break;

//       case archsai_MPI_COMPLEX:
//       {
//          ArchSAI_Complex *crecvbuf = (ArchSAI_Complex *)recvbuf;
//          ArchSAI_Complex *csendbuf = (ArchSAI_Complex *)sendbuf;
//          for (i = 0; i < sendcount; i++)
//          {
//             crecvbuf[i] = csendbuf[i];
//          }
//       }
      break;
   }

   return (0);
}

ARCHSAI_INT
archsai_MPI_Allgatherv( void               *sendbuf,
                      ARCHSAI_INT           sendcount,
                      archsai_MPI_Datatype  sendtype,
                      void               *recvbuf,
                      ARCHSAI_INT          *recvcounts,
                      ARCHSAI_INT          *displs,
                      archsai_MPI_Datatype  recvtype,
                      archsai_MPI_Comm      comm )
{
   return ( archsai_MPI_Allgather(sendbuf, sendcount, sendtype,
                                recvbuf, *recvcounts, recvtype, comm) );
}

ARCHSAI_INT
archsai_MPI_Gather( void               *sendbuf,
                  ARCHSAI_INT           sendcount,
                  archsai_MPI_Datatype  sendtype,
                  void               *recvbuf,
                  ARCHSAI_INT           recvcount,
                  archsai_MPI_Datatype  recvtype,
                  ARCHSAI_INT           root,
                  archsai_MPI_Comm      comm )
{
   return ( archsai_MPI_Allgather(sendbuf, sendcount, sendtype,
                                recvbuf, recvcount, recvtype, comm) );
}

ARCHSAI_INT
archsai_MPI_Gatherv( void              *sendbuf,
                   ARCHSAI_INT           sendcount,
                   archsai_MPI_Datatype  sendtype,
                   void               *recvbuf,
                   ARCHSAI_INT          *recvcounts,
                   ARCHSAI_INT          *displs,
                   archsai_MPI_Datatype  recvtype,
                   ARCHSAI_INT           root,
                   archsai_MPI_Comm      comm )
{
   return ( archsai_MPI_Allgather(sendbuf, sendcount, sendtype,
                                recvbuf, *recvcounts, recvtype, comm) );
}

ARCHSAI_INT
archsai_MPI_Scatter( void               *sendbuf,
                   ARCHSAI_INT           sendcount,
                   archsai_MPI_Datatype  sendtype,
                   void               *recvbuf,
                   ARCHSAI_INT           recvcount,
                   archsai_MPI_Datatype  recvtype,
                   ARCHSAI_INT           root,
                   archsai_MPI_Comm      comm )
{
   return ( archsai_MPI_Allgather(sendbuf, sendcount, sendtype,
                                recvbuf, recvcount, recvtype, comm) );
}

ARCHSAI_INT
archsai_MPI_Scatterv( void               *sendbuf,
                    ARCHSAI_INT           *sendcounts,
                    ARCHSAI_INT           *displs,
                    archsai_MPI_Datatype   sendtype,
                    void                *recvbuf,
                    ARCHSAI_INT            recvcount,
                    archsai_MPI_Datatype   recvtype,
                    ARCHSAI_INT            root,
                    archsai_MPI_Comm       comm )
{
   return ( archsai_MPI_Allgather(sendbuf, *sendcounts, sendtype,
                                recvbuf, recvcount, recvtype, comm) );
}

ARCHSAI_INT
archsai_MPI_Bcast( void               *buffer,
                 ARCHSAI_INT           count,
                 archsai_MPI_Datatype  datatype,
                 ARCHSAI_INT           root,
                 archsai_MPI_Comm      comm )
{
   return (0);
}

ARCHSAI_INT
archsai_MPI_Send( void               *buf,
                ARCHSAI_INT           count,
                archsai_MPI_Datatype  datatype,
                ARCHSAI_INT           dest,
                ARCHSAI_INT           tag,
                archsai_MPI_Comm      comm )
{
   return (0);
}

ARCHSAI_INT
archsai_MPI_Recv( void               *buf,
                ARCHSAI_INT           count,
                archsai_MPI_Datatype  datatype,
                ARCHSAI_INT           source,
                ARCHSAI_INT           tag,
                archsai_MPI_Comm      comm,
                archsai_MPI_Status   *status )
{
   return (0);
}

ARCHSAI_INT
archsai_MPI_Isend( void               *buf,
                 ARCHSAI_INT           count,
                 archsai_MPI_Datatype  datatype,
                 ARCHSAI_INT           dest,
                 ARCHSAI_INT           tag,
                 archsai_MPI_Comm      comm,
                 archsai_MPI_Request  *request )
{
   return (0);
}

ARCHSAI_INT
archsai_MPI_Irecv( void               *buf,
                 ARCHSAI_INT           count,
                 archsai_MPI_Datatype  datatype,
                 ARCHSAI_INT           source,
                 ARCHSAI_INT           tag,
                 archsai_MPI_Comm      comm,
                 archsai_MPI_Request  *request )
{
   return (0);
}

ARCHSAI_INT
archsai_MPI_Send_init( void               *buf,
                     ARCHSAI_INT           count,
                     archsai_MPI_Datatype  datatype,
                     ARCHSAI_INT           dest,
                     ARCHSAI_INT           tag,
                     archsai_MPI_Comm      comm,
                     archsai_MPI_Request  *request )
{
   return 0;
}

ARCHSAI_INT
archsai_MPI_Recv_init( void               *buf,
                     ARCHSAI_INT           count,
                     archsai_MPI_Datatype  datatype,
                     ARCHSAI_INT           dest,
                     ARCHSAI_INT           tag,
                     archsai_MPI_Comm      comm,
                     archsai_MPI_Request  *request )
{
   return 0;
}

ARCHSAI_INT
archsai_MPI_Irsend( void               *buf,
                  ARCHSAI_INT           count,
                  archsai_MPI_Datatype  datatype,
                  ARCHSAI_INT           dest,
                  ARCHSAI_INT           tag,
                  archsai_MPI_Comm      comm,
                  archsai_MPI_Request  *request )
{
   return 0;
}

ARCHSAI_INT
archsai_MPI_Startall( ARCHSAI_INT          count,
                    archsai_MPI_Request *array_of_requests )
{
   return 0;
}

ARCHSAI_INT
archsai_MPI_Probe( ARCHSAI_INT         source,
                 ARCHSAI_INT         tag,
                 archsai_MPI_Comm    comm,
                 archsai_MPI_Status *status )
{
   return 0;
}

ARCHSAI_INT
archsai_MPI_Iprobe( ARCHSAI_INT         source,
                  ARCHSAI_INT         tag,
                  archsai_MPI_Comm    comm,
                  ARCHSAI_INT        *flag,
                  archsai_MPI_Status *status )
{
   return 0;
}

ARCHSAI_INT
archsai_MPI_Test( archsai_MPI_Request *request,
                ARCHSAI_INT         *flag,
                archsai_MPI_Status  *status )
{
   *flag = 1;
   return (0);
}

ARCHSAI_INT
archsai_MPI_Testall( ARCHSAI_INT          count,
                   archsai_MPI_Request *array_of_requests,
                   ARCHSAI_INT         *flag,
                   archsai_MPI_Status  *array_of_statuses )
{
   *flag = 1;
   return (0);
}

ARCHSAI_INT
archsai_MPI_Wait( archsai_MPI_Request *request,
                archsai_MPI_Status  *status )
{
   return (0);
}

ARCHSAI_INT
archsai_MPI_Waitall( ARCHSAI_INT          count,
                   archsai_MPI_Request *array_of_requests,
                   archsai_MPI_Status  *array_of_statuses )
{
   return (0);
}

ARCHSAI_INT
archsai_MPI_Waitany( ARCHSAI_INT          count,
                   archsai_MPI_Request *array_of_requests,
                   ARCHSAI_INT         *index,
                   archsai_MPI_Status  *status )
{
   return (0);
}

ARCHSAI_INT
archsai_MPI_Allreduce( void              *sendbuf,
                     void              *recvbuf,
                     ARCHSAI_INT          count,
                     archsai_MPI_Datatype datatype,
                     archsai_MPI_Op       op,
                     archsai_MPI_Comm     comm )
{
   ARCHSAI_INT i;

   switch (datatype)
   {
      case archsai_MPI_INT:
      {
         ARCHSAI_INT *crecvbuf = (ARCHSAI_INT *)recvbuf;
         ARCHSAI_INT *csendbuf = (ARCHSAI_INT *)sendbuf;
         for (i = 0; i < count; i++)
         {
            crecvbuf[i] = csendbuf[i];
         }
      }
      break;

      case archsai_MPI_LONG_LONG_INT:
      {
         ARCHSAI_BIGINT *crecvbuf = (ARCHSAI_BIGINT *)recvbuf;
         ARCHSAI_BIGINT *csendbuf = (ARCHSAI_BIGINT *)sendbuf;
         for (i = 0; i < count; i++)
         {
            crecvbuf[i] = csendbuf[i];
         }
      }
      break;

      case archsai_MPI_FLOAT:
      {
         float *crecvbuf = (float *)recvbuf;
         float *csendbuf = (float *)sendbuf;
         for (i = 0; i < count; i++)
         {
            crecvbuf[i] = csendbuf[i];
         }
      }
      break;

      case archsai_MPI_DOUBLE:
      {
         double *crecvbuf = (double *)recvbuf;
         double *csendbuf = (double *)sendbuf;
         for (i = 0; i < count; i++)
         {
            crecvbuf[i] = csendbuf[i];
         }
      }
      break;

      case archsai_MPI_LONG_DOUBLE:
      {
         long double *crecvbuf = (long double *)recvbuf;
         long double *csendbuf = (long double *)sendbuf;
         for (i = 0; i < count; i++)
         {
            crecvbuf[i] = csendbuf[i];
         }
      }
      break;

      case archsai_MPI_CHAR:
      {
         char *crecvbuf = (char *)recvbuf;
         char *csendbuf = (char *)sendbuf;
         for (i = 0; i < count; i++)
         {
            crecvbuf[i] = csendbuf[i];
         }
      }
      break;

      case archsai_MPI_LONG:
      {
         archsai_longint *crecvbuf = (archsai_longint *)recvbuf;
         archsai_longint *csendbuf = (archsai_longint *)sendbuf;
         for (i = 0; i < count; i++)
         {
            crecvbuf[i] = csendbuf[i];
         }
      }
      break;

      case archsai_MPI_BYTE:
      {
         ArchSAI_TMemcpy(recvbuf, sendbuf, char, count, ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);
      }
      break;

      case MPI_Double:
      {
         ARCHSAI_DOUBLE *crecvbuf = (ARCHSAI_DOUBLE *)recvbuf;
         ARCHSAI_DOUBLE *csendbuf = (ARCHSAI_DOUBLE *)sendbuf;
         for (i = 0; i < count; i++)
         {
            crecvbuf[i] = csendbuf[i];
         }
      }
      break;

//       case archsai_MPI_COMPLEX:
//       {
//          ArchSAI_Complex *crecvbuf = (ArchSAI_Complex *)recvbuf;
//          ArchSAI_Complex *csendbuf = (ArchSAI_Complex *)sendbuf;
//          for (i = 0; i < count; i++)
//          {
//             crecvbuf[i] = csendbuf[i];
//          }
//       }
      break;
   }

   return 0;
}

ARCHSAI_INT
archsai_MPI_Reduce( void               *sendbuf,
                  void               *recvbuf,
                  ARCHSAI_INT           count,
                  archsai_MPI_Datatype  datatype,
                  archsai_MPI_Op        op,
                  ARCHSAI_INT           root,
                  archsai_MPI_Comm      comm )
{
   archsai_MPI_Allreduce(sendbuf, recvbuf, count, datatype, op, comm);
   return 0;
}

ARCHSAI_INT
archsai_MPI_Scan( void               *sendbuf,
                void               *recvbuf,
                ARCHSAI_INT           count,
                archsai_MPI_Datatype  datatype,
                archsai_MPI_Op        op,
                archsai_MPI_Comm      comm )
{
   archsai_MPI_Allreduce(sendbuf, recvbuf, count, datatype, op, comm);
   return 0;
}

ARCHSAI_INT
archsai_MPI_Request_free( archsai_MPI_Request *request )
{
   return 0;
}

ARCHSAI_INT
archsai_MPI_Type_contiguous( ARCHSAI_INT           count,
                           archsai_MPI_Datatype  oldtype,
                           archsai_MPI_Datatype *newtype )
{
   return (0);
}

ARCHSAI_INT
archsai_MPI_Type_vector( ARCHSAI_INT           count,
                       ARCHSAI_INT           blocklength,
                       ARCHSAI_INT           stride,
                       archsai_MPI_Datatype  oldtype,
                       archsai_MPI_Datatype *newtype )
{
   return (0);
}

ARCHSAI_INT
archsai_MPI_Type_hvector( ARCHSAI_INT           count,
                        ARCHSAI_INT           blocklength,
                        archsai_MPI_Aint      stride,
                        archsai_MPI_Datatype  oldtype,
                        archsai_MPI_Datatype *newtype )
{
   return (0);
}

ARCHSAI_INT
archsai_MPI_Type_struct( ARCHSAI_INT           count,
                       ARCHSAI_INT          *array_of_blocklengths,
                       archsai_MPI_Aint     *array_of_displacements,
                       archsai_MPI_Datatype *array_of_types,
                       archsai_MPI_Datatype *newtype )
{
   return (0);
}

ARCHSAI_INT
archsai_MPI_Type_commit( archsai_MPI_Datatype *datatype )
{
   return (0);
}

ARCHSAI_INT
archsai_MPI_Type_free( archsai_MPI_Datatype *datatype )
{
   return (0);
}

ARCHSAI_INT
archsai_MPI_Op_create( archsai_MPI_User_function *function, ARCHSAI_INT commute, archsai_MPI_Op *op )
{
   return (0);
}

ARCHSAI_INT
archsai_MPI_Op_free( archsai_MPI_Op *op )
{
   return (0);
}

#if defined(ArchSAI_USING_GPU)
ARCHSAI_INT archsai_MPI_Comm_split_type( archsai_MPI_Comm comm, ARCHSAI_INT split_type, ARCHSAI_INT key,
                                     archsai_MPI_Info info, archsai_MPI_Comm *newcomm )
{
   return (0);
}

ARCHSAI_INT archsai_MPI_Info_create( archsai_MPI_Info *info )
{
   return (0);
}

ARCHSAI_INT archsai_MPI_Info_free( archsai_MPI_Info *info )
{
   return (0);
}
#endif

/******************************************************************************
 * MPI stubs to do casting of ARCHSAI_INT and ARCHSAI_INT correctly
 *****************************************************************************/

#else

ARCHSAI_INT
archsai_MPI_Init( ARCHSAI_INT   *argc,
                char      ***argv )
{
   return (ARCHSAI_INT) MPI_Init(argc, argv);
}

ARCHSAI_INT
archsai_MPI_Finalize( void )
{
   return (ARCHSAI_INT) MPI_Finalize();
}

ARCHSAI_INT
archsai_MPI_Abort( archsai_MPI_Comm comm,
                 ARCHSAI_INT      errorcode )
{
   return (ARCHSAI_INT) MPI_Abort(comm, (ARCHSAI_INT)errorcode);
}

ARCHSAI_DOUBLE
archsai_MPI_Wtime( void )
{
   return (ARCHSAI_DOUBLE)MPI_Wtime();
}

ARCHSAI_DOUBLE
archsai_MPI_Wtick( void )
{
   return (ARCHSAI_DOUBLE)MPI_Wtick();
}

ARCHSAI_INT
archsai_MPI_Barrier( archsai_MPI_Comm comm )
{
   return (ARCHSAI_INT) MPI_Barrier(comm);
}

ARCHSAI_INT
archsai_MPI_Comm_create( archsai_MPI_Comm   comm,
                       archsai_MPI_Group  group,
                       archsai_MPI_Comm  *newcomm )
{
   return (ARCHSAI_INT) MPI_Comm_create(comm, group, newcomm);
}

ARCHSAI_INT
archsai_MPI_Comm_dup( archsai_MPI_Comm  comm,
                    archsai_MPI_Comm *newcomm )
{
   return (ARCHSAI_INT) MPI_Comm_dup(comm, newcomm);
}

ARCHSAI_INT
archsai_MPI_Comm_size( archsai_MPI_Comm  comm,
                     ARCHSAI_INT      *size )
{
   ARCHSAI_INT mpi_size;
   ARCHSAI_INT ierr;
   ierr = (ARCHSAI_INT) MPI_Comm_size(comm, &mpi_size);
   *size = (ARCHSAI_INT) mpi_size;
   return ierr;
}

ARCHSAI_INT
archsai_MPI_Comm_rank( archsai_MPI_Comm  comm,
                     ARCHSAI_INT      *rank )
{
   ARCHSAI_INT mpi_rank;
   ARCHSAI_INT ierr;
   ierr = (ARCHSAI_INT) MPI_Comm_rank(comm, &mpi_rank);
   *rank = (ARCHSAI_INT) mpi_rank;
   return ierr;
}

ARCHSAI_INT
archsai_MPI_Comm_free( archsai_MPI_Comm *comm )
{
   return (ARCHSAI_INT) MPI_Comm_free(comm);
}

ARCHSAI_INT
archsai_MPI_Comm_group( archsai_MPI_Comm   comm,
                      archsai_MPI_Group *group )
{
   return (ARCHSAI_INT) MPI_Comm_group(comm, group);
}

ARCHSAI_INT
archsai_MPI_Comm_split( archsai_MPI_Comm  comm,
                      ARCHSAI_INT       n,
                      ARCHSAI_INT       m,
                      archsai_MPI_Comm *comms )
{
   return (ARCHSAI_INT) MPI_Comm_split(comm, (ARCHSAI_INT)n, (ARCHSAI_INT)m, comms);
}

ARCHSAI_INT
archsai_MPI_Group_incl( archsai_MPI_Group  group,
                      ARCHSAI_INT        n,
                      ARCHSAI_INT       *ranks,
                      archsai_MPI_Group *newgroup )
{
   ARCHSAI_INT *mpi_ranks;
   ARCHSAI_INT  i;
   ARCHSAI_INT  ierr;

   mpi_ranks = ArchSAI_TAlloc(ARCHSAI_INT,  n, ArchSAI_MEMORY_HOST);
   for (i = 0; i < n; i++)
   {
      mpi_ranks[i] = (ARCHSAI_INT) ranks[i];
   }
   ierr = (ARCHSAI_INT) MPI_Group_incl(group, (ARCHSAI_INT)n, mpi_ranks, newgroup);
   ArchSAI_TFree(mpi_ranks, ArchSAI_MEMORY_HOST);

   return ierr;
}

ARCHSAI_INT
archsai_MPI_Group_free( archsai_MPI_Group *group )
{
   return (ARCHSAI_INT) MPI_Group_free(group);
}

ARCHSAI_INT
archsai_MPI_Address( void           *location,
                   archsai_MPI_Aint *address )
{
#if MPI_VERSION > 1
   return (ARCHSAI_INT) MPI_Get_address(location, address);
#else
   return (ARCHSAI_INT) MPI_Address(location, address);
#endif
}

ARCHSAI_INT
archsai_MPI_Get_count( archsai_MPI_Status   *status,
                     archsai_MPI_Datatype  datatype,
                     ARCHSAI_INT          *count )
{
   ARCHSAI_INT mpi_count;
   ARCHSAI_INT ierr;
   ierr = (ARCHSAI_INT) MPI_Get_count(status, datatype, &mpi_count);
   *count = (ARCHSAI_INT) mpi_count;
   return ierr;
}

ARCHSAI_INT
archsai_MPI_Alltoall( void               *sendbuf,
                    ARCHSAI_INT           sendcount,
                    archsai_MPI_Datatype  sendtype,
                    void               *recvbuf,
                    ARCHSAI_INT           recvcount,
                    archsai_MPI_Datatype  recvtype,
                    archsai_MPI_Comm      comm )
{
   return (ARCHSAI_INT) MPI_Alltoall(sendbuf, (ARCHSAI_INT)sendcount, sendtype,
                                   recvbuf, (ARCHSAI_INT)recvcount, recvtype, comm);
}

ARCHSAI_INT
archsai_MPI_Allgather( void               *sendbuf,
                     ARCHSAI_INT           sendcount,
                     archsai_MPI_Datatype  sendtype,
                     void               *recvbuf,
                     ARCHSAI_INT           recvcount,
                     archsai_MPI_Datatype  recvtype,
                     archsai_MPI_Comm      comm )
{
   return (ARCHSAI_INT) MPI_Allgather(sendbuf, (ARCHSAI_INT)sendcount, sendtype,
                                    recvbuf, (ARCHSAI_INT)recvcount, recvtype, comm);
}

ARCHSAI_INT
archsai_MPI_Allgatherv( void               *sendbuf,
                      ARCHSAI_INT           sendcount,
                      archsai_MPI_Datatype  sendtype,
                      void               *recvbuf,
                      ARCHSAI_INT          *recvcounts,
                      ARCHSAI_INT          *displs,
                      archsai_MPI_Datatype  recvtype,
                      archsai_MPI_Comm      comm )
{
   ARCHSAI_INT *mpi_recvcounts, *mpi_displs, csize;
   ARCHSAI_INT  i;
   ARCHSAI_INT  ierr;

   MPI_Comm_size(comm, &csize);
   mpi_recvcounts = ArchSAI_TAlloc(ARCHSAI_INT, csize, ArchSAI_MEMORY_HOST);
   mpi_displs = ArchSAI_TAlloc(ARCHSAI_INT, csize, ArchSAI_MEMORY_HOST);
   for (i = 0; i < csize; i++)
   {
      mpi_recvcounts[i] = (ARCHSAI_INT) recvcounts[i];
      mpi_displs[i] = (ARCHSAI_INT) displs[i];
   }
   ierr = (ARCHSAI_INT) MPI_Allgatherv(sendbuf, (ARCHSAI_INT)sendcount, sendtype,
                                     recvbuf, mpi_recvcounts, mpi_displs,
                                     recvtype, comm);
   ArchSAI_TFree(mpi_recvcounts, ArchSAI_MEMORY_HOST);
   ArchSAI_TFree(mpi_displs, ArchSAI_MEMORY_HOST);

   return ierr;
}

ARCHSAI_INT
archsai_MPI_Gather( void               *sendbuf,
                  ARCHSAI_INT           sendcount,
                  archsai_MPI_Datatype  sendtype,
                  void               *recvbuf,
                  ARCHSAI_INT           recvcount,
                  archsai_MPI_Datatype  recvtype,
                  ARCHSAI_INT           root,
                  archsai_MPI_Comm      comm )
{
   return (ARCHSAI_INT) MPI_Gather(sendbuf, (ARCHSAI_INT) sendcount, sendtype,
                                 recvbuf, (ARCHSAI_INT) recvcount, recvtype,
                                 (ARCHSAI_INT)root, comm);
}

ARCHSAI_INT
archsai_MPI_Gatherv(void               *sendbuf,
                  ARCHSAI_INT           sendcount,
                  archsai_MPI_Datatype  sendtype,
                  void               *recvbuf,
                  ARCHSAI_INT          *recvcounts,
                  ARCHSAI_INT          *displs,
                  archsai_MPI_Datatype  recvtype,
                  ARCHSAI_INT           root,
                  archsai_MPI_Comm      comm )
{
   ARCHSAI_INT *mpi_recvcounts = NULL;
   ARCHSAI_INT *mpi_displs = NULL;
   ARCHSAI_INT csize, croot;
   ARCHSAI_INT  i;
   ARCHSAI_INT  ierr;

   MPI_Comm_size(comm, &csize);
   MPI_Comm_rank(comm, &croot);
   if (croot == (ARCHSAI_INT) root)
   {
      mpi_recvcounts = ArchSAI_TAlloc(ARCHSAI_INT,  csize, ArchSAI_MEMORY_HOST);
      mpi_displs = ArchSAI_TAlloc(ARCHSAI_INT,  csize, ArchSAI_MEMORY_HOST);
      for (i = 0; i < csize; i++)
      {
         mpi_recvcounts[i] = (ARCHSAI_INT) recvcounts[i];
         mpi_displs[i] = (ARCHSAI_INT) displs[i];
      }
   }
   ierr = (ARCHSAI_INT) MPI_Gatherv(sendbuf, (ARCHSAI_INT)sendcount, sendtype,
                                  recvbuf, mpi_recvcounts, mpi_displs,
                                  recvtype, (ARCHSAI_INT) root, comm);
   ArchSAI_TFree(mpi_recvcounts, ArchSAI_MEMORY_HOST);
   ArchSAI_TFree(mpi_displs, ArchSAI_MEMORY_HOST);

   return ierr;
}

ARCHSAI_INT
archsai_MPI_Scatter( void               *sendbuf,
                   ARCHSAI_INT           sendcount,
                   archsai_MPI_Datatype  sendtype,
                   void               *recvbuf,
                   ARCHSAI_INT           recvcount,
                   archsai_MPI_Datatype  recvtype,
                   ARCHSAI_INT           root,
                   archsai_MPI_Comm      comm )
{
   return (ARCHSAI_INT) MPI_Scatter(sendbuf, (ARCHSAI_INT)sendcount, sendtype,
                                  recvbuf, (ARCHSAI_INT)recvcount, recvtype,
                                  (ARCHSAI_INT)root, comm);
}

ARCHSAI_INT
archsai_MPI_Scatterv(void               *sendbuf,
                   ARCHSAI_INT          *sendcounts,
                   ARCHSAI_INT          *displs,
                   archsai_MPI_Datatype  sendtype,
                   void               *recvbuf,
                   ARCHSAI_INT           recvcount,
                   archsai_MPI_Datatype  recvtype,
                   ARCHSAI_INT           root,
                   archsai_MPI_Comm      comm )
{
   ARCHSAI_INT *mpi_sendcounts = NULL;
   ARCHSAI_INT *mpi_displs = NULL;
   ARCHSAI_INT csize, croot;
   ARCHSAI_INT  i;
   ARCHSAI_INT  ierr;

   MPI_Comm_size(comm, &csize);
   MPI_Comm_rank(comm, &croot);
   if (croot == (ARCHSAI_INT) root)
   {
      mpi_sendcounts = ArchSAI_TAlloc(ARCHSAI_INT,  csize, ArchSAI_MEMORY_HOST);
      mpi_displs = ArchSAI_TAlloc(ARCHSAI_INT,  csize, ArchSAI_MEMORY_HOST);
      for (i = 0; i < csize; i++)
      {
         mpi_sendcounts[i] = (ARCHSAI_INT) sendcounts[i];
         mpi_displs[i] = (ARCHSAI_INT) displs[i];
      }
   }
   ierr = (ARCHSAI_INT) MPI_Scatterv(sendbuf, mpi_sendcounts, mpi_displs, sendtype,
                                   recvbuf, (ARCHSAI_INT) recvcount,
                                   recvtype, (ARCHSAI_INT) root, comm);
   ArchSAI_TFree(mpi_sendcounts, ArchSAI_MEMORY_HOST);
   ArchSAI_TFree(mpi_displs, ArchSAI_MEMORY_HOST);

   return ierr;
}

ARCHSAI_INT
archsai_MPI_Bcast( void               *buffer,
                 ARCHSAI_INT           count,
                 archsai_MPI_Datatype  datatype,
                 ARCHSAI_INT           root,
                 archsai_MPI_Comm      comm )
{
   return (ARCHSAI_INT) MPI_Bcast(buffer, (ARCHSAI_INT)count, datatype,
                                (ARCHSAI_INT)root, comm);
}

ARCHSAI_INT
archsai_MPI_Send( void               *buf,
                ARCHSAI_INT           count,
                archsai_MPI_Datatype  datatype,
                ARCHSAI_INT           dest,
                ARCHSAI_INT           tag,
                archsai_MPI_Comm      comm )
{
   return (ARCHSAI_INT) MPI_Send(buf, (ARCHSAI_INT)count, datatype,
                               (ARCHSAI_INT)dest, (ARCHSAI_INT)tag, comm);
}

ARCHSAI_INT
archsai_MPI_Recv( void               *buf,
                ARCHSAI_INT           count,
                archsai_MPI_Datatype  datatype,
                ARCHSAI_INT           source,
                ARCHSAI_INT           tag,
                archsai_MPI_Comm      comm,
                archsai_MPI_Status   *status )
{
   return (ARCHSAI_INT) MPI_Recv(buf, (ARCHSAI_INT)count, datatype,
                               (ARCHSAI_INT)source, (ARCHSAI_INT)tag, comm, status);
}

ARCHSAI_INT
archsai_MPI_Isend( void               *buf,
                 ARCHSAI_INT           count,
                 archsai_MPI_Datatype  datatype,
                 ARCHSAI_INT           dest,
                 ARCHSAI_INT           tag,
                 archsai_MPI_Comm      comm,
                 archsai_MPI_Request  *request )
{
   return (ARCHSAI_INT) MPI_Isend(buf, (ARCHSAI_INT)count, datatype,
                                (ARCHSAI_INT)dest, (ARCHSAI_INT)tag, comm, request);
}

ARCHSAI_INT
archsai_MPI_Irecv( void               *buf,
                 ARCHSAI_INT           count,
                 archsai_MPI_Datatype  datatype,
                 ARCHSAI_INT           source,
                 ARCHSAI_INT           tag,
                 archsai_MPI_Comm      comm,
                 archsai_MPI_Request  *request )
{
   return (ARCHSAI_INT) MPI_Irecv(buf, (ARCHSAI_INT)count, datatype,
                                (ARCHSAI_INT)source, (ARCHSAI_INT)tag, comm, request);
}

ARCHSAI_INT
archsai_MPI_Send_init( void               *buf,
                     ARCHSAI_INT           count,
                     archsai_MPI_Datatype  datatype,
                     ARCHSAI_INT           dest,
                     ARCHSAI_INT           tag,
                     archsai_MPI_Comm      comm,
                     archsai_MPI_Request  *request )
{
   return (ARCHSAI_INT) MPI_Send_init(buf, (ARCHSAI_INT)count, datatype,
                                    (ARCHSAI_INT)dest, (ARCHSAI_INT)tag,
                                    comm, request);
}

ARCHSAI_INT
archsai_MPI_Recv_init( void               *buf,
                     ARCHSAI_INT           count,
                     archsai_MPI_Datatype  datatype,
                     ARCHSAI_INT           dest,
                     ARCHSAI_INT           tag,
                     archsai_MPI_Comm      comm,
                     archsai_MPI_Request  *request )
{
   return (ARCHSAI_INT) MPI_Recv_init(buf, (ARCHSAI_INT)count, datatype,
                                    (ARCHSAI_INT)dest, (ARCHSAI_INT)tag,
                                    comm, request);
}

ARCHSAI_INT
archsai_MPI_Irsend( void               *buf,
                  ARCHSAI_INT           count,
                  archsai_MPI_Datatype  datatype,
                  ARCHSAI_INT           dest,
                  ARCHSAI_INT           tag,
                  archsai_MPI_Comm      comm,
                  archsai_MPI_Request  *request )
{
   return (ARCHSAI_INT) MPI_Irsend(buf, (ARCHSAI_INT)count, datatype,
                                 (ARCHSAI_INT)dest, (ARCHSAI_INT)tag, comm, request);
}

ARCHSAI_INT
archsai_MPI_Startall( ARCHSAI_INT          count,
                    archsai_MPI_Request *array_of_requests )
{
   return (ARCHSAI_INT) MPI_Startall((ARCHSAI_INT)count, array_of_requests);
}

ARCHSAI_INT
archsai_MPI_Probe( ARCHSAI_INT         source,
                 ARCHSAI_INT         tag,
                 archsai_MPI_Comm    comm,
                 archsai_MPI_Status *status )
{
   return (ARCHSAI_INT) MPI_Probe((ARCHSAI_INT)source, (ARCHSAI_INT)tag, comm, status);
}

ARCHSAI_INT
archsai_MPI_Iprobe( ARCHSAI_INT         source,
                  ARCHSAI_INT         tag,
                  archsai_MPI_Comm    comm,
                  ARCHSAI_INT        *flag,
                  archsai_MPI_Status *status )
{
   ARCHSAI_INT mpi_flag;
   ARCHSAI_INT ierr;
   ierr = (ARCHSAI_INT) MPI_Iprobe((ARCHSAI_INT)source, (ARCHSAI_INT)tag, comm,
                                 &mpi_flag, status);
   *flag = (ARCHSAI_INT) mpi_flag;
   return ierr;
}

ARCHSAI_INT
archsai_MPI_Test( archsai_MPI_Request *request,
                ARCHSAI_INT         *flag,
                archsai_MPI_Status  *status )
{
   ARCHSAI_INT mpi_flag;
   ARCHSAI_INT ierr;
   ierr = (ARCHSAI_INT) MPI_Test(request, &mpi_flag, status);
   *flag = (ARCHSAI_INT) mpi_flag;
   return ierr;
}

ARCHSAI_INT
archsai_MPI_Testall( ARCHSAI_INT          count,
                   archsai_MPI_Request *array_of_requests,
                   ARCHSAI_INT         *flag,
                   archsai_MPI_Status  *array_of_statuses )
{
   ARCHSAI_INT mpi_flag;
   ARCHSAI_INT ierr;
   ierr = (ARCHSAI_INT) MPI_Testall((ARCHSAI_INT)count, array_of_requests,
                                  &mpi_flag, array_of_statuses);
   *flag = (ARCHSAI_INT) mpi_flag;
   return ierr;
}

ARCHSAI_INT
archsai_MPI_Wait( archsai_MPI_Request *request,
                archsai_MPI_Status  *status )
{
   return (ARCHSAI_INT) MPI_Wait(request, status);
}

ARCHSAI_INT
archsai_MPI_Waitall( ARCHSAI_INT          count,
                   archsai_MPI_Request *array_of_requests,
                   archsai_MPI_Status  *array_of_statuses )
{
   return (ARCHSAI_INT) MPI_Waitall((ARCHSAI_INT)count,
                                  array_of_requests, array_of_statuses);
}

ARCHSAI_INT
archsai_MPI_Waitany( ARCHSAI_INT          count,
                   archsai_MPI_Request *array_of_requests,
                   ARCHSAI_INT         *index,
                   archsai_MPI_Status  *status )
{
   ARCHSAI_INT mpi_index;
   ARCHSAI_INT ierr;
   ierr = (ARCHSAI_INT) MPI_Waitany((ARCHSAI_INT)count, array_of_requests,
                                  &mpi_index, status);
   *index = (ARCHSAI_INT) mpi_index;
   return ierr;
}

ARCHSAI_INT
archsai_MPI_Allreduce( void              *sendbuf,
                     void              *recvbuf,
                     ARCHSAI_INT          count,
                     archsai_MPI_Datatype datatype,
                     archsai_MPI_Op       op,
                     archsai_MPI_Comm     comm )
{
#if defined(ArchSAI_USING_NVTX)
   archsai_GpuProfilingPushRange("MPI_Allreduce");
#endif

   ARCHSAI_INT result = MPI_Allreduce(sendbuf, recvbuf, (ARCHSAI_INT)count,
                                    datatype, op, comm);

#if defined(ArchSAI_USING_NVTX)
   archsai_GpuProfilingPopRange();
#endif

   return result;
}

ARCHSAI_INT
archsai_MPI_Reduce( void               *sendbuf,
                  void               *recvbuf,
                  ARCHSAI_INT           count,
                  archsai_MPI_Datatype  datatype,
                  archsai_MPI_Op        op,
                  ARCHSAI_INT           root,
                  archsai_MPI_Comm      comm )
{
   return (ARCHSAI_INT) MPI_Reduce(sendbuf, recvbuf, (ARCHSAI_INT)count,
                                 datatype, op, (ARCHSAI_INT)root, comm);
}

ARCHSAI_INT
archsai_MPI_Scan( void               *sendbuf,
                void               *recvbuf,
                ARCHSAI_INT           count,
                archsai_MPI_Datatype  datatype,
                archsai_MPI_Op        op,
                archsai_MPI_Comm      comm )
{
   return (ARCHSAI_INT) MPI_Scan(sendbuf, recvbuf, (ARCHSAI_INT)count,
                               datatype, op, comm);
}

ARCHSAI_INT
archsai_MPI_Request_free( archsai_MPI_Request *request )
{
   return (ARCHSAI_INT) MPI_Request_free(request);
}

ARCHSAI_INT
archsai_MPI_Type_contiguous( ARCHSAI_INT           count,
                           archsai_MPI_Datatype  oldtype,
                           archsai_MPI_Datatype *newtype )
{
   return (ARCHSAI_INT) MPI_Type_contiguous((ARCHSAI_INT)count, oldtype, newtype);
}

ARCHSAI_INT
archsai_MPI_Type_vector( ARCHSAI_INT           count,
                       ARCHSAI_INT           blocklength,
                       ARCHSAI_INT           stride,
                       archsai_MPI_Datatype  oldtype,
                       archsai_MPI_Datatype *newtype )
{
   return (ARCHSAI_INT) MPI_Type_vector((ARCHSAI_INT)count, (ARCHSAI_INT)blocklength,
                                      (ARCHSAI_INT)stride, oldtype, newtype);
}

ARCHSAI_INT
archsai_MPI_Type_hvector( ARCHSAI_INT           count,
                        ARCHSAI_INT           blocklength,
                        archsai_MPI_Aint      stride,
                        archsai_MPI_Datatype  oldtype,
                        archsai_MPI_Datatype *newtype )
{
#if MPI_VERSION > 1
   return (ARCHSAI_INT) MPI_Type_create_hvector((ARCHSAI_INT)count, (ARCHSAI_INT)blocklength,
                                              stride, oldtype, newtype);
#else
   return (ARCHSAI_INT) MPI_Type_hvector((ARCHSAI_INT)count, (ARCHSAI_INT)blocklength,
                                       stride, oldtype, newtype);
#endif
}

ARCHSAI_INT
archsai_MPI_Type_struct( ARCHSAI_INT           count,
                       ARCHSAI_INT          *array_of_blocklengths,
                       archsai_MPI_Aint     *array_of_displacements,
                       archsai_MPI_Datatype *array_of_types,
                       archsai_MPI_Datatype *newtype )
{
   ARCHSAI_INT *mpi_array_of_blocklengths;
   ARCHSAI_INT  i;
   ARCHSAI_INT  ierr;

   mpi_array_of_blocklengths = ArchSAI_TAlloc(ARCHSAI_INT,  count, ArchSAI_MEMORY_HOST);
   for (i = 0; i < count; i++)
   {
      mpi_array_of_blocklengths[i] = (ARCHSAI_INT) array_of_blocklengths[i];
   }

#if MPI_VERSION > 1
   ierr = (ARCHSAI_INT) MPI_Type_create_struct((ARCHSAI_INT)count, mpi_array_of_blocklengths,
                                             array_of_displacements, array_of_types,
                                             newtype);
#else
   ierr = (ARCHSAI_INT) MPI_Type_struct((ARCHSAI_INT)count, mpi_array_of_blocklengths,
                                      array_of_displacements, array_of_types,
                                      newtype);
#endif

   ArchSAI_TFree(mpi_array_of_blocklengths, ArchSAI_MEMORY_HOST);

   return ierr;
}

ARCHSAI_INT
archsai_MPI_Type_commit( archsai_MPI_Datatype *datatype )
{
   return (ARCHSAI_INT) MPI_Type_commit(datatype);
}

ARCHSAI_INT
archsai_MPI_Type_free( archsai_MPI_Datatype *datatype )
{
   return (ARCHSAI_INT) MPI_Type_free(datatype);
}

ARCHSAI_INT
archsai_MPI_Op_free( archsai_MPI_Op *op )
{
   return (ARCHSAI_INT) MPI_Op_free(op);
}

ARCHSAI_INT
archsai_MPI_Op_create( archsai_MPI_User_function *function, ARCHSAI_INT commute, archsai_MPI_Op *op )
{
   return (ARCHSAI_INT) MPI_Op_create(function, commute, op);
}

#if defined(ArchSAI_USING_GPU)
ARCHSAI_INT
archsai_MPI_Comm_split_type( archsai_MPI_Comm comm, ARCHSAI_INT split_type, ARCHSAI_INT key,
                           archsai_MPI_Info info, archsai_MPI_Comm *newcomm )
{
   return (ARCHSAI_INT) MPI_Comm_split_type(comm, split_type, key, info, newcomm );
}

ARCHSAI_INT
archsai_MPI_Info_create( archsai_MPI_Info *info )
{
   return (ARCHSAI_INT) MPI_Info_create(info);
}

ARCHSAI_INT
archsai_MPI_Info_free( archsai_MPI_Info *info )
{
   return (ARCHSAI_INT) MPI_Info_free(info);
}
#endif

#endif
