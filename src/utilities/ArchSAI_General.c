
 /*! \file ArchSAI_General.c
    \brief Source file for the general functions in ArchSAI.

    ArchSAI general functions source file.
*/

#include <ArchSAI_General.h>


ARCHSAI_INT ArchSAI_Init(ARCHSAI_INT *argc, ARCHSAI_CHAR** argv[]){

    MPI_Init(argc, argv);

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    #ifdef ARCHSAI_TIMING
    ArchSAI_InitializeTiming("ArchSAI");
    ArchSAI_BeginTiming(0);
    #endif

    #ifdef ARCHSAI_VERBOSE
        ArchSAI_ParPrintf(MPI_COMM_WORLD, "\n************** ArchSAI Init **************\n");
        ArchSAI_ParPrintf(MPI_COMM_WORLD, "\nInitializing ArchSAI\n");
        ArchSAI_ParPrintf(MPI_COMM_WORLD, "        | Processes:\t%i\n", nprocs);
        ArchSAI_ParPrintf(MPI_COMM_WORLD, "        | Threads:\t%i\n", omp_get_max_threads());
        fflush(stdout);
        MPI_Barrier(MPI_COMM_WORLD);
    #endif

    return archsai_error_flag;
}

ARCHSAI_INT ArchSAI_End(void){

    MPI_Barrier(MPI_COMM_WORLD);

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    #ifdef ARCHSAI_VERBOSE
        ArchSAI_ParPrintf(MPI_COMM_WORLD, "\n************** ArchSAI End  **************\n\n");
        fflush(stdout);
        MPI_Barrier(MPI_COMM_WORLD);
    #endif

    MPI_Barrier(MPI_COMM_WORLD);

    #ifdef ARCHSAI_TIMING
    ArchSAI_EndTiming(0);
    #ifdef ARCHSAI_VERBOSE
    ArchSAI_PrintTiming("APPLICATION TIMES", MPI_COMM_WORLD);
    #endif
    ArchSAI_FinalizeAllTimings();
    ArchSAI_ClearTiming();
    #endif

    MPI_Barrier(MPI_COMM_WORLD);

    MPI_Finalize();

    return archsai_error_flag;
}
