#include <ArchSAI_Vector.h>

ARCHSAI_VEC ArchSAI_DAllocCreateVecfromArray(ARCHSAI_DOUBLE *readvec, ARCHSAI_INT size, ARCHSAI_INT gsize){

    ARCHSAI_VEC ptr;

    ptr.gsize = gsize;
    ptr.size  = size;
    ptr.val   = ArchSAI_TAlloc(ARCHSAI_DOUBLE, ptr.size, ArchSAI_MEMORY_HOST);

    ARCHSAI_INT i;

    for (i = 0; i < size; i++){
        ptr.val[i] = readvec[i];
    }

    return ptr;
}
