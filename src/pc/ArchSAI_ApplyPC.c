#include <ArchSAI_ApplyPC.h>

const ARCHSAI_INT    one = 1;

void ArchSAI_PrecondSpMV(ARCHSAI_PRECOND PC, ARCHSAI_DOUBLE *In, ARCHSAI_DOUBLE *Out, ARCHSAI_DOUBLE *tmp, ARCHSAI_INT nprocs){

    ARCHSAI_CHAR trans[1] = "N";
    ARCHSAI_CHAR matdescra[4] = "GLNC";
    ARCHSAI_DOUBLE fp_one = 1.0;
    ARCHSAI_DOUBLE fp_zero = 0.0;

    if (PC.type == 0)
        ArchSAI_Copy(PC.sys_size, In, one, Out, one);
    if (PC.type == 1){
        ArchSAI_CSRMV(trans, matdescra, nprocs, fp_one, M(PC), In, fp_zero, Out);
    }
    if (PC.type == 2){
        ArchSAI_CSRMV(trans, matdescra, nprocs, fp_one, M(PC), In, fp_zero, Out);
    }
    if (PC.type == 3){
        ArchSAI_CSRMV(trans, matdescra, nprocs, fp_one, G(PC), In, fp_zero, tmp);
        ArchSAI_CSRMV(trans, matdescra, nprocs, fp_one, Gt(PC), tmp, fp_zero, Out);
    }
}

