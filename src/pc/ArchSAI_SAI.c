#include <ArchSAI_SAI.h>
#include <omp.h>

ARCHSAI_PRECOND ArchSAI_SAI(ARCHSAI_MATCSR A, ARCHSAI_PARAMS params){

    ARCHSAI_PRECOND PC;
    ARCHSAI_MATCSR pattern;

    ArchSAI_GetPattern(A, &pattern, params);

    switch(params.ext_sel){
        case 0:
            break;
        case 1:
            ArchSAI_ExtendPatternSpatTemp(A, &pattern, params);
            break;
        case 2:
            ArchSAI_ExtendPatternSpatTemp_SingleStep(A, &pattern, params);
            break;
        case 3:
            ArchSAI_ExtendPatternSpatTemp_Reference(A, &pattern, params);
            break;
        case 4:
            ArchSAI_ExtendPatternSpatTemp_Reference_SingleStep(A, &pattern, params);
            break;
        default:
            break;
    }

    ArchSAI_ComputeSAI(A, &pattern);

    PC.type = params.precond;
    PC.sys_size = A.ncols;
    PC.mat = pattern;
    return PC;
}

ARCHSAI_PRECOND ArchSAI_FSAI(ARCHSAI_MATCSR A, ARCHSAI_PARAMS params){

    ARCHSAI_PRECOND PC;
    ARCHSAI_MATCSR pattern;

    ArchSAI_GetPattern(A, &pattern, params);

    switch(params.ext_sel){
        case 0:
            break;
        case 1:
            ArchSAI_ExtendPatternSpatTemp(A, &pattern, params);
            break;
        case 2:
            ArchSAI_ExtendPatternSpatTemp_SingleStep(A, &pattern, params);
            break;
        case 3:
            ArchSAI_ExtendPatternSpatTemp_Reference(A, &pattern, params);
            break;
        case 4:
            ArchSAI_ExtendPatternSpatTemp_Reference_SingleStep(A, &pattern, params);
            break;
        default:
            break;
    }

    // COMPUTE SAI

    ArchSAI_ComputeFSAI(A, &pattern);

    // BUILD PC

    PC.type = params.precond;
    PC.sys_size = A.ncols;
    PC.mat = pattern;
    PC.mattransp = ArchSAI_CreateTransposedMatCSR(pattern);
    return PC;
}



ARCHSAI_INT ArchSAI_ComputeFSAI(ARCHSAI_MATCSR A, ARCHSAI_MATCSR *pattern){

    if (pattern->fulldiag == 0){
        ArchSAI_ParPrintf(pattern->comm, "Error: Can not compute FSAI on a non full diagonal pattern.\n");
        ArchSAI_Error(1);
    }

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(pattern->comm, &rank);
    MPI_Comm_size(pattern->comm, &nprocs);

    ARCHSAI_INT *gl_rows = NULL;
    ARCHSAI_MATCSR outer_A = ArchSAI_OuterMatrix(*pattern, A, &gl_rows);

    /* NOW I HAVE THE OUTER MAT, WHICH I NEED TO CREATE THE LOCAL RESTRICTED MAT */
    /* OUTER MAT HAS GLOBAL INDICES */

    ArchSAI_ToGlobalMatCSR(pattern);

    if (pattern->fval) ArchSAI_TFree(pattern->fval, ArchSAI_MEMORY_HOST);
    if (pattern->val) ArchSAI_TFree(pattern->val, ArchSAI_MEMORY_HOST);
    pattern->fval   = NULL;
    pattern->val    = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, pattern->nnz, ArchSAI_MEMORY_HOST);

    ARCHSAI_INT maxrow = 0;

    for (int i = 0; i < pattern->nrows;i++) {if (pattern->rowind[i + 1] - pattern->rowind[i] > maxrow) maxrow = pattern->rowind[i + 1] - pattern->rowind[i];}

    #pragma omp parallel
    {
        ARCHSAI_DOUBLE *a = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, maxrow * maxrow, ArchSAI_MEMORY_HOST);
        ARCHSAI_DOUBLE *b = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, maxrow, ArchSAI_MEMORY_HOST);

        #pragma omp for
        for (int i = 0; i < pattern->nrows; i++){
            int rowelems = pattern->rowind[i + 1] - pattern->rowind[i];
            int n = rowelems, nrhs = 1, lda = rowelems, ldb = rowelems, info;
            int ipiv[rowelems];
            int searchpos = 0;
            ArchSAI_Memset(a, 0, maxrow * maxrow * sizeof(ARCHSAI_DOUBLE), ArchSAI_MEMORY_HOST);
            ArchSAI_Memset(b, 0, maxrow * sizeof(ARCHSAI_DOUBLE), ArchSAI_MEMORY_HOST);

            for (int j = 0; j < rowelems; j++){
                searchpos = j;
                ARCHSAI_INT start = 0;

                ARCHSAI_INT row = pattern->colind[pattern->rowind[i] + j];
                if (pattern->rowdist[row] == rank){     // We find a column entry. Corresponding row is local.
                    for (int k = j; k < rowelems; k++){
                        start = 0;
                        ARCHSAI_INT col = pattern->colind[pattern->rowind[i] + k];
                        a[j * rowelems + k] = (ARCHSAI_DOUBLE) ArchSAI_GetValueMatCSR_nolim(&start, A.gl2loc[row], A.gl2loc[col], A);
                        a[k * rowelems + j] = a[j * rowelems + k];
                    }
                }
                else {                                  // We have a column entry. Corresponding row is halo.
					ARCHSAI_INT lastrow = 0;
                    for (int k = j; k < rowelems; k++){
                        ARCHSAI_INT col = pattern->colind[pattern->rowind[i] + k];
                        ARCHSAI_INT outer_row = 0;
                        for (outer_row = lastrow; outer_row < outer_A.size; outer_row++){
                            if (gl_rows[outer_row] == row) {
								lastrow = outer_row;
								break;
							}
                        }
                        start = 0;
                        a[j * rowelems + k] = (ARCHSAI_DOUBLE) ArchSAI_GetValueMatCSR_nolim(&start, outer_row, col, outer_A);
                        a[k * rowelems + j] = a[j * rowelems + k];
                    }
                }
            }
            b[rowelems - 1] = 1.0;

            ArchSAI_DGESV(LAPACK_COL_MAJOR, rowelems, nrhs, a, lda, ipiv, b, ldb);

            ArchSAI_TMemcpy(pattern->val + pattern->rowind[i], b, ARCHSAI_DOUBLE, rowelems, ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);
        }
        ArchSAI_TFree(a, ArchSAI_MEMORY_HOST);
        ArchSAI_TFree(b, ArchSAI_MEMORY_HOST);
    }

    ArchSAI_ToLocalMatCSR(pattern);

    ARCHSAI_INT inzero = 0;
    ARCHSAI_DOUBLE diag;
    #pragma omp parallel for private(diag, inzero)
    for (int i = 0; i < pattern->nrows; i++){
        ARCHSAI_DOUBLE val = 0.0;
        for (int j = pattern->rowind[i]; j < pattern->rowind[i + 1]; j++){
            if (pattern->loc2gl[pattern->colind[j]] == pattern->loc2gl[i]){
                val = pattern->val[j];
                break;
            }
        }

        inzero = 0;
        diag = 1.0/ArchSAI_Sqrt(ArchSAI_Abs(val));
        for (int j = pattern->rowind[i]; j < pattern->rowind[i + 1]; j++){
            pattern->val[j] = pattern->val[j] * diag;
        }
    }

    ArchSAI_TFree(gl_rows, ArchSAI_MEMORY_HOST);
    ArchSAI_DestroyMatCSR(&outer_A, ArchSAI_MEMORY_HOST);

    return archsai_error_flag;
}







ARCHSAI_INT ArchSAI_ComputeFSAIApprox(ARCHSAI_MATCSR A, ARCHSAI_MATCSR *pattern){

    if (pattern->fulldiag == 0){
        ArchSAI_ParPrintf(pattern->comm, "Error: Can not compute FSAI on a non full diagonal pattern.\n");
        ArchSAI_Error(1);
    }

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(pattern->comm, &rank);
    MPI_Comm_size(pattern->comm, &nprocs);

    ARCHSAI_INT *gl_rows = NULL;
    ARCHSAI_MATCSR outer_A = ArchSAI_OuterMatrix(*pattern, A, &gl_rows);

    /* NOW I HAVE THE OUTER MAT, WHICH I NEED TO CREATE THE LOCAL RESTRICTED MAT */
    /* OUTER MAT HAS GLOBAL INDICES */

    ArchSAI_ToGlobalMatCSR(pattern);

    if (pattern->fval) ArchSAI_TFree(pattern->fval, ArchSAI_MEMORY_HOST);
    pattern->fval    = ArchSAI_CTAlloc(ARCHSAI_FLOAT, pattern->nnz, ArchSAI_MEMORY_HOST);

    ARCHSAI_INT maxrow = 0;

    for (int i = 0; i < pattern->nrows;i++) {if (pattern->rowind[i + 1] - pattern->rowind[i] > maxrow) maxrow = pattern->rowind[i + 1] - pattern->rowind[i];}

    #pragma omp parallel
    {
        ARCHSAI_FLOAT *a = ArchSAI_CTAlloc(ARCHSAI_FLOAT, maxrow * maxrow, ArchSAI_MEMORY_HOST);
        ARCHSAI_FLOAT *b = ArchSAI_CTAlloc(ARCHSAI_FLOAT, maxrow, ArchSAI_MEMORY_HOST);

        #pragma omp for
        for (int i = 0; i < pattern->nrows; i++){
            int rowelems = pattern->rowind[i + 1] - pattern->rowind[i];
            int n = rowelems, nrhs = 1, lda = rowelems, ldb = rowelems, info;
            int ipiv[rowelems];
            int searchpos = 0;
            ArchSAI_Memset(a, 0, rowelems * rowelems * sizeof(ARCHSAI_FLOAT), ArchSAI_MEMORY_HOST);
            ArchSAI_Memset(b, 0, rowelems * sizeof(ARCHSAI_FLOAT), ArchSAI_MEMORY_HOST);

            for (int j = 0; j < rowelems; j++){
                searchpos = j;
                ARCHSAI_INT start = 0;

                ARCHSAI_INT row = pattern->colind[pattern->rowind[i] + j];
                if (pattern->rowdist[row] == rank){     // We find a column entry. Corresponding row is local.
                    for (int k = j; k < rowelems; k++){
                        ARCHSAI_INT col = pattern->colind[pattern->rowind[i] + k];
                        start = 0;
                        a[j * rowelems + k] = (ARCHSAI_FLOAT) ArchSAI_GetValueMatCSR_nolim(&start, A.gl2loc[row], A.gl2loc[col], A);
                        a[k * rowelems + j] = a[j * rowelems + k];
                    }
                }
                else {                                  // We have a column entry. Corresponding row is halo.
					ARCHSAI_INT lastrow = 0;
                    for (int k = j; k < rowelems; k++){
                        ARCHSAI_INT col = pattern->colind[pattern->rowind[i] + k];
                        ARCHSAI_INT outer_row = 0;
                        for (outer_row = lastrow; outer_row < outer_A.size; outer_row++){
                            if (gl_rows[outer_row] == row) {
								lastrow = outer_row;
								break;
							}
                        }
                        start = 0;
                        a[j * rowelems + k] = (ARCHSAI_FLOAT) ArchSAI_GetValueMatCSR_nolim(&start, outer_row, col, outer_A);
                        a[k * rowelems + j] = a[j * rowelems + k];
                    }
                }
            }
            b[rowelems - 1] = 1.0;

            ArchSAI_SGESV(LAPACK_COL_MAJOR, rowelems, nrhs, a, lda, ipiv, b, ldb);

            ArchSAI_TMemcpy(pattern->fval + pattern->rowind[i], b, ARCHSAI_FLOAT, rowelems, ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);
        }
        ArchSAI_TFree(a, ArchSAI_MEMORY_HOST);
        ArchSAI_TFree(b, ArchSAI_MEMORY_HOST);
    }

    ArchSAI_ToLocalMatCSR(pattern);

    ARCHSAI_INT inzero = 0;
    ARCHSAI_FLOAT diag;
    #pragma omp parallel for private(diag, inzero)
    for (int i = 0; i < pattern->nrows; i++){
        ARCHSAI_FLOAT fval = 0.0;
        for (int j = pattern->rowind[i]; j < pattern->rowind[i + 1]; j++){
            if (pattern->loc2gl[pattern->colind[j]] == pattern->loc2gl[i]){
                fval = pattern->fval[j];
                break;
            }
        }

        inzero = 0;
        diag = 1.0/ArchSAI_Sqrt(ArchSAI_Abs(fval));
        for (int j = pattern->rowind[i]; j < pattern->rowind[i + 1]; j++){
            pattern->fval[j] = pattern->fval[j] * diag;
        }
    }

    ArchSAI_TFree(gl_rows, ArchSAI_MEMORY_HOST);
    ArchSAI_DestroyMatCSR(&outer_A, ArchSAI_MEMORY_HOST);

    return archsai_error_flag;
}


ARCHSAI_INT ArchSAI_ComputeSAIApprox(ARCHSAI_MATCSR A, ARCHSAI_MATCSR *pattern){

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(pattern->comm, &rank);
    MPI_Comm_size(pattern->comm, &nprocs);

    /* How many rows should every process send and receive */

	ARCHSAI_INT		*proc_rows_recv = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs, ArchSAI_MEMORY_HOST),
                    *proc_rows_send = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs, ArchSAI_MEMORY_HOST);
	ARCHSAI_CHAR    *row_use = ArchSAI_CTAlloc(ARCHSAI_CHAR, pattern->gnrows, ArchSAI_MEMORY_HOST);

	for (int i = 0; i < pattern->nrows; i++){
		for (int j = pattern->rowind[i]; j < pattern->rowind[i + 1]; j++){
			row_use[pattern->loc2gl[pattern->colind[j]]] = 1;
		}
	}

	for (int i = 0; i < pattern->gnrows; i++){
		if ((row_use[i] == 1) && (pattern->rowdist[i] != rank)){
			proc_rows_recv[pattern->rowdist[i]]++;
		}
	}

	MPI_Alltoall(proc_rows_recv, 1, ARCHSAI_MPI_INT, proc_rows_send, 1, ARCHSAI_MPI_INT, pattern->comm);

	/* Obtain indices of rows that I need */

	ARCHSAI_INT		*torecv_ptrs = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs + 1, ArchSAI_MEMORY_HOST),
                    *tosend_ptrs = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs + 1, ArchSAI_MEMORY_HOST),
                    *counter_procs = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs + 1, ArchSAI_MEMORY_HOST);

	for (int i = 0; i < nprocs; i++){
		torecv_ptrs[i + 1] = torecv_ptrs[i] + proc_rows_recv[i];
	}

	ARCHSAI_INT		*torecv_indices = ArchSAI_CTAlloc(ARCHSAI_INT, torecv_ptrs[nprocs], ArchSAI_MEMORY_HOST);

	for (int i = 0; i < pattern->gnrows; i++){
		if ((row_use[i] == 1) && (pattern->rowdist[i] != rank)){
			torecv_indices[torecv_ptrs[pattern->rowdist[i]] + counter_procs[pattern->rowdist[i]]] = i;
			counter_procs[pattern->rowdist[i]]++;
		}
	}

	for (int i = 0; i < nprocs; i++){
		tosend_ptrs[i + 1] = tosend_ptrs[i] + proc_rows_send[i];
	}

    /* Communications of row indices to know what indices have to be sent */
	ARCHSAI_INT		*tosend_indices = ArchSAI_CTAlloc(ARCHSAI_INT, tosend_ptrs[nprocs], ArchSAI_MEMORY_HOST);

    MPI_Request     *req = ArchSAI_TAlloc(MPI_Request, 2 * nprocs, ArchSAI_MEMORY_HOST);
    ARCHSAI_INT     counter = 0;

	for (int i = 0; i < nprocs; i++){
		if (proc_rows_send[i] > 0) {
			MPI_Irecv(tosend_indices + tosend_ptrs[i], tosend_ptrs[i + 1] - tosend_ptrs[i], ARCHSAI_MPI_INT, i, 0, pattern->comm, &req[counter]);
			counter++;
		}
	}
	for (int i = 0; i < nprocs; i++){
		if (proc_rows_recv[i] > 0) {
			MPI_Isend(torecv_indices + torecv_ptrs[i], torecv_ptrs[i + 1] - torecv_ptrs[i], ARCHSAI_MPI_INT, i, 0, pattern->comm, &req[counter]);
			counter++;
		}
	}
	MPI_Waitall(counter, req, MPI_STATUSES_IGNORE);

    /* Send sizes of rows to create buffers */

    ARCHSAI_INT *send_sizes = ArchSAI_CTAlloc(ARCHSAI_INT, tosend_ptrs[nprocs], ArchSAI_MEMORY_HOST);

	for (int i = 0; i < nprocs; i++){
		for (int j = tosend_ptrs[i]; j < tosend_ptrs[i + 1]; j++){
			send_sizes[j] += (A.rowind[A.gl2loc[tosend_indices[j]] + 1] - A.rowind[A.gl2loc[tosend_indices[j]]]);
		}
	}

	/* We create a local temporary matrix to store the outer rows */

    ARCHSAI_MATCSR outer_A;
    ArchSAI_InitMatCSR(&outer_A);

    outer_A.rowind = ArchSAI_CTAlloc(ARCHSAI_INT, torecv_ptrs[nprocs] + 1, ArchSAI_MEMORY_HOST);
    outer_A.size   = torecv_ptrs[nprocs];

	counter = 0;
	for (int i = 0; i < nprocs; i++){
		if (proc_rows_recv[i] > 0) {
			MPI_Irecv(outer_A.rowind + torecv_ptrs[i], torecv_ptrs[i + 1] - torecv_ptrs[i], ARCHSAI_MPI_INT, i, 0, pattern->comm, &req[counter]);
			counter++;
		}
	}

	for (int i = 0; i < nprocs; i++){
		if (proc_rows_send[i] > 0) {
			MPI_Isend(send_sizes + tosend_ptrs[i], tosend_ptrs[i + 1] - tosend_ptrs[i], ARCHSAI_MPI_INT, i, 0, pattern->comm, &req[counter]);
			counter++;
		}
	}

	MPI_Waitall(counter, req, MPI_STATUSES_IGNORE);

    ARCHSAI_INT nextval = 0,
                prevval = 0;

	for (int i = 0; i < outer_A.size + 1; i++){
		nextval = prevval + outer_A.rowind[i];
		outer_A.rowind[i] = prevval;
		prevval = nextval;
	}

	outer_A.nnz = outer_A.rowind[outer_A.size];

    /* Now I know recv index sizes so we build a buffer */

    outer_A.colind = ArchSAI_CTAlloc(ARCHSAI_INT, outer_A.nnz, ArchSAI_MEMORY_HOST);
    outer_A.val = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, outer_A.nnz, ArchSAI_MEMORY_HOST);

        /* We will transfer columns and values at the same communication step with a buffer */
    ARCHSAI_DOUBLE *recvVCbuffer = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, outer_A.nnz * 2, ArchSAI_MEMORY_HOST);

    ARCHSAI_INT sizesendbuffer = 0;
    for (int i = 0; i < tosend_ptrs[nprocs]; i++) sizesendbuffer += send_sizes[i];

    ARCHSAI_DOUBLE *sendVCbuffer = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, sizesendbuffer * 2, ArchSAI_MEMORY_HOST);
	ARCHSAI_INT *sendVCbuffer_ptr = ArchSAI_CTAlloc(ARCHSAI_INT, tosend_ptrs[nprocs] + 1, ArchSAI_MEMORY_HOST);

    ARCHSAI_INT position = 0;

	for (int i = 0; i < tosend_ptrs[nprocs]; i++){
		for (int j = A.rowind[A.gl2loc[tosend_indices[i]]]; j < A.rowind[A.gl2loc[tosend_indices[i]] + 1]; j++){
			sendVCbuffer[position] = (double) A.loc2gl[A.colind[j]];
			sendVCbuffer[position + 1] = A.val[j];
			position += 2;
			sendVCbuffer_ptr[i + 1]++;
		}
	}

	for (int i = 0; i < tosend_ptrs[nprocs]; i++){
		sendVCbuffer_ptr[i + 1] = sendVCbuffer_ptr[i + 1] + sendVCbuffer_ptr[i];
	}

	for (int i = 0; i < tosend_ptrs[nprocs]; i++){
		sendVCbuffer_ptr[i + 1] = 2 * sendVCbuffer_ptr[i + 1];
	}


	/* Communications for actual values */

	for (int i = 0; i < outer_A.size; i++){
		outer_A.rowind[i + 1] = 2 * outer_A.rowind[i + 1];
	}

	counter = 0;
	int		start_pos = 0,
			end_pos = 0;

	for (int i = 0; i < nprocs; i++){
		if (proc_rows_recv[i] > 0) {
			end_pos = start_pos + proc_rows_recv[i];
			MPI_Irecv(recvVCbuffer + outer_A.rowind[start_pos], (outer_A.rowind[end_pos] - outer_A.rowind[start_pos]), ARCHSAI_MPI_DOUBLE, i, 0, pattern->comm, &req[counter]);
			start_pos = end_pos;
			counter++;
		}
	}

	start_pos = 0;
	end_pos = 0;

	for (int i = 0; i < nprocs; i++){
		if (proc_rows_send[i] > 0) {
			end_pos = start_pos + proc_rows_send[i];
			MPI_Isend(sendVCbuffer + sendVCbuffer_ptr[start_pos], (sendVCbuffer_ptr[end_pos] - sendVCbuffer_ptr[start_pos]), ARCHSAI_MPI_DOUBLE, i, 0, pattern->comm, &req[counter]);
			start_pos = end_pos;
			counter++;
		}
	}

	MPI_Waitall(counter, req, MPI_STATUSES_IGNORE);

	for (int i = 0; i < outer_A.size; i++){
		outer_A.rowind[i + 1] = outer_A.rowind[i + 1] / 2;
	}


	/* Create matrix with external rows */

	for (int i = 0; i < outer_A.rowind[outer_A.size]; i++){
		outer_A.colind[i] = (ARCHSAI_INT) recvVCbuffer[2 * i];
		outer_A.val[i] = recvVCbuffer[2 * i + 1];
	}

	ArchSAI_TFree(proc_rows_send, ArchSAI_MEMORY_HOST);
	ArchSAI_TFree(proc_rows_recv, ArchSAI_MEMORY_HOST);
	ArchSAI_TFree(row_use, ArchSAI_MEMORY_HOST);
	ArchSAI_TFree(counter_procs, ArchSAI_MEMORY_HOST);
	ArchSAI_TFree(tosend_ptrs, ArchSAI_MEMORY_HOST);
	ArchSAI_TFree(tosend_indices, ArchSAI_MEMORY_HOST);
	ArchSAI_TFree(req, ArchSAI_MEMORY_HOST);
	ArchSAI_TFree(send_sizes, ArchSAI_MEMORY_HOST);
	ArchSAI_TFree(recvVCbuffer, ArchSAI_MEMORY_HOST);
	ArchSAI_TFree(sendVCbuffer, ArchSAI_MEMORY_HOST);
	ArchSAI_TFree(sendVCbuffer_ptr, ArchSAI_MEMORY_HOST);

    /* NOW I HAVE THE OUTER MAT, WHICH I NEED TO CREATE THE LOCAL RESTRICTED MAT */
    /* OUTER MAT HAS GLOBAL INDICES */

    ArchSAI_ToGlobalMatCSR(pattern);

    if (pattern->fval) ArchSAI_TFree(pattern->fval, ArchSAI_MEMORY_HOST);
    pattern->fval    = ArchSAI_CTAlloc(ARCHSAI_FLOAT, pattern->nnz, ArchSAI_MEMORY_HOST);

    #pragma omp parallel
    {
        ARCHSAI_INT *row  =   ArchSAI_CTAlloc(ARCHSAI_INT, pattern->gncols, ArchSAI_MEMORY_HOST),
                            diff_cols = 0,
                            counter = 0,
                            num_rows = 0;

        #pragma omp for
        for (int i = 0; i < pattern->nrows; i++){

            counter = 0;
            num_rows = 0;

            diff_cols = pattern->rowind[i + 1] - pattern->rowind[i];

            for (int j = pattern->rowind[i]; j < pattern->rowind[i + 1]; j++){
                ARCHSAI_INT restricted_row = pattern->colind[j];
                if (pattern->rowdist[restricted_row] == rank) { // Row is local
                    for (int k = pattern->rowind[pattern->gl2loc[restricted_row]]; k <  pattern->rowind[pattern->gl2loc[restricted_row] + 1]; k++){
                        ARCHSAI_INT restricted_col = pattern->colind[k];
                        if (row[restricted_col] == 0) {
                            row[restricted_col] = 1;
                            num_rows++;
                        }
                    }
                }
                else {                                          // Row is in outer_A
                    ARCHSAI_INT outer_row = 0;
                    for (outer_row = 0; outer_row < torecv_ptrs[nprocs]; outer_row++){
                        if (torecv_indices[outer_row] == restricted_row) break;
                    }
                    for (int k =  outer_A.rowind[outer_row]; k <  outer_A.rowind[outer_row + 1]; k++){
                        ARCHSAI_INT restricted_col = outer_A.colind[k];
                        if (row[restricted_col] == 0) {
                            row[restricted_col] = 1;
                            num_rows++;
                        }
                    }
                }
            }

            /* I have sizes of restricted matrix */

            //ARCHSAI_INT rest_vect[num_rows];
            ARCHSAI_INT *rest_vect = ArchSAI_CTAlloc(ARCHSAI_INT, num_rows, ArchSAI_MEMORY_HOST);//[num_rows];
            num_rows = 0;

            for (int j = pattern->rowind[i]; j < pattern->rowind[i + 1]; j++){
                ARCHSAI_INT restricted_row = pattern->colind[j];
                if (pattern->rowdist[restricted_row] == rank) { // Row is local
                    for (int k = pattern->rowind[pattern->gl2loc[restricted_row]]; k <  pattern->rowind[pattern->gl2loc[restricted_row] + 1]; k++){
                        ARCHSAI_INT restricted_col = pattern->colind[k];
                        if (row[restricted_col] == 1) {
                            row[restricted_col] = 0;
                            rest_vect[num_rows] = restricted_col;
                            num_rows++;
                        }
                    }
                }
                else {                                          // Row is in outer_A
                    ARCHSAI_INT outer_row = 0;
                    for (outer_row = 0; outer_row < torecv_ptrs[nprocs]; outer_row++){
                        if (torecv_indices[outer_row] == restricted_row) break;
                    }
                    for (int k =  outer_A.rowind[outer_row]; k <  outer_A.rowind[outer_row + 1]; k++){
                        ARCHSAI_INT restricted_col = outer_A.colind[k];
                        if (row[restricted_col] == 1) {
                            row[restricted_col] = 0;
                            rest_vect[num_rows] = restricted_col;
                            num_rows++;
                        }
                    }
                }
            }

            qsort(rest_vect, num_rows, sizeof(ARCHSAI_INT), intcompare);

            for (int j = 0; j < num_rows; j++){
                row[rest_vect[j]] = counter;        // Row converts global indices to restricted indices
                counter++;
            }

            ARCHSAI_FLOAT *a = ArchSAI_CTAlloc(ARCHSAI_FLOAT, num_rows * diff_cols, ArchSAI_MEMORY_HOST);
            ARCHSAI_FLOAT *b = ArchSAI_CTAlloc(ARCHSAI_FLOAT, ArchSAI_Max(num_rows, diff_cols), ArchSAI_MEMORY_HOST);

            for (int j = pattern->rowind[i]; j < pattern->rowind[i + 1]; j++){

                ARCHSAI_INT col_id  = pattern->colind[j];           // Global index of the column of row.
                                                                    // Corresponds to the row we will be putting in a
                                                                    // as a column

                ARCHSAI_INT rest_col = j - pattern->rowind[i];      // Column of a we are building in restricted index

                ARCHSAI_INT start = 0;
                ARCHSAI_INT outer_A_start = 0;

                ARCHSAI_INT get_row = col_id;                       // This is the global row we gather entries from

                ARCHSAI_INT row_owner = pattern->rowdist[get_row];  // This is the owner of the row we gather entries from

                if (row_owner == rank) {                            // Row is local

                    ARCHSAI_INT local_row = A.gl2loc[col_id];

                    for (int k = A.rowind[local_row]; k < A.rowind[local_row + 1]; k++){
                        ARCHSAI_INT local_col = A.loc2gl[A.colind[k]];
                        ARCHSAI_FLOAT local_val = (ARCHSAI_FLOAT) A.val[k];
                        ARCHSAI_INT rest_row = row[local_col];

                        a[rest_row * diff_cols + rest_col] = local_val;
                    }
                }
                else {                                              // Row is in outer_A

                    ARCHSAI_INT outer_A_row = 0;

                    // Find row in outer_A
                    for (outer_A_row = outer_A_start; outer_A_row < outer_A.size; outer_A_row++){
                        if (torecv_indices[outer_A_row] == col_id) break;
                    }

                    outer_A_start = outer_A_row;

                    for (int k = outer_A.rowind[outer_A_row]; k < outer_A.rowind[outer_A_row + 1]; k++){
                        ARCHSAI_INT outer_A_col = outer_A.colind[k];
                        ARCHSAI_FLOAT outer_A_val = (ARCHSAI_FLOAT) outer_A.val[k];
                        ARCHSAI_INT rest_row = row[outer_A_col];

                        a[rest_row * diff_cols + rest_col] = outer_A_val;

                    }
                }

                if (pattern->gl2loc[col_id] == i) b[row[col_id]] = 1.0;
            }

            ARCHSAI_INT M = num_rows, N = diff_cols, NRHS = 1, LDA = N, LDB = NRHS;

            ArchSAI_SGELS(LAPACK_ROW_MAJOR, 'N', M, N, NRHS, a, LDA, b, LDB);

            ArchSAI_TMemcpy(pattern->fval + pattern->rowind[i], b, ARCHSAI_FLOAT, diff_cols, ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);

            ArchSAI_TFree(a, ArchSAI_MEMORY_HOST);
            ArchSAI_TFree(b, ArchSAI_MEMORY_HOST);

            for (int j = 0; j < num_rows; j++){
                row[rest_vect[j]] = 0;
            }
            ArchSAI_TFree(rest_vect, ArchSAI_MEMORY_HOST);

        }

        ArchSAI_TFree(row, ArchSAI_MEMORY_HOST);

    }

    ArchSAI_ToLocalMatCSR(pattern);

	ArchSAI_TFree(torecv_ptrs, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(torecv_indices, ArchSAI_MEMORY_HOST);
    ArchSAI_DestroyMatCSR(&outer_A, ArchSAI_MEMORY_HOST);

    return archsai_error_flag;
}



ARCHSAI_INT ArchSAI_ComputeSAI(ARCHSAI_MATCSR A, ARCHSAI_MATCSR *pattern){

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(pattern->comm, &rank);
    MPI_Comm_size(pattern->comm, &nprocs);

    /* How many rows should every process send and receive */

	ARCHSAI_INT		*proc_rows_recv = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs, ArchSAI_MEMORY_HOST),
                    *proc_rows_send = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs, ArchSAI_MEMORY_HOST);
	ARCHSAI_CHAR    *row_use = ArchSAI_CTAlloc(ARCHSAI_CHAR, pattern->gnrows, ArchSAI_MEMORY_HOST);

	for (int i = 0; i < pattern->nrows; i++){
		for (int j = pattern->rowind[i]; j < pattern->rowind[i + 1]; j++){
			row_use[pattern->loc2gl[pattern->colind[j]]] = 1;
		}
	}

	for (int i = 0; i < pattern->gnrows; i++){
		if ((row_use[i] == 1) && (pattern->rowdist[i] != rank)){
			proc_rows_recv[pattern->rowdist[i]]++;
		}
	}

	MPI_Alltoall(proc_rows_recv, 1, ARCHSAI_MPI_INT, proc_rows_send, 1, ARCHSAI_MPI_INT, pattern->comm);

	/* Obtain indices of rows that I need */

	ARCHSAI_INT		*torecv_ptrs = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs + 1, ArchSAI_MEMORY_HOST),
                    *tosend_ptrs = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs + 1, ArchSAI_MEMORY_HOST),
                    *counter_procs = ArchSAI_CTAlloc(ARCHSAI_INT, nprocs + 1, ArchSAI_MEMORY_HOST);

	for (int i = 0; i < nprocs; i++){
		torecv_ptrs[i + 1] = torecv_ptrs[i] + proc_rows_recv[i];
	}

	ARCHSAI_INT		*torecv_indices = ArchSAI_CTAlloc(ARCHSAI_INT, torecv_ptrs[nprocs], ArchSAI_MEMORY_HOST);

	for (int i = 0; i < pattern->gnrows; i++){
		if ((row_use[i] == 1) && (pattern->rowdist[i] != rank)){
			torecv_indices[torecv_ptrs[pattern->rowdist[i]] + counter_procs[pattern->rowdist[i]]] = i;
			counter_procs[pattern->rowdist[i]]++;
		}
	}

	for (int i = 0; i < nprocs; i++){
		tosend_ptrs[i + 1] = tosend_ptrs[i] + proc_rows_send[i];
	}

    /* Communications of row indices to know what indices have to be sent */
	ARCHSAI_INT		*tosend_indices = ArchSAI_CTAlloc(ARCHSAI_INT, tosend_ptrs[nprocs], ArchSAI_MEMORY_HOST);

    MPI_Request     *req = ArchSAI_TAlloc(MPI_Request, 2 * nprocs, ArchSAI_MEMORY_HOST);
    ARCHSAI_INT     counter = 0;

	for (int i = 0; i < nprocs; i++){
		if (proc_rows_send[i] > 0) {
			MPI_Irecv(tosend_indices + tosend_ptrs[i], tosend_ptrs[i + 1] - tosend_ptrs[i], ARCHSAI_MPI_INT, i, 0, pattern->comm, &req[counter]);
			counter++;
		}
	}
	for (int i = 0; i < nprocs; i++){
		if (proc_rows_recv[i] > 0) {
			MPI_Isend(torecv_indices + torecv_ptrs[i], torecv_ptrs[i + 1] - torecv_ptrs[i], ARCHSAI_MPI_INT, i, 0, pattern->comm, &req[counter]);
			counter++;
		}
	}
	MPI_Waitall(counter, req, MPI_STATUSES_IGNORE);

    /* Send sizes of rows to create buffers */

    ARCHSAI_INT *send_sizes = ArchSAI_CTAlloc(ARCHSAI_INT, tosend_ptrs[nprocs], ArchSAI_MEMORY_HOST);

	for (int i = 0; i < nprocs; i++){
		for (int j = tosend_ptrs[i]; j < tosend_ptrs[i + 1]; j++){
			send_sizes[j] += (A.rowind[A.gl2loc[tosend_indices[j]] + 1] - A.rowind[A.gl2loc[tosend_indices[j]]]);
		}
	}

	/* We create a local temporary matrix to store the outer rows */

    ARCHSAI_MATCSR outer_A;
    ArchSAI_InitMatCSR(&outer_A);

    outer_A.rowind = ArchSAI_CTAlloc(ARCHSAI_INT, torecv_ptrs[nprocs] + 1, ArchSAI_MEMORY_HOST);
    outer_A.size   = torecv_ptrs[nprocs];

	counter = 0;
	for (int i = 0; i < nprocs; i++){
		if (proc_rows_recv[i] > 0) {
			MPI_Irecv(outer_A.rowind + torecv_ptrs[i], torecv_ptrs[i + 1] - torecv_ptrs[i], ARCHSAI_MPI_INT, i, 0, pattern->comm, &req[counter]);
			counter++;
		}
	}

	for (int i = 0; i < nprocs; i++){
		if (proc_rows_send[i] > 0) {
			MPI_Isend(send_sizes + tosend_ptrs[i], tosend_ptrs[i + 1] - tosend_ptrs[i], ARCHSAI_MPI_INT, i, 0, pattern->comm, &req[counter]);
			counter++;
		}
	}

	MPI_Waitall(counter, req, MPI_STATUSES_IGNORE);

    ARCHSAI_INT nextval = 0,
                prevval = 0;

	for (int i = 0; i < outer_A.size + 1; i++){
		nextval = prevval + outer_A.rowind[i];
		outer_A.rowind[i] = prevval;
		prevval = nextval;
	}

	outer_A.nnz = outer_A.rowind[outer_A.size];

    /* Now I know recv index sizes so we build a buffer */

    outer_A.colind = ArchSAI_CTAlloc(ARCHSAI_INT, outer_A.nnz, ArchSAI_MEMORY_HOST);
    outer_A.val = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, outer_A.nnz, ArchSAI_MEMORY_HOST);

        /* We will transfer columns and values at the same communication step with a buffer */
    ARCHSAI_DOUBLE *recvVCbuffer = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, outer_A.nnz * 2, ArchSAI_MEMORY_HOST);

    ARCHSAI_INT sizesendbuffer = 0;
    for (int i = 0; i < tosend_ptrs[nprocs]; i++) sizesendbuffer += send_sizes[i];

    ARCHSAI_DOUBLE *sendVCbuffer = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, sizesendbuffer * 2, ArchSAI_MEMORY_HOST);
	ARCHSAI_INT *sendVCbuffer_ptr = ArchSAI_CTAlloc(ARCHSAI_INT, tosend_ptrs[nprocs] + 1, ArchSAI_MEMORY_HOST);

    ARCHSAI_INT position = 0;

	for (int i = 0; i < tosend_ptrs[nprocs]; i++){
		for (int j = A.rowind[A.gl2loc[tosend_indices[i]]]; j < A.rowind[A.gl2loc[tosend_indices[i]] + 1]; j++){
			sendVCbuffer[position] = (double) A.loc2gl[A.colind[j]];
			sendVCbuffer[position + 1] = A.val[j];
			position += 2;
			sendVCbuffer_ptr[i + 1]++;
		}
	}

	for (int i = 0; i < tosend_ptrs[nprocs]; i++){
		sendVCbuffer_ptr[i + 1] = sendVCbuffer_ptr[i + 1] + sendVCbuffer_ptr[i];
	}

	for (int i = 0; i < tosend_ptrs[nprocs]; i++){
		sendVCbuffer_ptr[i + 1] = 2 * sendVCbuffer_ptr[i + 1];
	}


	/* Communications for actual values */

	for (int i = 0; i < outer_A.size; i++){
		outer_A.rowind[i + 1] = 2 * outer_A.rowind[i + 1];
	}

	counter = 0;
	int		start_pos = 0,
			end_pos = 0;

	for (int i = 0; i < nprocs; i++){
		if (proc_rows_recv[i] > 0) {
			end_pos = start_pos + proc_rows_recv[i];
			MPI_Irecv(recvVCbuffer + outer_A.rowind[start_pos], (outer_A.rowind[end_pos] - outer_A.rowind[start_pos]), ARCHSAI_MPI_DOUBLE, i, 0, pattern->comm, &req[counter]);
			start_pos = end_pos;
			counter++;
		}
	}

	start_pos = 0;
	end_pos = 0;

	for (int i = 0; i < nprocs; i++){
		if (proc_rows_send[i] > 0) {
			end_pos = start_pos + proc_rows_send[i];
			MPI_Isend(sendVCbuffer + sendVCbuffer_ptr[start_pos], (sendVCbuffer_ptr[end_pos] - sendVCbuffer_ptr[start_pos]), ARCHSAI_MPI_DOUBLE, i, 0, pattern->comm, &req[counter]);
			start_pos = end_pos;
			counter++;
		}
	}

	MPI_Waitall(counter, req, MPI_STATUSES_IGNORE);

	for (int i = 0; i < outer_A.size; i++){
		outer_A.rowind[i + 1] = outer_A.rowind[i + 1] / 2;
	}


	/* Create matrix with external rows */

	for (int i = 0; i < outer_A.rowind[outer_A.size]; i++){
		outer_A.colind[i] = (ARCHSAI_INT) recvVCbuffer[2 * i];
		outer_A.val[i] = recvVCbuffer[2 * i + 1];
	}

	ArchSAI_TFree(proc_rows_send, ArchSAI_MEMORY_HOST);
	ArchSAI_TFree(proc_rows_recv, ArchSAI_MEMORY_HOST);
	ArchSAI_TFree(row_use, ArchSAI_MEMORY_HOST);
	ArchSAI_TFree(counter_procs, ArchSAI_MEMORY_HOST);
	ArchSAI_TFree(tosend_ptrs, ArchSAI_MEMORY_HOST);
	ArchSAI_TFree(tosend_indices, ArchSAI_MEMORY_HOST);
	ArchSAI_TFree(req, ArchSAI_MEMORY_HOST);
	ArchSAI_TFree(send_sizes, ArchSAI_MEMORY_HOST);
	ArchSAI_TFree(recvVCbuffer, ArchSAI_MEMORY_HOST);
	ArchSAI_TFree(sendVCbuffer, ArchSAI_MEMORY_HOST);
	ArchSAI_TFree(sendVCbuffer_ptr, ArchSAI_MEMORY_HOST);

    /* NOW I HAVE THE OUTER MAT, WHICH I NEED TO CREATE THE LOCAL RESTRICTED MAT */
    /* OUTER MAT HAS GLOBAL INDICES */

    ArchSAI_ToGlobalMatCSR(pattern);

    if (pattern->fval) ArchSAI_TFree(pattern->fval, ArchSAI_MEMORY_HOST);
    if (pattern->val) ArchSAI_TFree(pattern->val, ArchSAI_MEMORY_HOST);
    pattern->fval   = NULL;
    pattern->val    = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, pattern->nnz, ArchSAI_MEMORY_HOST);

    #pragma omp parallel
    {
        ARCHSAI_INT *row  =   ArchSAI_CTAlloc(ARCHSAI_INT, pattern->gncols, ArchSAI_MEMORY_HOST),
                            diff_cols = 0,
                            counter = 0,
                            num_rows = 0;

        #pragma omp for
        for (int i = 0; i < pattern->nrows; i++){

            counter = 0;
            num_rows = 0;

            diff_cols = pattern->rowind[i + 1] - pattern->rowind[i];

            for (int j = pattern->rowind[i]; j < pattern->rowind[i + 1]; j++){
                ARCHSAI_INT restricted_row = pattern->colind[j];
                if (pattern->rowdist[restricted_row] == rank) { // Row is local
                    for (int k = pattern->rowind[pattern->gl2loc[restricted_row]]; k <  pattern->rowind[pattern->gl2loc[restricted_row] + 1]; k++){
                        ARCHSAI_INT restricted_col = pattern->colind[k];
                        if (row[restricted_col] == 0) {
                            row[restricted_col] = 1;
                            num_rows++;
                        }
                    }
                }
                else {                                          // Row is in outer_A
                    ARCHSAI_INT outer_row = 0;
                    for (outer_row = 0; outer_row < torecv_ptrs[nprocs]; outer_row++){
                        if (torecv_indices[outer_row] == restricted_row) break;
                    }
                    for (int k =  outer_A.rowind[outer_row]; k <  outer_A.rowind[outer_row + 1]; k++){
                        ARCHSAI_INT restricted_col = outer_A.colind[k];
                        if (row[restricted_col] == 0) {
                            row[restricted_col] = 1;
                            num_rows++;
                        }
                    }
                }
            }

            /* I have sizes of restricted matrix */

            //ARCHSAI_INT rest_vect[num_rows];
            ARCHSAI_INT *rest_vect = ArchSAI_CTAlloc(ARCHSAI_INT, num_rows, ArchSAI_MEMORY_HOST);//[num_rows];
            num_rows = 0;

            for (int j = pattern->rowind[i]; j < pattern->rowind[i + 1]; j++){
                ARCHSAI_INT restricted_row = pattern->colind[j];
                if (pattern->rowdist[restricted_row] == rank) { // Row is local
                    for (int k = pattern->rowind[pattern->gl2loc[restricted_row]]; k <  pattern->rowind[pattern->gl2loc[restricted_row] + 1]; k++){
                        ARCHSAI_INT restricted_col = pattern->colind[k];
                        if (row[restricted_col] == 1) {
                            row[restricted_col] = 0;
                            rest_vect[num_rows] = restricted_col;
                            num_rows++;
                        }
                    }
                }
                else {                                          // Row is in outer_A
                    ARCHSAI_INT outer_row = 0;
                    for (outer_row = 0; outer_row < torecv_ptrs[nprocs]; outer_row++){
                        if (torecv_indices[outer_row] == restricted_row) break;
                    }
                    for (int k =  outer_A.rowind[outer_row]; k <  outer_A.rowind[outer_row + 1]; k++){
                        ARCHSAI_INT restricted_col = outer_A.colind[k];
                        if (row[restricted_col] == 1) {
                            row[restricted_col] = 0;
                            rest_vect[num_rows] = restricted_col;
                            num_rows++;
                        }
                    }
                }
            }

            qsort(rest_vect, num_rows, sizeof(ARCHSAI_INT), intcompare);

            for (int j = 0; j < num_rows; j++){
                row[rest_vect[j]] = counter;        // Row converts global indices to restricted indices
                counter++;
            }

            ARCHSAI_DOUBLE *a = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, num_rows * diff_cols, ArchSAI_MEMORY_HOST);
            ARCHSAI_DOUBLE *b = ArchSAI_CTAlloc(ARCHSAI_DOUBLE, ArchSAI_Max(num_rows, diff_cols), ArchSAI_MEMORY_HOST);

            for (int j = pattern->rowind[i]; j < pattern->rowind[i + 1]; j++){

                ARCHSAI_INT col_id  = pattern->colind[j];           // Global index of the column of row.
                                                                    // Corresponds to the row we will be putting in a
                                                                    // as a column

                ARCHSAI_INT rest_col = j - pattern->rowind[i];      // Column of a we are building in restricted index

                ARCHSAI_INT start = 0;
                ARCHSAI_INT outer_A_start = 0;

                ARCHSAI_INT get_row = col_id;                       // This is the global row we gather entries from

                ARCHSAI_INT row_owner = pattern->rowdist[get_row];  // This is the owner of the row we gather entries from

                if (row_owner == rank) {                            // Row is local

                    ARCHSAI_INT local_row = A.gl2loc[col_id];

                    for (int k = A.rowind[local_row]; k < A.rowind[local_row + 1]; k++){
                        ARCHSAI_INT local_col = A.loc2gl[A.colind[k]];
                        ARCHSAI_DOUBLE local_val = A.val[k];
                        ARCHSAI_INT rest_row = row[local_col];

                        a[rest_row * diff_cols + rest_col] = local_val;
                    }
                }
                else {                                              // Row is in outer_A

                    ARCHSAI_INT outer_A_row = 0;

                    // Find row in outer_A
                    for (outer_A_row = outer_A_start; outer_A_row < outer_A.size; outer_A_row++){
                        if (torecv_indices[outer_A_row] == col_id) break;
                    }

                    outer_A_start = outer_A_row;

                    for (int k = outer_A.rowind[outer_A_row]; k < outer_A.rowind[outer_A_row + 1]; k++){
                        ARCHSAI_INT outer_A_col = outer_A.colind[k];
                        ARCHSAI_DOUBLE outer_A_val = outer_A.val[k];
                        ARCHSAI_INT rest_row = row[outer_A_col];

                        a[rest_row * diff_cols + rest_col] = outer_A_val;

                    }

                }

                if (pattern->gl2loc[col_id] == i) b[row[col_id]] = 1.0;
            }

            ARCHSAI_INT M = num_rows, N = diff_cols, NRHS = 1, LDA = N, LDB = NRHS;

            ArchSAI_DGELS(LAPACK_ROW_MAJOR, 'N', M, N, NRHS, a, LDA, b, LDB);

            ArchSAI_TMemcpy(pattern->val + pattern->rowind[i], b, ARCHSAI_DOUBLE, diff_cols, ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);

            ArchSAI_TFree(a, ArchSAI_MEMORY_HOST);
            ArchSAI_TFree(b, ArchSAI_MEMORY_HOST);

            for (int j = 0; j < num_rows; j++){
                row[rest_vect[j]] = 0;
            }

            ArchSAI_TFree(rest_vect, ArchSAI_MEMORY_HOST);

        }

        ArchSAI_TFree(row, ArchSAI_MEMORY_HOST);

    }

    ArchSAI_ToLocalMatCSR(pattern);

	ArchSAI_TFree(torecv_ptrs, ArchSAI_MEMORY_HOST);
    ArchSAI_TFree(torecv_indices, ArchSAI_MEMORY_HOST);
    ArchSAI_DestroyMatCSR(&outer_A, ArchSAI_MEMORY_HOST);

    return archsai_error_flag;
}




