#include <ArchSAI_PC.h>
#include <omp.h>

ARCHSAI_PRECOND ArchSAI_DiagonalPC(ARCHSAI_MATCSR A, ARCHSAI_PARAMS params){

    // CHECK CONSISTENCY
    if (A.fulldiag == 0){
        ArchSAI_ParPrintf(A.comm, "Error: Can not compute Diagonal PC of a non full diagonal matrix.\n");
        ArchSAI_Error(1);
    }
    if (A.gnrows != A.gncols){
        ArchSAI_ParPrintf(A.comm, "Error: Can not compute Diagonal PC of a non square matrix.\n");
        ArchSAI_Error(1);
    }

    ARCHSAI_PRECOND PC;
    ARCHSAI_MATCSR diagonal;

    ArchSAI_InitMatCSR(&diagonal);

    diagonal.size       = A.size;
    diagonal.nnz        = A.nrows;
    diagonal.nrows      = A.nrows;
    diagonal.ncols      = A.nrows;
    diagonal.sym        = 1;
    diagonal.fulldiag   = 1;
    diagonal.gsize      = A.gsize;
    diagonal.gnnz       = A.gnrows;
    diagonal.gnrows     = A.gnrows;
    diagonal.gncols     = A.gnrows;
    diagonal.locgl      = A.locgl;
    diagonal.comm       = A.comm;


    /* Allocate */
    diagonal.rowind     = ArchSAI_CTAlloc(ARCHSAI_INT,      diagonal.nrows + 1,   ArchSAI_MEMORY_HOST);
    diagonal.colind     = ArchSAI_CTAlloc(ARCHSAI_INT,      diagonal.nnz,         ArchSAI_MEMORY_HOST);
    diagonal.val        = ArchSAI_CTAlloc(ARCHSAI_DOUBLE,   diagonal.nnz,         ArchSAI_MEMORY_HOST);
    diagonal.rowdist    = ArchSAI_CTAlloc(ARCHSAI_INT,      diagonal.gnrows,     ArchSAI_MEMORY_HOST);
    diagonal.gl2loc     = ArchSAI_CTAlloc(ARCHSAI_INT,      diagonal.gnrows,     ArchSAI_MEMORY_HOST);
    diagonal.loc2gl     = ArchSAI_CTAlloc(ARCHSAI_INT,      diagonal.gnrows,     ArchSAI_MEMORY_HOST);


    /* Copy */
    ArchSAI_TMemcpy(diagonal.rowdist, A.rowdist,    ARCHSAI_INT,   diagonal.gnrows,       ArchSAI_MEMORY_HOST, ArchSAI_MEMORY_HOST);

    ARCHSAI_INT rank, nprocs;
    MPI_Comm_rank(diagonal.comm, &rank);
    MPI_Comm_size(diagonal.comm, &nprocs);

    ARCHSAI_INT counter = 0;

    for (int i = 0; i < diagonal.gnrows; i++){
        if (diagonal.rowdist[i] == rank) {
            diagonal.gl2loc[i] = counter;
            diagonal.loc2gl[counter] = i;
            counter++;
        }

    }

    /* Fill matrix */
    for (int i = 0; i < A.nrows; i++){
        for (int j = A.rowind[i]; j < A.rowind[i + 1]; j++){
            if (A.loc2gl[A.colind[j]] == A.loc2gl[i]){
                diagonal.rowind[i + 1] = i + 1;
                diagonal.colind[i] = i;
                if (A.val[j] == 0.0) diagonal.val[i] = 1.0;
                else diagonal.val[i] = 1.0 / A.val[j];
                break;
            }
        }
    }

    /* Obtain comm scheme */
    diagonal.commscheme = ArchSAI_CreateCommSchemeMatCSR(diagonal);

    PC.type = params.precond;
    PC.sys_size = A.nrows;
    PC.mat = diagonal;
    return PC;
}
