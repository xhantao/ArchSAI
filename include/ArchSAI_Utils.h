 
 /*! \file ArchSAI_Utils.h
    \brief This file contains basic utilities and functions used by ArchSAI.

    ArchSAI basic utilities.
*/

 #ifndef ArchSAI_Utils_h_
    #define ArchSAI_Utils_h_

    #include <ArchSAI_DTypes.h>
    #include <math.h>


    /*! \def ArchSAI_Max(a,b)
    \brief A macro that returns the maximum of \a a and \a b.

    Details.
    */
    #define ArchSAI_Max(a,b) (((a)>(b))?(a):(b))


    /*! \def ArchSAI_Min(a,b)
    \brief A macro that returns the minimum of \a a and \a b.

    Details.
    */
    #define ArchSAI_Min(a,b) (((a)<(b))?(a):(b))

    #ifndef ArchSAI_Abs
        #define ArchSAI_Abs(a)  (((a)>0) ? (a) : -(a))
    #endif


    #ifndef ArchSAI_Sqrt
        #if defined(ARCHSAI_SINGLE)
            #define ArchSAI_Sqrt sqrtf
        #elif defined(ARCHSAI_FULLPREC)
            #define ArchSAI_Sqrt sqrt
        #else
            #define ArchSAI_Sqrt sqrt
        #endif
    #endif

//     static inline ARCHSAI_INT __attribute__((always_inline)) intcompare( const void* a, const void* b){
//         return *((ARCHSAI_INT*)a)-*((ARCHSAI_INT*)b);
//     }
//
//     static inline ARCHSAI_INT __attribute__((always_inline))  intpointercompare(const void *a, const void *b){
//         return **((ARCHSAI_INT**)a)-**((ARCHSAI_INT**)b);
//     }

    static ARCHSAI_INT intcompare( const void* a, const void* b){
        return *((ARCHSAI_INT*)a)-*((ARCHSAI_INT*)b);
    }

    static ARCHSAI_INT intpointercompare(const void *a, const void *b){
        return **((ARCHSAI_INT**)a)-**((ARCHSAI_INT**)b);
    }


#endif // __ArchSAI_Utils_h__

