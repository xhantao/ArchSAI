/*! \file ArchSAI_LinAlg.h
    \brief This file contains the types and data structures used by ArchSAI.

    ArchSAI types and structures.
*/

 #ifndef ArchSAI_LinAlg_h_
    #define ArchSAI_LinAlg_h_

    #include <ArchSAI_DTypes.h>
    #include <ArchSAI_Error.h>
    #include <string.h>
    #include <omp.h>

    #if defined ARCHSAI_WITH_MKL

        #include "mkl.h"
        #include "mkl_lapacke.h"
        #include "mkl_cblas.h"

        #if defined ARCHSAI_FULLPREC
            #define ArchSAI_SGELS(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb) \
                        LAPACKE_dgels(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb)
            #define ArchSAI_DGELS(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb) \
                        LAPACKE_dgels(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb)
            #define ArchSAI_GELS(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb) \
                        LAPACKE_dgels(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb)

            #define ArchSAI_SGESV(layout, n, nrhs, a, lda, ipiv, b, ldb) \
                        LAPACKE_dgesv(layout, n, nrhs, a, lda, ipiv, b, ldb)
            #define ArchSAI_DGESV(layout, n, nrhs, a, lda, ipiv, b, ldb) \
                        LAPACKE_dgesv(layout, n, nrhs, a, lda, ipiv, b, ldb)
            #define ArchSAI_GESV(layout, n, nrhs, a, lda, ipiv, b, ldb) \
                        LAPACKE_dgesv(layout, n, nrhs, a, lda, ipiv, b, ldb)

            #define ArchSAI_DotP(size, v1, inc1, v2, inc2)               cblas_ddot(size, v1, inc1, v2, inc2)
            #define ArchSAI_Axpy(size, alpha, v1, inc1, v2, inc2)        cblas_daxpy(size, alpha, v1, inc1, v2, inc2)
            #define ArchSAI_Copy(size, v1, inc1, v2, inc2)               cblas_dcopy(size, v1, inc1, v2, inc2)
            #define ArchSAI_Scal(size, alpha, v1, inc1)                  cblas_dscal(size, alpha, v1, inc1)
            #define ArchSAI_Csrmv(trans, matdescra, mat, alpha, x, beta, y); \
                        mkl_dcsrmv(trans, &m, &n, &alpha, matdescra, mat.val, mat.colind, mat.rowind, mat.rowind + 1, x, &beta, y)

        #elif defined ARCHSAI_SINGLE
            #define ArchSAI_SGELS(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb) \
                        LAPACKE_sgels(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb)
            #define ArchSAI_DGELS(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb) \
                        LAPACKE_sgels(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb)
            #define ArchSAI_GELS(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb) \
                        LAPACKE_sgels(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb)

            #define ArchSAI_SGESV(layout, n, nrhs, a, lda, ipiv, b, ldb) \
                        LAPACKE_sgesv(layout, n, nrhs, a, lda, ipiv, b, ldb)
            #define ArchSAI_DGESV(layout, n, nrhs, a, lda, ipiv, b, ldb) \
                        LAPACKE_sgesv(layout, n, nrhs, a, lda, ipiv, b, ldb)
            #define ArchSAI_GESV(layout, n, nrhs, a, lda, ipiv, b, ldb) \
                        LAPACKE_sgesv(layout, n, nrhs, a, lda, ipiv, b, ldb)

            #define ArchSAI_DotP(size, v1, inc1, v2, inc2)               cblas_sdot(size, v1, inc1, v2, inc2)
            #define ArchSAI_Axpy(size, alpha, v1, inc1, v2, inc2)        cblas_saxpy(size, alpha, v1, inc1, v2, inc2)
            #define ArchSAI_Copy(size, v1, inc1, v2, inc2)               cblas_scopy(size, v1, inc1, v2, inc2)
            #define ArchSAI_Scal(size, alpha, v1, inc1)                  cblas_sscal(size, alpha, v1, inc1)
            #define ArchSAI_Csrmv(trans, matdescra, mat, alpha, x, beta, y); \
                        mkl_scsrmv(trans, &m, &n, &alpha, matdescra, mat.val, mat.colind, mat.rowind, mat.rowind + 1, x, &beta, y)
        #else
            #define ArchSAI_SGELS(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb) \
                        LAPACKE_sgels(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb)
            #define ArchSAI_DGELS(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb) \
                        LAPACKE_dgels(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb)
            #define ArchSAI_GELS(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb) \
                        LAPACKE_dgels(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb)

            #define ArchSAI_SGESV(layout, n, nrhs, a, lda, ipiv, b, ldb) \
                        LAPACKE_sgesv(layout, n, nrhs, a, lda, ipiv, b, ldb)
            #define ArchSAI_DGESV(layout, n, nrhs, a, lda, ipiv, b, ldb) \
                        LAPACKE_dgesv(layout, n, nrhs, a, lda, ipiv, b, ldb)
            #define ArchSAI_GESV(layout, n, nrhs, a, lda, ipiv, b, ldb) \
                        LAPACKE_dgesv(layout, n, nrhs, a, lda, ipiv, b, ldb)

            #define ArchSAI_DotP(size, v1, inc1, v2, inc2)               cblas_ddot(size, v1, inc1, v2, inc2)
            #define ArchSAI_Axpy(size, alpha, v1, inc1, v2, inc2)        cblas_daxpy(size, alpha, v1, inc1, v2, inc2)
            #define ArchSAI_Copy(size, v1, inc1, v2, inc2)               cblas_dcopy(size, v1, inc1, v2, inc2)
            #define ArchSAI_Scal(size, alpha, v1, inc1)                  cblas_dscal(size, alpha, v1, inc1)
            #define ArchSAI_Csrmv(trans, matdescra, mat, alpha, x, beta, y); \
                        mkl_dcsrmv(trans, &m, &n, &alpha, matdescra, mat.val, mat.colind, mat.rowind, mat.rowind + 1, x, &beta, y)
        #endif

    #elif defined ARCHSAI_WITH_LAPACK

        #include <lapacke.h>

        #if defined ARCHSAI_FULLPREC
            #define ArchSAI_SGELS(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb) \
                        LAPACKE_dgels(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb)
            #define ArchSAI_DGELS(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb) \
                        LAPACKE_dgels(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb)
            #define ArchSAI_GELS(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb) \
                        LAPACKE_dgels(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb)

            #define ArchSAI_SGESV(layout, n, nrhs, a, lda, ipiv, b, ldb) \
                        LAPACKE_dgesv(layout, n, nrhs, a, lda, ipiv, b, ldb)
            #define ArchSAI_DGESV(layout, n, nrhs, a, lda, ipiv, b, ldb) \
                        LAPACKE_dgesv(layout, n, nrhs, a, lda, ipiv, b, ldb)
            #define ArchSAI_GESV(layout, n, nrhs, a, lda, ipiv, b, ldb) \
                        LAPACKE_dgesv(layout, n, nrhs, a, lda, ipiv, b, ldb)

        #elif defined ARCHSAI_SINGLE
            #define ArchSAI_SGELS(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb) \
                        LAPACKE_sgels(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb)
            #define ArchSAI_DGELS(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb) \
                        LAPACKE_sgels(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb)
            #define ArchSAI_GELS(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb) \
                        LAPACKE_sgels(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb)

            #define ArchSAI_SGESV(layout, n, nrhs, a, lda, ipiv, b, ldb) \
                        LAPACKE_sgesv(layout, n, nrhs, a, lda, ipiv, b, ldb)
            #define ArchSAI_DGESV(layout, n, nrhs, a, lda, ipiv, b, ldb) \
                        LAPACKE_sgesv(layout, n, nrhs, a, lda, ipiv, b, ldb)
            #define ArchSAI_GESV(layout, n, nrhs, a, lda, ipiv, b, ldb) \
                        LAPACKE_sgesv(layout, n, nrhs, a, lda, ipiv, b, ldb)

        #else
            #define ArchSAI_SGELS(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb) \
                        LAPACKE_sgels(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb)
            #define ArchSAI_DGELS(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb) \
                        LAPACKE_dgels(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb)
            #define ArchSAI_GELS(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb) \
                        LAPACKE_dgels(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb)

            #define ArchSAI_SGESV(layout, n, nrhs, a, lda, ipiv, b, ldb) \
                        LAPACKE_sgesv(layout, n, nrhs, a, lda, ipiv, b, ldb)
            #define ArchSAI_DGESV(layout, n, nrhs, a, lda, ipiv, b, ldb) \
                        LAPACKE_dgesv(layout, n, nrhs, a, lda, ipiv, b, ldb)
            #define ArchSAI_GESV(layout, n, nrhs, a, lda, ipiv, b, ldb) \
                        LAPACKE_dgesv(layout, n, nrhs, a, lda, ipiv, b, ldb)
        #endif

    #else

        #define LAPACK_COL_MAJOR "LAPACK_COL_MAJOR"
        #define LAPACK_ROW_MAJOR "LAPACK_ROW_MAJOR"

        #if defined ARCHSAI_FULLPREC
            #define ArchSAI_SGELS(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb) \
                        archsai_dgels(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb)
            #define ArchSAI_DGELS(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb) \
                        archsai_dgels(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb)
            #define ArchSAI_GELS(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb) \
                        archsai_dgels(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb)

            #define ArchSAI_SGESV(layout, n, nrhs, a, lda, ipiv, b, ldb) \
                        archsai_dgesv(layout, n, nrhs, a, lda, ipiv, b, ldb)
            #define ArchSAI_DGESV(layout, n, nrhs, a, lda, ipiv, b, ldb) \
                        archsai_dgesv(layout, n, nrhs, a, lda, ipiv, b, ldb)
            #define ArchSAI_GESV(layout, n, nrhs, a, lda, ipiv, b, ldb) \
                        archsai_dgesv(layout, n, nrhs, a, lda, ipiv, b, ldb)

        #elif defined ARCHSAI_SINGLE
            #define ArchSAI_SGELS(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb) \
                        archsai_sgels(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb)
            #define ArchSAI_DGELS(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb) \
                        archsai_sgels(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb)
            #define ArchSAI_GELS(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb) \
                        archsai_sgels(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb)

            #define ArchSAI_SGESV(layout, n, nrhs, a, lda, ipiv, b, ldb) \
                        archsai_sgesv(layout, n, nrhs, a, lda, ipiv, b, ldb)
            #define ArchSAI_DGESV(layout, n, nrhs, a, lda, ipiv, b, ldb) \
                        archsai_sgesv(layout, n, nrhs, a, lda, ipiv, b, ldb)
            #define ArchSAI_GESV(layout, n, nrhs, a, lda, ipiv, b, ldb) \
                        archsai_sgesv(layout, n, nrhs, a, lda, ipiv, b, ldb)
        #else
            #define ArchSAI_SGELS(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb) \
                        archsai_sgels(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb)
            #define ArchSAI_DGELS(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb) \
                        archsai_dgels(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb)
            #define ArchSAI_GELS(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb) \
                        archsai_dgels(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb)

            #define ArchSAI_SGESV(layout, n, nrhs, a, lda, ipiv, b, ldb) \
                        archsai_sgesv(layout, n, nrhs, a, lda, ipiv, b, ldb)
            #define ArchSAI_DGESV(layout, n, nrhs, a, lda, ipiv, b, ldb) \
                        archsai_dgesv(layout, n, nrhs, a, lda, ipiv, b, ldb)
            #define ArchSAI_GESV(layout, n, nrhs, a, lda, ipiv, b, ldb) \
                        archsai_dgesv(layout, n, nrhs, a, lda, ipiv, b, ldb)
        #endif


    #endif


    #if defined ARCHSAI_WITH_BLAS && !defined ARCHSAI_WITH_MKL

        #include <cblas.h>

        #if defined ARCHSAI_NEC
            #include <sblas.h>
        #endif

        #if defined ARCHSAI_FULLPREC
            #define ArchSAI_DotP(size, v1, inc1, v2, inc2)               cblas_ddot(size, v1, inc1, v2, inc2)
            #define ArchSAI_Axpy(size, alpha, v1, inc1, v2, inc2)        cblas_daxpy(size, alpha, v1, inc1, v2, inc2)
            #define ArchSAI_Copy(size, v1, inc1, v2, inc2)               cblas_dcopy(size, v1, inc1, v2, inc2)
            #define ArchSAI_Scal(size, alpha, v1, inc1)                  cblas_dscal(size, alpha, v1, inc1)

            #if defined ARCHSAI_NEC
                #define ArchSAI_Csrmv(trans, matdescra, mat, alpha, x, beta, y); \
                            sblas_execute_mv_rd(SBLAS_NON_TRANSPOSE, mat.sbh, alpha, x, beta, y);
            #else
                #define ArchSAI_Csrmv(trans, matdescra, mat, alpha, x, beta, y); \
                            archsai_dCsrmv(trans, mat.nrows, mat.ncols, alpha, mat.val, mat.colind, mat.rowind, x, beta, y)
            #endif


        #elif defined ARCHSAI_SINGLE
            #define ArchSAI_DotP(size, v1, inc1, v2, inc2)               cblas_sdot(size, v1, inc1, v2, inc2)
            #define ArchSAI_Axpy(size, alpha, v1, inc1, v2, inc2)        cblas_saxpy(size, alpha, v1, inc1, v2, inc2)
            #define ArchSAI_Copy(size, v1, inc1, v2, inc2)               cblas_scopy(size, v1, inc1, v2, inc2)
            #define ArchSAI_Scal(size, alpha, v1, inc1)                  cblas_sscal(size, alpha, v1, inc1)

            #if defined ARCHSAI_NEC
                #define ArchSAI_Csrmv(trans, matdescra, mat, alpha, x, beta, y); \
                            sblas_execute_mv_rs(SBLAS_NON_TRANSPOSE, mat.sbh, alpha, x, beta, y);
            #else
                #define ArchSAI_Csrmv(trans, matdescra, mat, alpha, x, beta, y); \
                            archsai_sCsrmv(trans, mat.nrows, mat.ncols, alpha, mat.val, mat.colind, mat.rowind, x, beta, y)
            #endif


        #else
            #define ArchSAI_DotP(size, v1, inc1, v2, inc2)               cblas_ddot(size, v1, inc1, v2, inc2)
            #define ArchSAI_Axpy(size, alpha, v1, inc1, v2, inc2)        cblas_daxpy(size, alpha, v1, inc1, v2, inc2)
            #define ArchSAI_Copy(size, v1, inc1, v2, inc2)               cblas_dcopy(size, v1, inc1, v2, inc2)
            #define ArchSAI_Scal(size, alpha, v1, inc1)                  cblas_dscal(size, alpha, v1, inc1)

            #if defined ARCHSAI_NEC
                #define ArchSAI_Csrmv(trans, matdescra, mat, alpha, x, beta, y); \
                            sblas_execute_mv_rd(SBLAS_NON_TRANSPOSE, mat.sbh, alpha, x, beta, y);
            #else
                #define ArchSAI_Csrmv(trans, matdescra, mat, alpha, x, beta, y); \
                            archsai_dCsrmv(trans, mat.nrows, mat.ncols, alpha, mat.val, mat.colind, mat.rowind, x, beta, y)
            #endif


        #endif


    #else

        #if defined ARCHSAI_FULLPREC
            #define ArchSAI_DotP(size, v1, inc1, v2, inc2)               archsai_dDotP(size, v1, inc1, v2, inc2)
            #define ArchSAI_Axpy(size, alpha, v1, inc1, v2, inc2)        archsai_dAxpy(size, alpha, v1, inc1, v2, inc2)
            #define ArchSAI_Copy(size, v1, inc1, v2, inc2)               archsai_dCopy(size, v1, inc1, v2, inc2)
            #define ArchSAI_Scal(size, alpha, v1, inc1)                  archsai_dScal(size, alpha, v1, inc1)
            #define ArchSAI_Csrmv(trans, matdescra, mat, alpha, x, beta, y); \
                        archsai_dCsrmv(trans, mat.nrows, mat.ncols, alpha, mat.val, mat.colind, mat.rowind, x, beta, y)
        #elif defined ARCHSAI_SINGLE
            #define ArchSAI_DotP(size, v1, inc1, v2, inc2)               archsai_sDotP(size, v1, inc1, v2, inc2)
            #define ArchSAI_Axpy(size, alpha, v1, inc1, v2, inc2)        archsai_sAxpy(size, alpha, v1, inc1, v2, inc2)
            #define ArchSAI_Copy(size, v1, inc1, v2, inc2)               archsai_sCopy(size, v1, inc1, v2, inc2)
            #define ArchSAI_Scal(size, alpha, v1, inc1)                  archsai_sScal(size, alpha, v1, inc1)
            #define ArchSAI_Csrmv(trans, matdescra, mat, alpha, x, beta, y); \
                        archsai_sCsrmv(trans, m, n, alpha, mat.val, mat.colind, mat.rowind, x, beta, y)
        #else
            #define ArchSAI_DotP(size, v1, inc1, v2, inc2)               archsai_dDotP(size, v1, inc1, v2, inc2)
            #define ArchSAI_Axpy(size, alpha, v1, inc1, v2, inc2)        archsai_dAxpy(size, alpha, v1, inc1, v2, inc2)
            #define ArchSAI_Copy(size, v1, inc1, v2, inc2)               archsai_dCopy(size, v1, inc1, v2, inc2)
            #define ArchSAI_Scal(size, alpha, v1, inc1)                  archsai_dScal(size, alpha, v1, inc1)
            #define ArchSAI_Csrmv(trans, matdescra, mat, alpha, x, beta, y); \
                        archsai_dCsrmv(trans, mat.nrows, mat.ncols, alpha, mat.val, mat.colind, mat.rowind, x, beta, y)
        #endif
    #endif

    // SINGLE PRECISION ROUTINES

    /*! \fn ARCHSAI_FLOAT archsai_sDotP(ARCHSAI_INT size, ARCHSAI_FLOAT *v1, ARCHSAI_INT inc1, ARCHSAI_FLOAT *v2, ARCHSAI_INT inc2)
        \brief Single-precision dot product.

        \param size Size of arrays
        \param v1 Array 1
        \param inc1 Increment of array 1
        \param v2 Array2
        \param inc2 Increment of array 2

    */
    ARCHSAI_FLOAT archsai_sDotP(ARCHSAI_INT size, ARCHSAI_FLOAT *v1, ARCHSAI_INT inc1, ARCHSAI_FLOAT *v2, ARCHSAI_INT inc2);

    /*! \fn archsai_sAxpy(ARCHSAI_INT size, ARCHSAI_FLOAT alpha, ARCHSAI_FLOAT *v1, ARCHSAI_INT inc1, ARCHSAI_FLOAT *v2, ARCHSAI_INT inc2)
        \brief Single-precision axpy. v2 = v2 + alpha*v1

        \param size Size of arrays
        \param alpha Scale of array to add
        \param v1 Array to add
        \param inc1 Increment of array 1
        \param v2 Input/Output array
        \param inc2 Increment of array 2

    */
    void archsai_sAxpy(ARCHSAI_INT size, ARCHSAI_FLOAT alpha, ARCHSAI_FLOAT *v1, ARCHSAI_INT inc1, ARCHSAI_FLOAT *v2, ARCHSAI_INT inc2);

    /*! \fn archsai_sCopy(ARCHSAI_INT size, ARCHSAI_FLOAT *v1, ARCHSAI_INT inc1, ARCHSAI_FLOAT *v2, ARCHSAI_INT inc2)
        \brief Single-precision array copy: v2 = v1

        \param size Size of array copy
        \param v1 Output
        \param inc1 Increment of array 1
        \param v2 Input
        \param inc 2Increment of array 2

    */
    void archsai_sCopy(ARCHSAI_INT size, ARCHSAI_FLOAT *v1, ARCHSAI_INT inc1, ARCHSAI_FLOAT *v2, ARCHSAI_INT inc2);

    /*! \fn archsai_sScal(ARCHSAI_INT size, ARCHSAI_FLOAT alpha, ARCHSAI_FLOAT *v1, ARCHSAI_INT inc1)
        \brief Scale array: v2 = alpha*v1

        \param size Size of arrays
        \param alpha Scaling value
        \param v1 Array to scale
        \param inc1 Increment of array 1

    */
    void archsai_sScal(ARCHSAI_INT size, ARCHSAI_FLOAT alpha, ARCHSAI_FLOAT *v1, ARCHSAI_INT inc1);

    /*! \fn void archsai_sCsrmv(ARCHSAI_CHAR *trans, ARCHSAI_INT m, ARCHSAI_INT n, ARCHSAI_FLOAT alpha, ARCHSAI_FLOAT *avval, ARCHSAI_INT *avpos, ARCHSAI_INT *avptr, ARCHSAI_FLOAT *Bptr, ARCHSAI_FLOAT beta, ARCHSAI_FLOAT *Cptr)
        \brief Single-precision CSR SpMV: y = alpha*A*x + beta*y

        \param trans Char indicating normal or transpose operation. 'N' or 'T'
        \param m Number of matrix rows
        \param n Number of matrix columns
        \param alpha Scaling value
        \param avval Pointer to matrix values
        \param avpos Pointer to matrix entry columns
        \param avptr Pointer to row indices
        \param Bptr Pointer to x
        \param beta y scaling factor
        \param Cptr Pointer to y

    */
    void archsai_sCsrmv(ARCHSAI_CHAR *trans, ARCHSAI_INT m, ARCHSAI_INT n, ARCHSAI_FLOAT alpha, ARCHSAI_FLOAT *avval, ARCHSAI_INT *avpos, ARCHSAI_INT *avptr, ARCHSAI_FLOAT *Bptr, ARCHSAI_FLOAT beta, ARCHSAI_FLOAT *Cptr);

    // DOUBLE PRECISION ROUTINES

    /*! \fn ARCHSAI_FLOAT archsai_dDotP(ARCHSAI_INT size, ARCHSAI_FLOAT *v1, ARCHSAI_INT inc1, ARCHSAI_FLOAT *v2, ARCHSAI_INT inc2)
        \brief Double-precision dot product.

        \param size Size of arrays
        \param v1 Array 1
        \param inc1 Increment of array 1
        \param v2 Array2
        \param inc2 Increment of array 2

    */
    ARCHSAI_DOUBLE archsai_dDotP(ARCHSAI_INT size, ARCHSAI_DOUBLE *v1, ARCHSAI_INT inc1, ARCHSAI_DOUBLE *v2, ARCHSAI_INT inc2);

    /*! \fn archsai_dAxpy(ARCHSAI_INT size, ARCHSAI_FLOAT alpha, ARCHSAI_FLOAT *v1, ARCHSAI_INT inc1, ARCHSAI_FLOAT *v2, ARCHSAI_INT inc2)
        \brief Double-precision axpy. v2 = v2 + alpha*v1

        \param size Size of arrays
        \param alpha Scale of array to add
        \param v1 Array to add
        \param inc1 Increment of array 1
        \param v2 Input/Output array
        \param inc2 Increment of array 2

    */
    void archsai_dAxpy(ARCHSAI_INT size, ARCHSAI_DOUBLE alpha, ARCHSAI_DOUBLE *v1, ARCHSAI_INT inc1, ARCHSAI_DOUBLE *v2, ARCHSAI_INT inc2);

    /*! \fn archsai_dCopy(ARCHSAI_INT size, ARCHSAI_FLOAT *v1, ARCHSAI_INT inc1, ARCHSAI_FLOAT *v2, ARCHSAI_INT inc2)
        \brief Double-precision array copy: v2 = v1

        \param size Size of array copy
        \param v1 Output
        \param inc1 Increment of array 1
        \param v2 Input
        \param inc 2Increment of array 2

    */
    void archsai_dCopy(ARCHSAI_INT size, ARCHSAI_DOUBLE *v1, ARCHSAI_INT inc1, ARCHSAI_DOUBLE *v2, ARCHSAI_INT inc2);

    /*! \fn archsai_dScal(ARCHSAI_INT size, ARCHSAI_FLOAT alpha, ARCHSAI_FLOAT *v1, ARCHSAI_INT inc1)
        \brief Double-precision scale array: v2 = alpha*v1

        \param size Size of arrays
        \param alpha Scaling value
        \param v1 Array to scale
        \param inc1 Increment of array 1

    */
    void archsai_dScal(ARCHSAI_INT size, ARCHSAI_DOUBLE alpha, ARCHSAI_DOUBLE *v1, ARCHSAI_INT inc1);

    /*! \fn void archsai_dCsrmv(ARCHSAI_CHAR *trans, ARCHSAI_INT m, ARCHSAI_INT n, ARCHSAI_FLOAT alpha, ARCHSAI_FLOAT *avval, ARCHSAI_INT *avpos, ARCHSAI_INT *avptr, ARCHSAI_FLOAT *Bptr, ARCHSAI_FLOAT beta, ARCHSAI_FLOAT *Cptr)
        \brief Double-precision CSR SpMV: y = alpha*A*x + beta*y

        \param trans Char indicating normal or transpose operation. 'N' or 'T'
        \param m Number of matrix rows
        \param n Number of matrix columns
        \param alpha Scaling value
        \param avval Pointer to matrix values
        \param avpos Pointer to matrix entry columns
        \param avptr Pointer to row indices
        \param Bptr Pointer to x
        \param beta y scaling factor
        \param Cptr Pointer to y

    */
    void archsai_dCsrmv(ARCHSAI_CHAR *trans, ARCHSAI_INT m, ARCHSAI_INT n, ARCHSAI_DOUBLE alpha, ARCHSAI_DOUBLE *avval, ARCHSAI_INT *avpos, ARCHSAI_INT *avptr, ARCHSAI_DOUBLE *Bptr, ARCHSAI_DOUBLE beta, ARCHSAI_DOUBLE *Cptr);




    /*! \fn
        \brief dgels implementation
//archsai_dgels(order, trans, rows, cols, nrhs, mat, lda, rhs, ldb)
//archsai_sgesv(layout, n, nrhs, a, lda, ipiv, b, ldb)
        \param

    */
    void archsai_dgels(ARCHSAI_CHAR *order, ARCHSAI_INT trans, ARCHSAI_INT rows, ARCHSAI_INT cols, ARCHSAI_INT nrhs, ARCHSAI_DOUBLE *mat, ARCHSAI_INT lda, ARCHSAI_DOUBLE *rhs, ARCHSAI_INT ldb);
    void archsai_sgels(ARCHSAI_CHAR *order, ARCHSAI_INT trans, ARCHSAI_INT rows, ARCHSAI_INT cols, ARCHSAI_INT nrhs, ARCHSAI_FLOAT *mat, ARCHSAI_INT lda, ARCHSAI_FLOAT *rhs, ARCHSAI_INT ldb);
    void archsai_dgesv(ARCHSAI_CHAR *layout, ARCHSAI_INT size, ARCHSAI_INT nrhs, ARCHSAI_DOUBLE *mat, ARCHSAI_INT lda, ARCHSAI_INT *ipiv, ARCHSAI_DOUBLE *rhs, ARCHSAI_INT ldb);
    void archsai_sgesv(ARCHSAI_CHAR *layout, ARCHSAI_INT size, ARCHSAI_INT nrhs, ARCHSAI_FLOAT *mat, ARCHSAI_INT lda, ARCHSAI_INT *ipiv, ARCHSAI_FLOAT *rhs, ARCHSAI_INT ldb);












////////////////////// WRAPPERS DIST MEM //////////////////////

void ArchSAI_CSRMV(ARCHSAI_CHAR *trans,  ARCHSAI_CHAR *matdescra, ARCHSAI_INT nprocs, ARCHSAI_DOUBLE alpha, ARCHSAI_MATCSR mat, ARCHSAI_DOUBLE *x, ARCHSAI_DOUBLE beta, ARCHSAI_DOUBLE *y);

ARCHSAI_DOUBLE ArchSAI_DOTP(ARCHSAI_INT size, ARCHSAI_DOUBLE *v1, ARCHSAI_INT inc1, ARCHSAI_DOUBLE *v2, ARCHSAI_INT inc2, MPI_Comm comm, ARCHSAI_INT nprocs);

#endif // __ArchSAI_LinAlg_h__



