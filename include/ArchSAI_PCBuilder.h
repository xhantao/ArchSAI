
 /*! \file ArchSAI_PCBuilder.h
    \brief This file contains the header of the function used by ArchSAI to compute preconditioners.

    ArchSAI preconditioning.
*/

 #ifndef ArchSAI_PCBuilder_h_
    #define ArchSAI_PCBuilder_h_

    #include <ArchSAI_DTypes.h>
    #include <ArchSAI_Error.h>
    #include <ArchSAI_SAI.h>
    #include <ArchSAI_PC.h>

    /*! \fn ARCHSAI_INT ArchSAI_BuildPC(ARCHSAI_MATCSR A, ARCHSAI_PRECOND *PC, ARCHSAI_HANDLER *handler);
        \brief Builds a preconditioner according to parameters

        \param A Input matrix
        \param pattern Pointer to empty preconditioner structure.
        \param handler ArchSAI handler.
    */
    ARCHSAI_INT ArchSAI_BuildPC(ARCHSAI_MATCSR A, ARCHSAI_PRECOND *PC, ARCHSAI_HANDLER *handler);


#endif // __ArchSAI_PCBuilder_h__



