 /*! \file ArchSAI_GPU.h
    \brief This file contains the GPU converters used by ArchSAI.

    ArchSAI GPU.
*/

 #ifndef ArchSAI_GPU_h_
    #define ArchSAI_GPU_h_

    #include <ArchSAI_DTypes.h>
	#include <ArchSAI_Memory.h>
	#include <ArchSAI_MPI.h>

	#ifdef ARCHSAI_USING_GPU
	#include <cudaProfiler.h>
    #include <cuda_profiler_api.h>
	#endif

	void ArchSAI_MatToGPU(ARCHSAI_MATCSR *mat, ARCHSAI_PARAMS params);
	ARCHSAI_VEC ArchSAI_InitGPUVec(ARCHSAI_INT size);
	size_t ArchSAI_GetSpMVBufferSize(ARCHSAI_MATCSR mat, ARCHSAI_VEC v1, ARCHSAI_VEC v2, ARCHSAI_DOUBLE fp1, ARCHSAI_DOUBLE fp2);
	void ArchSAI_CreateBLASHandle(ARCHSAI_GPU_BLASHANDLE *blashandle);

#endif // __ArchSAI_GPU_h__
