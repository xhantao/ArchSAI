
 /*! \file ArchSAI_Patterns.h
    \brief This file contains functions to operate with patterns for ArchSAI.

    ArchSAI pattern utilities.
*/

 #ifndef ArchSAI_Patterns_h_
    #define ArchSAI_Patterns_h_

    #include <ArchSAI_DTypes.h>
    #include <ArchSAI_Error.h>
    #include <ArchSAI_Memory.h>
    #include <ArchSAI_Matrix.h>
    #include <ArchSAI_Utils.h>
    #include <ArchSAI_Filter.h>
    #include <ArchSAI_SAI.h>
    #include <ArchSAI_Random.h>
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include <omp.h>

    /*! \fn ARCHSAI_INT ArchSAI_GetPattern(ARCHSAI_MATCSR A, ARCHSAI_MATCSR *pattern, ARCHSAI_PARAMS params);
        \brief Gets the pattern of a matrix according to parameters

        \param A Input matrix
        \param pattern Pattern according to parameters.
        \param params ArchSAI set of parameters.
    */
    ARCHSAI_INT ArchSAI_GetPattern(ARCHSAI_MATCSR A, ARCHSAI_MATCSR *pattern, ARCHSAI_PARAMS params);

    /*! \fn ARCHSAI_MATCSR ArchSAI_GetBasePattern(ARCHSAI_MATCSR A)
        \brief Gets the base pattern of input depending on PC.

        \param A Input matrix
        \param pc PC type.
    */
    ARCHSAI_MATCSR ArchSAI_GetBasePattern(ARCHSAI_MATCSR A, ARCHSAI_INT pc);


    /*! \fn ARCHSAI_INT ArchSAI_ExtendPatternSpatTemp(ARCHSAI_MATCSR A, ARCHSAI_MATCSR *pattern, ARCHSAI_PARAMS params);
        \brief Extends a pattern both spatially and temporally according to parameters

        \param A Input matrix
        \param pattern Pattern to extend
        \param params ArchSAI set of parameters.
    */
    ARCHSAI_INT ArchSAI_ExtendPatternSpatTemp(ARCHSAI_MATCSR A, ARCHSAI_MATCSR *pattern, ARCHSAI_PARAMS params);


    /*! \fn ARCHSAI_INT ArchSAI_ExtendPatternSpatTemp_SingleStep(ARCHSAI_MATCSR A, ARCHSAI_MATCSR *pattern, ARCHSAI_PARAMS params);
        \brief Extends a pattern both spatially and temporally according to parameters in a single step

        \param A Input matrix
        \param pattern Pattern to extend
        \param params ArchSAI set of parameters.
    */
    ARCHSAI_INT ArchSAI_ExtendPatternSpatTemp_SingleStep(ARCHSAI_MATCSR A, ARCHSAI_MATCSR *pattern, ARCHSAI_PARAMS params);

    /*! \fn ARCHSAI_INT ArchSAI_ExtendPatternSpatTemp_Reference(ARCHSAI_MATCSR A, ARCHSAI_MATCSR *pattern, ARCHSAI_PARAMS params);
        \brief Extends a pattern both spatially and temporally according to parameters but only extends initial pattern

        \param A Input matrix
        \param pattern Pattern to extend
        \param params ArchSAI set of parameters.
    */
    ARCHSAI_INT ArchSAI_ExtendPatternSpatTemp_Reference(ARCHSAI_MATCSR A, ARCHSAI_MATCSR *pattern, ARCHSAI_PARAMS params);

    /*! \fn ARCHSAI_INT ArchSAI_ExtendPatternSpatTemp_Reference_SingleStep(ARCHSAI_MATCSR A, ARCHSAI_MATCSR *pattern, ARCHSAI_PARAMS params);
        \brief Extends a pattern both spatially and temporally according to parameters but only extends initial pattern

        \param A Input matrix
        \param pattern Pattern to extend
        \param params ArchSAI set of parameters.
    */
    ARCHSAI_INT ArchSAI_ExtendPatternSpatTemp_Reference_SingleStep(ARCHSAI_MATCSR A, ARCHSAI_MATCSR *pattern, ARCHSAI_PARAMS params);


    /*! \fn ARCHSAI_INT ArchSAI_ExtendPattern(ARCHSAI_MATCSR *pattern, ARCHSAI_INT ext_size, ARCHSAI_INT VSAI, ARCHSAI_INT lower_upper);
        \brief Extends a pattern

        \param pattern Pattern to extend
        \param ext_size Size of extension
        \param VSAI 0 for SAI; 1 for FSAI
        \param lower_upper 0 for lower pattern; 1 for upper pattern. For SAI this is useless
    */
    ARCHSAI_INT ArchSAI_ExtendPattern(ARCHSAI_MATCSR *pattern, ARCHSAI_INT ext_size, ARCHSAI_INT VSAI, ARCHSAI_INT lower_upper);

    /*! \fn ARCHSAI_INT ArchSAI_ExtendPattern(ARCHSAI_MATCSR *pattern, ARCHSAI_INT ext_size, ARCHSAI_INT VSAI, ARCHSAI_INT lower_upper);
        \brief Extends a pattern. Temporal extension for SAI does not match cache lines.

        \param pattern Pattern to extend
        \param ext_size Size of extension
        \param VSAI 0 for SAI; 1 for FSAI
        \param lower_upper 0 for lower pattern; 1 for upper pattern. For SAI this is useless
    */
    ARCHSAI_INT ArchSAI_ExtendPattern_NoTmpCache(ARCHSAI_MATCSR *pattern, ARCHSAI_INT ext_size, ARCHSAI_INT VSAI, ARCHSAI_INT lower_upper);

    /*! \fn ARCHSAI_INT ArchSAI_ExtendReferencePattern(ARCHSAI_MATCSR *pattern, ARCHSAI_MATCSR ref, ARCHSAI_INT ext_size, ARCHSAI_INT VSAI, ARCHSAI_INT lower_upper);
        \brief Extends a pattern

        \param pattern Pattern to extend
        \param ref Reference pattern. Only entries with respect to this pattern will be added.
        \param ext_size Size of extension
        \param VSAI 0 for SAI; 1 for FSAI
        \param lower_upper 0 for lower pattern; 1 for upper pattern. For SAI this is useless
    */
    ARCHSAI_INT ArchSAI_ExtendReferencePattern(ARCHSAI_MATCSR *pattern, ARCHSAI_MATCSR ref, ARCHSAI_INT ext_size, ARCHSAI_INT VSAI, ARCHSAI_INT lower_upper);

    /*! \fn ARCHSAI_INT ArchSAI_PowerPattern(ARCHSAI_MATCSR *pattern, ARCHSAI_PARAMS power);
        \brief Powers a pattern

        \param pattern Pattern to power
        \param level Level.
    */
    ARCHSAI_INT ArchSAI_PowerPattern(ARCHSAI_MATCSR *pattern, ARCHSAI_INT level);


    /*! \fn ARCHSAI_INT ArchSAI_RandomizePattern(ARCHSAI_MATCSR *ptr, ARCHSAI_MATCSR ref, ARCHSAI_PARAMS params);
        \brief Randomly modifies a pattern keeping baseline untouched.

        \param ptr Pattern to modify.
        \param ref Reference pattern.
        \param params ArchSAI set of parameters.
    */
    ARCHSAI_INT ArchSAI_RandomizePattern(ARCHSAI_MATCSR *ptr, ARCHSAI_MATCSR ref, ARCHSAI_PARAMS params);

#endif // __ArchSAI_Patterns_h__




