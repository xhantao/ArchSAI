
 /*! \file ArchSAI_ParseArgs.h
    \brief This file contains the utilities to parse arguments for ArchSAI.

    ArchSAI argument setup utilities.
*/

 #ifndef ArchSAI_ParseArgs_h_
    #define ArchSAI_ParseArgs_h_

    #include <ArchSAI_DTypes.h>
    #include <ArchSAI_Error.h>
    #include <omp.h>
    #include <stdbool.h>
    #include <getopt.h>

    /*! \fn ARCHSAI_INT ArchSAI_ParseArgs(int argc, char *argv[], ARCHSAI_HANDLER *handler);
        \brief Reads execution arguments and sets the ArchSAI parameter structure

        \param argc
        \param argv
        \param handler Handler structure.
    */
    ARCHSAI_INT ArchSAI_ParseArgs(int argc, char *argv[], ARCHSAI_HANDLER *handler);

    ARCHSAI_INT iArchSAI_GetOpt(char *argv[], int opt, bool *input, ARCHSAI_INT *param);
    ARCHSAI_INT dArchSAI_GetOpt(char *argv[], int opt, bool *input, ARCHSAI_DOUBLE *param);
    ARCHSAI_INT cArchSAI_GetOpt(char *argv[], int opt, bool *input, ARCHSAI_CHAR *param);

    #define ArchSAI_GetOpt(w, x, y, z) \
   _Generic((z), ARCHSAI_INT*: iArchSAI_GetOpt, ARCHSAI_DOUBLE*: dArchSAI_GetOpt, ARCHSAI_CHAR*: cArchSAI_GetOpt, default: iArchSAI_GetOpt)(w, x, y, z)


    #endif // __ArchSAI_ParseArgs_h__




