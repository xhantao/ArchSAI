 /*! \file ArchSAI_DTypes.h
    \brief This file contains the types and data structures used by ArchSAI.

    ArchSAI types and structures.
*/

 #ifndef ArchSAI_DTypes_h_
    #define ArchSAI_DTypes_h_

	#ifdef ARCHSAI_USING_CUDA
		#include <cuda.h>
// 		#include <cuda_runtime.h>
// 		#include <cuda_runtime_api.h>
		#include <cusparse.h>
		#include <cublas_v2.h>
    #endif

	#ifdef ARCHSAI_USING_HIP
		#include <hip/hip_runtime.h>
	#endif

// ------------------------------------------------------------------------------- //
// Define ArchSAI data types // INTEGERS
// ------------------------------------------------------------------------------- //

/*! \var typedef ARCHSAI_BIGUINT
    \brief Type definition for Long Unsigned Integers.

    Long Unsigned Integers
*/

/*! \var typedef ARCHSAI_BIGINT
    \brief Type definition for Long Signed Integers.

    Long Signed Integers
*/

/*! \var typedef ARCHSAI_UINT
    \brief Type definition for Unsigned Integers.

    Unsigned Integers
*/

/*! \var typedef ARCHSAI_INT
    \brief Type definition for Signed Integers.

    Signed Integers
*/
    #if defined(ARCHSAI_BIGINT)
        typedef long long unsigned int  ARCHSAI_BIGUINT;
        typedef long long int           ARCHSAI_BIGINT;
        typedef long long unsigned int  ARCHSAI_UINT;
        typedef long long int           ARCHSAI_INT;
        #define ARCHSAI_MPI_LLINT       MPI_LONG_LONG_INT
        #define ARCHSAI_MPI_INT         MPI_LONG_LONG_INT
    #elif defined(ARCHSAI_SMALLINT)
        typedef int                     ARCHSAI_BIGUINT;
        typedef int                     ARCHSAI_BIGINT;
        typedef int                     ARCHSAI_UINT;
        typedef int                     ARCHSAI_INT;
        #define ARCHSAI_MPI_LLINT       MPI_INT
        #define ARCHSAI_MPI_INT         MPI_INT
    #else
        typedef long long unsigned int  ARCHSAI_BIGUINT;
        typedef long long int           ARCHSAI_BIGINT;
        typedef unsigned int            ARCHSAI_UINT;
        typedef int                     ARCHSAI_INT;
        #define ARCHSAI_MPI_LLINT       MPI_LONG_LONG_INT
        #define ARCHSAI_MPI_INT         MPI_INT
    #endif


    #ifndef ARCHSAI_USING_MPI
        typedef ARCHSAI_INT             MPI_Comm;
    #else
        #include <mpi.h>
    #endif

    /* This allows us to consistently avoid 'int' throughout archsai */
    typedef int                    archsai_int;
    typedef long int               archsai_longint;
    typedef unsigned int           archsai_uint;
    typedef unsigned long int      archsai_ulongint;
    typedef unsigned long long int archsai_ulonglongint;

// ------------------------------------------------------------------------------- //
// Define ArchSAI data types // Floats
// ------------------------------------------------------------------------------- //

/*! \var typedef ARCHSAI_FLOAT
    \brief Type definition for Single Precision numbers.

    Single Precision Real numbers.
*/

/*! \var typedef ARCHSAI_DOUBLE
    \brief Type definition for Double Precision numbers.

    Double Precision Real numbers.
*/
    #if defined ARCHSAI_FULLPREC
        typedef double                  ARCHSAI_FLOAT;
        typedef double                  ARCHSAI_DOUBLE;
        #define ARCHSAI_MPI_DOUBLE      MPI_DOUBLE

    #elif defined(ARCHSAI_SINGLE)
        typedef float                   ARCHSAI_FLOAT;
        typedef float                   ARCHSAI_DOUBLE;
        #define ARCHSAI_MPI_DOUBLE      MPI_FLOAT
    #else
        typedef float                   ARCHSAI_FLOAT;
        typedef double                  ARCHSAI_DOUBLE;
        #define ARCHSAI_MPI_DOUBLE      MPI_DOUBLE
    #endif

    /* This allows us to consistently avoid 'double' throughout archsai */
    typedef double                 archsai_double;

// ------------------------------------------------------------------------------- //
// Define ArchSAI data types // Floats
// ------------------------------------------------------------------------------- //

/*! \var typedef ARCHSAI_CHAR
    \brief Type definition for characters.

    Characters.
*/
    typedef char ARCHSAI_CHAR;

// ------------------------------------------------------------------------------- //
// Define ArchSAI Communicator Scheme Structure
// ------------------------------------------------------------------------------- //

/*! \struct ARCHSAI_COMM
    \brief ArchSAI basic structure to define the communicator scheme of a Distributed Matrix.

    Basic variables used to describe a communicator scheme
*/

/*!
    \var size
    \brief Defines the amount of entries.
*/

    typedef struct ARCHSAI_COMM {
        ARCHSAI_INT     nrecv;
        ARCHSAI_INT     nsend;
        ARCHSAI_INT     sendsize;
        ARCHSAI_INT     recvsize;   // number of halo rows
        ARCHSAI_INT    *sendproc;
        ARCHSAI_INT    *recvproc;
        ARCHSAI_INT    *sendptr;
        ARCHSAI_INT    *recvptr;
        ARCHSAI_INT    *sendpos;
        ARCHSAI_DOUBLE *send;
		ARCHSAI_DOUBLE *recv;

		#ifdef ARCHSAI_USING_GPU
			ARCHSAI_DOUBLE *gpusend;
		#endif
    } ARCHSAI_COMM;

// ACCESSOR

//     #define commscheme(i) i.val


// ------------------------------------------------------------------------------- //
// Define ArchSAI Vector Structure
// ------------------------------------------------------------------------------- //

/*! \struct ARCHSAI_VEC
    \brief ArchSAI basic structure to define vectors.

    Basic variables used to describe a dense vector.
*/

/*!
    \var size
    \brief Defines the amount of entries.
*/

/*!
    \var val
    \brief Pointer to memory space containing values .
*/

    typedef struct ARCHSAI_VEC {
        ARCHSAI_INT     size;
        ARCHSAI_INT    gsize;
        ARCHSAI_DOUBLE *val;

		#if defined ARCHSAI_USING_GPU
			#if defined ARCHSAI_USING_CUDA
				cusparseDnVecDescr_t dndescr;
			#elif defined ARCHSAI_USING_AMD
				cusparseDnVecDescr_t dndescr;
			#endif
		#endif


    } ARCHSAI_VEC;

// ACCESSOR

    #define vector(i) i.val

// ------------------------------------------------------------------------------- //
// Define ArchSAI Matrix CSR Structure
// ------------------------------------------------------------------------------- //

#if defined ARCHSAI_NEC
    #include <sblas.h>
#endif

/*! \struct ARCHSAI_MATCSR
    \brief ArchSAI basic structure to define sparse matrices.

    Basic variables used to describe a sparse matrix in the CSR format.
*/

/*!
    \var size
    \brief Defines the amount of rows/columns in symmetric matrices.
*/

/*!
    \var nnz
    \brief Defines the amount of non-zero entries in matrices.
*/

/*!
    \var nrows
    \brief Defines the amount of rows in general case matrices.
*/

/*!
    \var ncols
    \brief Defines the amount of columns in general case matrices.
*/

/*!
    \var rowind
    \brief Pointer to memory space containing row entry limits of a matrix.
*/

/*!
    \var colind
    \brief Pointer to memory space containing column indices of a matrix.
*/

/*!
    \var val
    \brief Pointer to memory space containing values of a matrix.
*/

/*!
    \var fval
    \brief Pointer to memory space containing values of a matrix.
*/

	#if defined ARCHSAI_USING_CUDA
		typedef cusparseMatDescr_t		ARCHSAI_MAT_DESCR;
		typedef cusparseSpMatDescr_t  	ARCHSAI_MAT_SPDESCR;
		typedef cusparseHandle_t 	    ARCHSAI_MAT_SPHANDLE;
	#elif defined ARCHSAI_USING_AMD
// 		rocsparse_mat_descr   descr;
	#endif

	#if defined ARCHSAI_USING_CUDA
		typedef cublasHandle_t			ARCHSAI_GPU_BLASHANDLE;
	#elif defined ARCHSAI_USING_AMD
		typedef rocsparse_mat_descr   	ARCHSAI_GPU_BLASHANDLE;
	#else
		typedef int ARCHSAI_GPU_BLASHANDLE;
	#endif

    typedef struct ARCHSAI_MATCSR {

        ARCHSAI_INT     size;
        ARCHSAI_INT     nnz;
        ARCHSAI_INT     nrows;
        ARCHSAI_INT     ncols;
        ARCHSAI_INT    *rowind;
        ARCHSAI_INT    *colind;
        ARCHSAI_DOUBLE *val;
        ARCHSAI_FLOAT  *fval;

		ARCHSAI_INT     format;  // 0 CSR 1 BSR 2 ELLPACK
		ARCHSAI_INT     gformat;
		ARCHSAI_INT		format_aux;



		ARCHSAI_INT    d_nnz;
		ARCHSAI_INT    d_nrows;
		ARCHSAI_INT    d_ncols;
		ARCHSAI_INT    d_gnnz;
		ARCHSAI_INT    d_gnrows;
		ARCHSAI_INT    d_gncols;

		#if defined ARCHSAI_USING_GPU
			ARCHSAI_INT    *d_rowind;
			ARCHSAI_INT    *d_colind;
			ARCHSAI_DOUBLE *d_val;

			ARCHSAI_VEC 	d_spmvr;
			ARCHSAI_VEC 	d_spmvc;

			ARCHSAI_MAT_SPDESCR 	matspdescr;
			ARCHSAI_MAT_DESCR   	matdescr;
			ARCHSAI_MAT_SPHANDLE   	sphandle;
		#endif

        ARCHSAI_INT     sym;
        ARCHSAI_INT     fulldiag;

        // Distributed

        ARCHSAI_INT     gsize;
        ARCHSAI_INT     gnnz;
        ARCHSAI_INT     gnrows;
        ARCHSAI_INT     gncols;
        ARCHSAI_INT    *rowdist;
        ARCHSAI_INT    *gl2loc;
        ARCHSAI_INT    *loc2gl;
        ARCHSAI_INT     locgl;

        ARCHSAI_COMM    *commscheme;
        MPI_Comm        comm;

        // SBLAS NEC

        #if defined ARCHSAI_NEC
            sblas_handle_t sbh;
        #endif

    } ARCHSAI_MATCSR;



    // ACCESSOR

    #define commscheme(i) i.commscheme
    #define ptrcomms(i) i->commscheme


// ------------------------------------------------------------------------------- //
// Define ArchSAI Parameter structure
// ------------------------------------------------------------------------------- //

/*! \struct ARCHSAI_PARAMS
    \brief ArchSAI structure that contains parameters about the execution.

    ArchSAI structure that contains parameters about the execution.
*/
    typedef struct ARCHSAI_PARAMS{

/*!
    \var matname
    \brief Name of the system matrix.
*/

/*!
    \var rhs
    \brief Name of the system rhs.
*/

/*!
    \var pat_sel
    \brief Type of pattern to be used in a potential preconditioning step.
*/

/*!
    \var pat_aux
    \brief Auxiliar value for selected pattern.
*/

/*!
    \var ext_sel
    \brief Type of architecture extension to be performed on the preconditioner.
*/

/*!
    \var ext_spa
    \brief Auxiliar value for spatial pattern extension.
*/

/*!
    \var ext_tmp
    \brief Auxiliar value for temporal pattern extension.
*/

/*!
    \var sol_sel
    \brief Solver to be used.
*/

/*!
    \var sol_aux
    \brief Auviliar value for solver.
*/

/*!
    \var precond
    \brief Type of preconditioner.
*/

/*!
    \var imax
    \brief Maximum amount of solver iterations.
*/

/*!
    \var reps
    \brief Repetitions of solver.
*/

/*!
    \var rel_tol
    \brief Solver relative tolerance.
*/

/*!
    \var abs_tol
    \brief Solver absolute tolerance.
*/

/*!
    \var kap_epsilon
    \brief Kaporin number for dynamic patterns.
*/

/*!
    \var filter_sel
    \brief Filtering-out type for pattern extensions.
*/

/*!
    \var filter
    \brief Filtering-out value for pattern extensions.
*/

/*!
    \var rand
    \brief Random pattern extension.
*/

        ARCHSAI_CHAR matname[1024];
        ARCHSAI_CHAR rhs[1024];
        ARCHSAI_INT pat_sel;
        ARCHSAI_INT pat_aux;
        ARCHSAI_INT ext_sel;
        ARCHSAI_INT ext_spa;
        ARCHSAI_INT ext_tmp;
        ARCHSAI_INT sol_sel;
        ARCHSAI_INT sol_aux;
        ARCHSAI_INT precond;
        ARCHSAI_INT imax;
        ARCHSAI_INT reps;
        ARCHSAI_DOUBLE rel_tol;
        ARCHSAI_DOUBLE abs_tol;
        ARCHSAI_DOUBLE kap_epsilon;
        ARCHSAI_INT     filter_sel;
        ARCHSAI_DOUBLE  filter;
        ARCHSAI_INT     rand;
        ARCHSAI_INT     epos;

		ARCHSAI_INT     format;
		ARCHSAI_INT		format_aux;

    } ARCHSAI_PARAMS;


// ------------------------------------------------------------------------------- //
// Define ArchSAI Solver Functions
// ------------------------------------------------------------------------------- //

/*! \struct ARCHSAI_SOLVER
    \brief ArchSAI structure that contains parameters about the solver.

    Describes solver.
*/

/*!
    \var bestrep
    \brief Best solver repetition.
*/

/*!
    \var reps
    \brief Number of repetitions.
*/

/*!
    \var residual
    \brief Stores resulting x residual for each repetition.
*/

/*!
    \var rel_res
    \brief Stores resulting x relative residual for each repetition.
*/

/*!
    \var iter_time
    \brief Stores iteration time for each repetition.
*/

/*!
    \var time
    \brief Stores total solving time for each repetition.
*/

/*!
    \var iter
    \brief Stores number of iterations for each repetition.
*/

    typedef struct ARCHSAI_SOLVER{
        ARCHSAI_INT           sys_ncols;
        ARCHSAI_INT           sys_nrows;
        ARCHSAI_INT           sys_nnz;
        ARCHSAI_INT           pc_ncols;
        ARCHSAI_INT           pc_nrows;
        ARCHSAI_INT           pc_nnz;
        ARCHSAI_INT           bestrep;
        ARCHSAI_INT           reps;
        ARCHSAI_DOUBLE        **residual;
        ARCHSAI_DOUBLE        **rel_res;
        archsai_double        **iter_time;
        archsai_double        *time;
        ARCHSAI_INT           *iter;
    } ARCHSAI_SOLVER;

// ------------------------------------------------------------------------------- //

// ------------------------------------------------------------------------------- //
// Define ArchSAI Preconditioner Structure
// ------------------------------------------------------------------------------- //


/*! \struct ARCHSAI_PRECOND
    \brief ArchSAI basic structure to define Preconditioners.

    Basic variables used to describe a Preconditioner in ArchSAI.
*/
    typedef struct ARCHSAI_PRECOND {

/*!
    \var type
    \brief Defines the preconditioner type.
*/

/*!
    \var sys_size
    \brief Defines the preconditioner size.
*/

/*!
    \var mat
    \brief Defines preconditioning matrix.
*/

/*!
    \var mattransp
    \brief Defines transposed preconditioning matrix.
*/
        ARCHSAI_INT type;
        ARCHSAI_INT sys_size;
        ARCHSAI_MATCSR mat;
        ARCHSAI_MATCSR mattransp;
    } ARCHSAI_PRECOND;


// ACCESSOR

    #define M(i) i.mat
    #define G(i) i.mat
    #define Gt(i) i.mattransp

// ------------------------------------------------------------------------------- //


// ------------------------------------------------------------------------------- //

// ------------------------------------------------------------------------------- //
// Define ArchSAI Handler. Stores execution information
// ------------------------------------------------------------------------------- //


/*! \struct ARCHSAI_HANDLER
    \brief ArchSAI basic structure to store information about execution.

    Basic variables used to describe an ArchSAI execution.
*/
    typedef struct ARCHSAI_HANDLER {

/*!
    \var params
    \brief Stores parameters.
*/

/*!
    \var solver
    \brief Stores solver results.
*/

/*!
    \var times
    \brief Stores ARCHSAI execution times.
*/

/*!
    \var comm
    \brief Communicator.
*/

        ARCHSAI_PARAMS params;
        ARCHSAI_SOLVER solver;
        ARCHSAI_DOUBLE times[3];
        ARCHSAI_COMM   comm;

    } ARCHSAI_HANDLER;

// ------------------------------------------------------------------------------- //

#endif // __ArchSAI_DTypes_h__

