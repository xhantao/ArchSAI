
 /*! \file ArchSAI_MPI.h
    \brief This file contains the MPI adaptations for non-distributed executions for ArchSAI.

    ArchSAI MPI utilities.
*/

 #ifndef ArchSAI_MPI_h_
    #define ArchSAI_MPI_h_

    #include <ArchSAI_DTypes.h>
    #include <ArchSAI_Memory.h>

    #ifndef ARCHSAI_USING_MPI

        #define MPI_Comm            archsai_MPI_Comm
        #define MPI_Group           archsai_MPI_Group
        #define MPI_Request         archsai_MPI_Request
        #define MPI_Datatype        archsai_MPI_Datatype
        #define MPI_Status          archsai_MPI_Status
        #define MPI_Op              archsai_MPI_Op
        #define MPI_Aint            archsai_MPI_Aint
        #define MPI_Info            archsai_MPI_Info

        #define MPI_COMM_WORLD       archsai_MPI_COMM_WORLD
        #define MPI_COMM_NULL        archsai_MPI_COMM_NULL
        #define MPI_COMM_SELF        archsai_MPI_COMM_SELF
        #define MPI_COMM_TYPE_SHARED archsai_MPI_COMM_TYPE_SHARED

        #define MPI_BOTTOM          archsai_MPI_BOTTOM

        #define MPI_FLOAT           archsai_MPI_FLOAT
        #define MPI_DOUBLE          archsai_MPI_DOUBLE
        #define MPI_LONG_DOUBLE     archsai_MPI_LONG_DOUBLE
        #define MPI_INT             archsai_MPI_INT
        #define MPI_LONG_LONG_INT   archsai_MPI_LONG_LONG_INT
        #define MPI_CHAR            archsai_MPI_CHAR
        #define MPI_LONG            archsai_MPI_LONG
        #define MPI_BYTE            archsai_MPI_BYTE

        #define MPI_C_FLOAT_COMPLEX archsai_MPI_COMPLEX
        #define MPI_C_LONG_DOUBLE_COMPLEX archsai_MPI_COMPLEX
        #define MPI_C_DOUBLE_COMPLEX archsai_MPI_COMPLEX

        #define MPI_SUM             archsai_MPI_SUM
        #define MPI_MIN             archsai_MPI_MIN
        #define MPI_MAX             archsai_MPI_MAX
        #define MPI_LOR             archsai_MPI_LOR
        #define MPI_LAND            archsai_MPI_LAND
        #define MPI_SUCCESS         archsai_MPI_SUCCESS
        #define MPI_STATUSES_IGNORE archsai_MPI_STATUSES_IGNORE

        #define MPI_UNDEFINED       archsai_MPI_UNDEFINED
        #define MPI_REQUEST_NULL    archsai_MPI_REQUEST_NULL
        #define MPI_INFO_NULL       archsai_MPI_INFO_NULL
        #define MPI_ANY_SOURCE      archsai_MPI_ANY_SOURCE
        #define MPI_ANY_TAG         archsai_MPI_ANY_TAG
        #define MPI_SOURCE          archsai_MPI_SOURCE
        #define MPI_TAG             archsai_MPI_TAG

        #define MPI_Init            archsai_MPI_Init
        #define MPI_Finalize        archsai_MPI_Finalize
        #define MPI_Abort           archsai_MPI_Abort
        #define MPI_Wtime           archsai_MPI_Wtime
        #define MPI_Wtick           archsai_MPI_Wtick
        #define MPI_Barrier         archsai_MPI_Barrier
        #define MPI_Comm_create     archsai_MPI_Comm_create
        #define MPI_Comm_dup        archsai_MPI_Comm_dup
        #define MPI_Comm_f2c        archsai_MPI_Comm_f2c
        #define MPI_Comm_group      archsai_MPI_Comm_group
        #define MPI_Comm_size       archsai_MPI_Comm_size
        #define MPI_Comm_rank       archsai_MPI_Comm_rank
        #define MPI_Comm_free       archsai_MPI_Comm_free
        #define MPI_Comm_split      archsai_MPI_Comm_split
        #define MPI_Comm_split_type archsai_MPI_Comm_split_type
        #define MPI_Group_incl      archsai_MPI_Group_incl
        #define MPI_Group_free      archsai_MPI_Group_free
        #define MPI_Address         archsai_MPI_Address
        #define MPI_Get_count       archsai_MPI_Get_count
        #define MPI_Alltoall        archsai_MPI_Alltoall
        #define MPI_Allgather       archsai_MPI_Allgather
        #define MPI_Allgatherv      archsai_MPI_Allgatherv
        #define MPI_Gather          archsai_MPI_Gather
        #define MPI_Gatherv         archsai_MPI_Gatherv
        #define MPI_Scatter         archsai_MPI_Scatter
        #define MPI_Scatterv        archsai_MPI_Scatterv
        #define MPI_Bcast           archsai_MPI_Bcast
        #define MPI_Send            archsai_MPI_Send
        #define MPI_Recv            archsai_MPI_Recv
        #define MPI_Isend           archsai_MPI_Isend
        #define MPI_Irecv           archsai_MPI_Irecv
        #define MPI_Send_init       archsai_MPI_Send_init
        #define MPI_Recv_init       archsai_MPI_Recv_init
        #define MPI_Irsend          archsai_MPI_Irsend
        #define MPI_Startall        archsai_MPI_Startall
        #define MPI_Probe           archsai_MPI_Probe
        #define MPI_Iprobe          archsai_MPI_Iprobe
        #define MPI_Test            archsai_MPI_Test
        #define MPI_Testall         archsai_MPI_Testall
        #define MPI_Wait            archsai_MPI_Wait
        #define MPI_Waitall         archsai_MPI_Waitall
        #define MPI_Waitany         archsai_MPI_Waitany
        #define MPI_Allreduce       archsai_MPI_Allreduce
        #define MPI_Reduce          archsai_MPI_Reduce
        #define MPI_Scan            archsai_MPI_Scan
        #define MPI_Request_free    archsai_MPI_Request_free
        #define MPI_Type_contiguous archsai_MPI_Type_contiguous
        #define MPI_Type_vector     archsai_MPI_Type_vector
        #define MPI_Type_hvector    archsai_MPI_Type_hvector
        #define MPI_Type_struct     archsai_MPI_Type_struct
        #define MPI_Type_commit     archsai_MPI_Type_commit
        #define MPI_Type_free       archsai_MPI_Type_free
        #define MPI_Op_free         archsai_MPI_Op_free
        #define MPI_Op_create       archsai_MPI_Op_create
        #define MPI_User_function   archsai_MPI_User_function
        #define MPI_Info_create     archsai_MPI_Info_create


        /*--------------------------------------------------------------------------
        * Types, etc.
        *--------------------------------------------------------------------------*/

        /* These types have associated creation and destruction routines */
        typedef ARCHSAI_INT archsai_MPI_Comm;
        typedef ARCHSAI_INT archsai_MPI_Group;
        typedef ARCHSAI_INT archsai_MPI_Request;
        typedef ARCHSAI_INT archsai_MPI_Datatype;
        typedef void (archsai_MPI_User_function) (void);

        typedef struct
        {
        ARCHSAI_INT archsai_MPI_SOURCE;
        ARCHSAI_INT archsai_MPI_TAG;
        } archsai_MPI_Status;

        typedef ARCHSAI_INT  archsai_MPI_Op;
        typedef ARCHSAI_INT  archsai_MPI_Aint;
        typedef ARCHSAI_INT  archsai_MPI_Info;

        #define  archsai_MPI_COMM_SELF   1
        #define  archsai_MPI_COMM_WORLD  0
        #define  archsai_MPI_COMM_NULL  -1

        #define  archsai_MPI_COMM_TYPE_SHARED 0

        #define  archsai_MPI_BOTTOM  0x0

        #define  archsai_MPI_FLOAT 0
        #define  archsai_MPI_DOUBLE 1
        #define  archsai_MPI_LONG_DOUBLE 2
        #define  archsai_MPI_INT 3
        #define  archsai_MPI_CHAR 4
        #define  archsai_MPI_LONG 5
        #define  archsai_MPI_BYTE 6
        #define  MPI_Double 7
        #define  archsai_MPI_COMPLEX 8
        #define  archsai_MPI_LONG_LONG_INT 9

        #define  archsai_MPI_SUM 0
        #define  archsai_MPI_MIN 1
        #define  archsai_MPI_MAX 2
        #define  archsai_MPI_LOR 3
        #define  archsai_MPI_LAND 4
        #define  archsai_MPI_SUCCESS 0
        #define  archsai_MPI_STATUSES_IGNORE 0

        #define  archsai_MPI_UNDEFINED -9999
        #define  archsai_MPI_REQUEST_NULL  0
        #define  archsai_MPI_INFO_NULL     0
        #define  archsai_MPI_ANY_SOURCE    1
        #define  archsai_MPI_ANY_TAG       1


    #else

        #include <mpi.h>

        /******************************************************************************
        * MPI stubs to do casting of ARCHSAI_INT and ARCHSAI_INT correctly
        *****************************************************************************/

        typedef MPI_Comm     archsai_MPI_Comm;
        typedef MPI_Group    archsai_MPI_Group;
        typedef MPI_Request  archsai_MPI_Request;
        typedef MPI_Datatype archsai_MPI_Datatype;
        typedef MPI_Status   archsai_MPI_Status;
        typedef MPI_Op       archsai_MPI_Op;
        typedef MPI_Aint     archsai_MPI_Aint;
        typedef MPI_Info     archsai_MPI_Info;
        typedef MPI_User_function    archsai_MPI_User_function;

        #define  archsai_MPI_COMM_WORLD         MPI_COMM_WORLD
        #define  archsai_MPI_COMM_NULL          MPI_COMM_NULL
        #define  archsai_MPI_BOTTOM             MPI_BOTTOM
        #define  archsai_MPI_COMM_SELF          MPI_COMM_SELF
        #define  archsai_MPI_COMM_TYPE_SHARED   MPI_COMM_TYPE_SHARED

        #define  archsai_MPI_FLOAT   MPI_FLOAT
        #define  archsai_MPI_DOUBLE  MPI_DOUBLE
        #define  archsai_MPI_LONG_DOUBLE  MPI_LONG_DOUBLE
        /* ARCHSAI_MPI_INT is defined in ArchSAI_utilities.h */
        #define  archsai_MPI_INT     ARCHSAI_MPI_INT
        #define  archsai_MPI_CHAR    MPI_CHAR
        #define  archsai_MPI_LONG    MPI_LONG
        #define  archsai_MPI_BYTE    MPI_BYTE
        /* MPI_Double is defined in ArchSAI_utilities.h */
        #define  MPI_Double    MPI_Double
        /* ArchSAI_MPI_COMPLEX is defined in ArchSAI_utilities.h */
        #define  archsai_MPI_COMPLEX ArchSAI_MPI_COMPLEX

        #define  archsai_MPI_SUM MPI_SUM
        #define  archsai_MPI_MIN MPI_MIN
        #define  archsai_MPI_MAX MPI_MAX
        #define  archsai_MPI_LOR MPI_LOR
        #define  archsai_MPI_SUCCESS MPI_SUCCESS
        #define  archsai_MPI_STATUSES_IGNORE MPI_STATUSES_IGNORE

        #define  archsai_MPI_UNDEFINED       MPI_UNDEFINED
        #define  archsai_MPI_REQUEST_NULL    MPI_REQUEST_NULL
        #define  archsai_MPI_INFO_NULL       MPI_INFO_NULL
        #define  archsai_MPI_ANY_SOURCE      MPI_ANY_SOURCE
        #define  archsai_MPI_ANY_TAG         MPI_ANY_TAG
        #define  archsai_MPI_SOURCE          MPI_SOURCE
        #define  archsai_MPI_TAG             MPI_TAG
        #define  archsai_MPI_LAND            MPI_LAND



    #endif


    ARCHSAI_INT archsai_MPI_Init( ARCHSAI_INT *argc, char ***argv );
    ARCHSAI_INT archsai_MPI_Finalize( void );
    ARCHSAI_INT archsai_MPI_Abort( archsai_MPI_Comm comm, ARCHSAI_INT errorcode );
    ARCHSAI_DOUBLE archsai_MPI_Wtime( void );
    ARCHSAI_DOUBLE archsai_MPI_Wtick( void );
    ARCHSAI_INT archsai_MPI_Barrier( archsai_MPI_Comm comm );
    ARCHSAI_INT archsai_MPI_Comm_create( archsai_MPI_Comm comm, archsai_MPI_Group group,
                                    archsai_MPI_Comm *newcomm );
    ARCHSAI_INT archsai_MPI_Comm_dup( archsai_MPI_Comm comm, archsai_MPI_Comm *newcomm );
    archsai_MPI_Comm archsai_MPI_Comm_f2c( ARCHSAI_INT comm );
    ARCHSAI_INT archsai_MPI_Comm_size( archsai_MPI_Comm comm, ARCHSAI_INT *size );
    ARCHSAI_INT archsai_MPI_Comm_rank( archsai_MPI_Comm comm, ARCHSAI_INT *rank );
    ARCHSAI_INT archsai_MPI_Comm_free( archsai_MPI_Comm *comm );
    ARCHSAI_INT archsai_MPI_Comm_group( archsai_MPI_Comm comm, archsai_MPI_Group *group );
    ARCHSAI_INT archsai_MPI_Comm_split( archsai_MPI_Comm comm, ARCHSAI_INT n, ARCHSAI_INT m,
                                    archsai_MPI_Comm * comms );
    ARCHSAI_INT archsai_MPI_Group_incl( archsai_MPI_Group group, ARCHSAI_INT n, ARCHSAI_INT *ranks,
                                    archsai_MPI_Group *newgroup );
    ARCHSAI_INT archsai_MPI_Group_free( archsai_MPI_Group *group );
    ARCHSAI_INT archsai_MPI_Address( void *location, archsai_MPI_Aint *address );
    ARCHSAI_INT archsai_MPI_Get_count( archsai_MPI_Status *status, archsai_MPI_Datatype datatype,
                                ARCHSAI_INT *count );
    ARCHSAI_INT archsai_MPI_Alltoall( void *sendbuf, ARCHSAI_INT sendcount, archsai_MPI_Datatype sendtype,
                                void *recvbuf, ARCHSAI_INT recvcount, archsai_MPI_Datatype recvtype, archsai_MPI_Comm comm );
    ARCHSAI_INT archsai_MPI_Allgather( void *sendbuf, ARCHSAI_INT sendcount, archsai_MPI_Datatype sendtype,
                                void *recvbuf, ARCHSAI_INT recvcount, archsai_MPI_Datatype recvtype, archsai_MPI_Comm comm );
    ARCHSAI_INT archsai_MPI_Allgatherv( void *sendbuf, ARCHSAI_INT sendcount, archsai_MPI_Datatype sendtype,
                                    void *recvbuf, ARCHSAI_INT *recvcounts, ARCHSAI_INT *displs, archsai_MPI_Datatype recvtype,
                                    archsai_MPI_Comm comm );
    ARCHSAI_INT archsai_MPI_Gather( void *sendbuf, ARCHSAI_INT sendcount, archsai_MPI_Datatype sendtype,
                                void *recvbuf, ARCHSAI_INT recvcount, archsai_MPI_Datatype recvtype, ARCHSAI_INT root,
                                archsai_MPI_Comm comm );
    ARCHSAI_INT archsai_MPI_Gatherv( void *sendbuf, ARCHSAI_INT sendcount, archsai_MPI_Datatype sendtype,
                                void *recvbuf, ARCHSAI_INT *recvcounts, ARCHSAI_INT *displs, archsai_MPI_Datatype recvtype,
                                ARCHSAI_INT root, archsai_MPI_Comm comm );
    ARCHSAI_INT archsai_MPI_Scatter( void *sendbuf, ARCHSAI_INT sendcount, archsai_MPI_Datatype sendtype,
                                void *recvbuf, ARCHSAI_INT recvcount, archsai_MPI_Datatype recvtype, ARCHSAI_INT root,
                                archsai_MPI_Comm comm );
    ARCHSAI_INT archsai_MPI_Scatterv( void *sendbuf, ARCHSAI_INT *sendcounts, ARCHSAI_INT *displs,
                                archsai_MPI_Datatype sendtype, void *recvbuf, ARCHSAI_INT recvcount, archsai_MPI_Datatype recvtype,
                                ARCHSAI_INT root, archsai_MPI_Comm comm );
    ARCHSAI_INT archsai_MPI_Bcast( void *buffer, ARCHSAI_INT count, archsai_MPI_Datatype datatype,
                            ARCHSAI_INT root, archsai_MPI_Comm comm );
    ARCHSAI_INT archsai_MPI_Send( void *buf, ARCHSAI_INT count, archsai_MPI_Datatype datatype, ARCHSAI_INT dest,
                            ARCHSAI_INT tag, archsai_MPI_Comm comm );
    ARCHSAI_INT archsai_MPI_Recv( void *buf, ARCHSAI_INT count, archsai_MPI_Datatype datatype, ARCHSAI_INT source,
                            ARCHSAI_INT tag, archsai_MPI_Comm comm, archsai_MPI_Status *status );
    ARCHSAI_INT archsai_MPI_Isend( void *buf, ARCHSAI_INT count, archsai_MPI_Datatype datatype, ARCHSAI_INT dest,
                            ARCHSAI_INT tag, archsai_MPI_Comm comm, archsai_MPI_Request *request );
    ARCHSAI_INT archsai_MPI_Irecv( void *buf, ARCHSAI_INT count, archsai_MPI_Datatype datatype,
                            ARCHSAI_INT source, ARCHSAI_INT tag, archsai_MPI_Comm comm, archsai_MPI_Request *request );
    ARCHSAI_INT archsai_MPI_Send_init( void *buf, ARCHSAI_INT count, archsai_MPI_Datatype datatype,
                                ARCHSAI_INT dest, ARCHSAI_INT tag, archsai_MPI_Comm comm, archsai_MPI_Request *request );
    ARCHSAI_INT archsai_MPI_Recv_init( void *buf, ARCHSAI_INT count, archsai_MPI_Datatype datatype,
                                ARCHSAI_INT dest, ARCHSAI_INT tag, archsai_MPI_Comm comm, archsai_MPI_Request *request );
    ARCHSAI_INT archsai_MPI_Irsend( void *buf, ARCHSAI_INT count, archsai_MPI_Datatype datatype, ARCHSAI_INT dest,
                                ARCHSAI_INT tag, archsai_MPI_Comm comm, archsai_MPI_Request *request );
    ARCHSAI_INT archsai_MPI_Startall( ARCHSAI_INT count, archsai_MPI_Request *array_of_requests );
    ARCHSAI_INT archsai_MPI_Probe( ARCHSAI_INT source, ARCHSAI_INT tag, archsai_MPI_Comm comm,
                            archsai_MPI_Status *status );
    ARCHSAI_INT archsai_MPI_Iprobe( ARCHSAI_INT source, ARCHSAI_INT tag, archsai_MPI_Comm comm, ARCHSAI_INT *flag,
                                archsai_MPI_Status *status );
    ARCHSAI_INT archsai_MPI_Test( archsai_MPI_Request *request, ARCHSAI_INT *flag, archsai_MPI_Status *status );
    ARCHSAI_INT archsai_MPI_Testall( ARCHSAI_INT count, archsai_MPI_Request *array_of_requests, ARCHSAI_INT *flag,
                                archsai_MPI_Status *array_of_statuses );
    ARCHSAI_INT archsai_MPI_Wait( archsai_MPI_Request *request, archsai_MPI_Status *status );
    ARCHSAI_INT archsai_MPI_Waitall( ARCHSAI_INT count, archsai_MPI_Request *array_of_requests,
                                archsai_MPI_Status *array_of_statuses );
    ARCHSAI_INT archsai_MPI_Waitany( ARCHSAI_INT count, archsai_MPI_Request *array_of_requests,
                                ARCHSAI_INT *index, archsai_MPI_Status *status );
    ARCHSAI_INT archsai_MPI_Allreduce( void *sendbuf, void *recvbuf, ARCHSAI_INT count,
                                archsai_MPI_Datatype datatype, archsai_MPI_Op op, archsai_MPI_Comm comm );
    ARCHSAI_INT archsai_MPI_Reduce( void *sendbuf, void *recvbuf, ARCHSAI_INT count,
                                archsai_MPI_Datatype datatype, archsai_MPI_Op op, ARCHSAI_INT root, archsai_MPI_Comm comm );
    ARCHSAI_INT archsai_MPI_Scan( void *sendbuf, void *recvbuf, ARCHSAI_INT count,
                            archsai_MPI_Datatype datatype, archsai_MPI_Op op, archsai_MPI_Comm comm );
    ARCHSAI_INT archsai_MPI_Request_free( archsai_MPI_Request *request );
    ARCHSAI_INT archsai_MPI_Type_contiguous( ARCHSAI_INT count, archsai_MPI_Datatype oldtype,
                                        archsai_MPI_Datatype *newtype );
    ARCHSAI_INT archsai_MPI_Type_vector( ARCHSAI_INT count, ARCHSAI_INT blocklength, ARCHSAI_INT stride,
                                    archsai_MPI_Datatype oldtype, archsai_MPI_Datatype *newtype );
    ARCHSAI_INT archsai_MPI_Type_hvector( ARCHSAI_INT count, ARCHSAI_INT blocklength, archsai_MPI_Aint stride,
                                    archsai_MPI_Datatype oldtype, archsai_MPI_Datatype *newtype );
    ARCHSAI_INT archsai_MPI_Type_struct( ARCHSAI_INT count, ARCHSAI_INT *array_of_blocklengths,
                                    archsai_MPI_Aint *array_of_displacements, archsai_MPI_Datatype *array_of_types,
                                    archsai_MPI_Datatype *newtype );
    ARCHSAI_INT archsai_MPI_Type_commit( archsai_MPI_Datatype *datatype );
    ARCHSAI_INT archsai_MPI_Type_free( archsai_MPI_Datatype *datatype );
    ARCHSAI_INT archsai_MPI_Op_free( archsai_MPI_Op *op );
    ARCHSAI_INT archsai_MPI_Op_create( archsai_MPI_User_function *function, ARCHSAI_INT commute,
                                archsai_MPI_Op *op );

#endif // __ArchSAI_MPI_h__




