
 /*! \file ArchSAI_Random.h
    \brief This file contains random number generation.

    ArchSAI RNG utilities.
*/

 #ifndef ArchSAI_Random_h_
    #define ArchSAI_Random_h_

    #include <ArchSAI_DTypes.h>

    ARCHSAI_DOUBLE ArchSAI_dRandom();
    ARCHSAI_INT ArchSAI_iRandom();
    void ArchSAI_Seed(ARCHSAI_DOUBLE low_in, ARCHSAI_DOUBLE hi_in);

#endif // __ArchSAI_Random_h__





