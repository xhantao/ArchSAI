 /*! \file ArchSAI_PC.h
    \brief This file contains the types and data structures used by preconditioners in ArchSAI.

    ArchSAI types and structures.
*/

 #ifndef ArchSAI_PC_h_
    #define ArchSAI_PC_h_

    #include <ArchSAI_DTypes.h>
    #include <ArchSAI_Error.h>
    #include <ArchSAI_Memory.h>
    #include <ArchSAI_Patterns.h>
    #include <ArchSAI_LinAlg.h>
    #include <ArchSAI_Utils.h>

// ------------------------------------------------------------------------------- //
    ARCHSAI_PRECOND ArchSAI_DiagonalPC(ARCHSAI_MATCSR A, ARCHSAI_PARAMS params);


#endif // __ArchSAI_PC_h__


