
 /*! \file ArchSAI_Print.h
    \brief This file contains the print utilities for ArchSAI.

    ArchSAI print utilities.
*/

 #ifndef ArchSAI_Print_h_
    #define ArchSAI_Print_h_

    #include <ArchSAI_DTypes.h>
    #include <ArchSAI_Error.h>
    #include <ArchSAI_Memory.h>
    #include <ArchSAI_Time.h>
    #include <ArchSAI_MPI.h>
    #include <ArchSAI_Utils.h>
    #include <stdarg.h>
    #include <stdio.h>
    #include <sys/stat.h>
    #include <omp.h>

    #if defined ARCHSAI_USING_MPI
        #include <mpi.h>
    #endif

    ARCHSAI_INT ArchSAI_PrintMatCSR(ARCHSAI_MATCSR ptr, ARCHSAI_INT val);
    ARCHSAI_INT ArchSAI_OutPrintMatCSR(ARCHSAI_MATCSR ptr);
    ARCHSAI_INT ArchSAI_OutPrintRHS(ARCHSAI_VEC vector, ARCHSAI_CHAR *name);

    ARCHSAI_INT ArchSAI_ExportSolverMetrics(ARCHSAI_SOLVER solver, ARCHSAI_PARAMS params, MPI_Comm comm);
    ARCHSAI_INT ArchSAI_ExportData(ARCHSAI_HANDLER handler, MPI_Comm comm);

    ARCHSAI_INT ArchSAI_ndigits( ARCHSAI_BIGINT number );
    ARCHSAI_INT ArchSAI_printf( const char *format, ... );
    ARCHSAI_INT ArchSAI_fprintf( FILE *stream, const char *format, ... );
    ARCHSAI_INT ArchSAI_sprintf( char *s, const char *format, ... );
    ARCHSAI_INT ArchSAI_scanf( const char *format, ... );
    ARCHSAI_INT ArchSAI_fscanf( FILE *stream, const char *format, ... );
    ARCHSAI_INT ArchSAI_sscanf( char *s, const char *format, ... );
    ARCHSAI_INT ArchSAI_ParPrintf(MPI_Comm comm, const char *format, ...);

#endif // __ArchSAI_Print_h__



